﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UMC.Video;
namespace UMC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UMCPresenter presenter = null;
        UMCModel umcmodel = null;
        bool isCabinSw = true;
        public MainWindow()
        {
            InitializeComponent();
            StethoscopeUI.btnClose.Click += BtnClose_Click;

             presenter = new UMCPresenter();
            presenter.TXRXevents += new EventHandler<EventArgs>(TXRXeventHandlerFunc);
             umcmodel = new UMCModel();
            SetDataContext();
            if (isCabinSw)
            {
                lblTitle.Text = "UMC CABIN ";
                Connect.Content = "Connect";
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Stethoscopelungpopup.StaysOpen = false;
            Stethoscopelungpopup.IsOpen = false;
            lblVideoStatus.Visibility = Visibility.Hidden;
            VideoStatus.Text = "";
        }

        private void btnSH_Click(object sender, RoutedEventArgs e)
        {
            string SR = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
            string SR1 = System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
        }

        private void shutdown_Click(object sender, RoutedEventArgs e)
        {
            string msg = "Are you sure you want to close the application?";
            MessageBoxResult res = MessageBox.Show(msg, "Shutdown", MessageBoxButton.YesNo);
            if(res == MessageBoxResult.Yes)
            this.Close();
        }

        /// <summary>
        /// SMART CARD DATA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSmartCard_Checked(object sender, RoutedEventArgs e)
        {

            presenter.ReadDeviceData(DEVICEDATA.SMARTCARD);
           
           
            
        }

        private void btnLungstethoscope_Click(object sender, RoutedEventArgs e)
        {
            isSthethoscopeChest = false;
            Stethoscopelungpopup.StaysOpen = true;
            Stethoscopelungpopup.IsOpen = true;
            StethoscopeUI.PresenterObject = presenter;
            StethoscopeUI.CABINSW = isCabinSw;


        }

        private void CABINMONITOR_Expanded(object sender, RoutedEventArgs e)
        {
            
            presenter.ReadDeviceData(DEVICEDATA.CABINMONITOR);
            

        }

        private void CABINMONITOR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
             
        }

        /// <summary>
        /// set the data context
        /// </summary>
        private void SetDataContext()
        {
            presenter.ModelObject = umcmodel;
            SMARTCARDGRID.DataContext = presenter;
            ATTNDENT.DataContext = presenter;
            SenserDatagrid.DataContext = presenter;
            GridTabPatientInfo.DataContext = presenter.PatientInfo;
            NoteTabGrid.DataContext = presenter.PatientInfo;
            VitalTabGrid.DataContext = presenter.PatientInfo;
        }
        /// <summary>
        /// Attendant Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMedicAttn_Click(object sender, RoutedEventArgs e)
        {
            presenter.ReadDeviceData(DEVICEDATA.ATTENDANTNP);    
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            UMCChartUI.DrawChart();
            VideoPlayer.StartStreaming();
        }

        void TXRXeventHandlerFunc(object sender,EventArgs args)
        {
            Dispatcher.BeginInvoke((Action)delegate ()
            {
               
                    lblVideoStatus.Visibility = Visibility;
                    lblVideoStatus.Text = "Stethoscope Status: ";
                    VideoStatus.Text = ((TXRXeventsArgs)args).msg;
                if (!isCabinSw)
                {
                    if (((TXRXeventsArgs)args).msg.Contains("Receiving stream"))
                    {
                        VideoStatus.Text = ((TXRXeventsArgs)args).msg + ". Video: Mute";
                        if (isSthethoscopeChest)
                            btnStethoscopeChest.Background = Brushes.LightSkyBlue;
                        else
                            StethoscopeUI.UpdateColor(1);
                    }
                    else
                    {

                        btnStethoscopeChest.Background = null;
                        if (((TXRXeventsArgs)args).msg.Contains("Stopped receiving"))
                            StethoscopeUI.UpdateColor(0);
                    }
                }
                else
                {
                    string msg = ((TXRXeventsArgs)args).msg;
                    if(msg.Contains("Ready for streaming at :rtsp:"))
                    {
                        if (isSthethoscopeChest)
                            btnStethoscopeChest.Background = Brushes.LightSkyBlue;
                        else
                            StethoscopeUI.UpdateColor(1);
                    }
                    else
                    {
                        btnStethoscopeChest.Background = null;
                        if (msg.Contains("Streaming Stopped."))
                        {
                            if (!isSthethoscopeChest)
                                StethoscopeUI.UpdateColor(0);
                        }
                    }
                }
               
            });
           
        }

        bool isSthethoscopeChest= false;
        private void btnStethoscopeChest_Click(object sender, RoutedEventArgs e)
        {
            isSthethoscopeChest = true;
            this.Cursor = Cursors.Wait;
            if(!isCabinSw)
                presenter.StartRX();
          
            else
            {
                //Temporary
                presenter.StartTX(0);
            }

            this.Cursor = Cursors.Arrow;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
              
        }

        private void PatientTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch(PatientTabControl.SelectedIndex)
            {
                case 0:
                case 1:
                    presenter.PatientInfo.ReadPatientInformation(PatientInformation.PatientInfoTab.INFO);
                    break;
                 
                case 2:
                    presenter.PatientInfo.ReadPatientInformation(PatientInformation.PatientInfoTab.VITAL);
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    break;

            }
        }

        private void btnEKG_Click(object sender, RoutedEventArgs e)
        {
          
        }
    }
}
