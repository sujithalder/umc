﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.Net;
using System.Threading;
using System.Runtime.CompilerServices;

namespace UMC.Presenter
{
   

    internal  class StethoscopeRx
    {
       
        [DllImport("ssoipRXLib.dll", CallingConvention = CallingConvention.Cdecl) ]
        extern static void ReadConfigurationFile();

        [DllImport("ssoipRXLib.dll", CallingConvention = CallingConvention.Cdecl)   ]
        extern static void SetLogFolder(String filewithfolder);

        [DllImport("ssoipRXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void Connect();
        [DllImport("ssoipRXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void RegisterRXCallback(IntPtr callback);
        

        //TX
        [UnmanagedFunctionPointer(System.Runtime.InteropServices.CallingConvention.StdCall, SetLastError = true)]
        public delegate void CallbackDelegate(string x);

        [DllImport("ssoipTXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void ReadTXConfigurationFile();

        [DllImport("ssoipTXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void GenerateRecordingDeviceFile();

        [DllImport("ssoipTXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void StartStreaming( int stethoscopeIndx);

        [DllImport("ssoipTXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void StopStreaming();

        [DllImport("ssoipTXLib.dll", CallingConvention = CallingConvention.Cdecl)]
        extern static void RegisterCallback(IntPtr callbackfunc);
        
        readonly string RXLogfile = "RXlog.txt";
        public event EventHandler<EventArgs> TXevents;
        IntPtr handle;
        CallbackDelegate NotificationHandle;
        public StethoscopeRx()
        {
           
        }

        /// <summary>
        /// 
        /// </summary>
        public void StartRX()
        {
           
            try
            {
                if (File.Exists(RXLogfile))
                    File.Delete(RXLogfile);

                NotificationHandle = new CallbackDelegate(CallBackTX);
                handle = Marshal.GetFunctionPointerForDelegate(NotificationHandle);
                RegisterRXCallback(handle);
                SetLogFolder(RXLogfile);
                ReadConfigurationFile();
                Connect();
            }
            catch(Exception ex)
            {
                string exp = ex.Message.ToString();
                if (TXevents != null)
                {
                    TXRXeventsArgs msg = new TXRXeventsArgs(exp);
                    TXevents(this, msg);
                }
            }
        }

        public string RXLogFile 
        {
            get
            {
                string logfileWithPath = AppDomain.CurrentDomain.BaseDirectory;
                logfileWithPath = Path.Combine(logfileWithPath, RXLogfile);
                return logfileWithPath;
            }
        }
       
        public void InitializeTX()
        {
            try
            {
                ReadTXConfigurationFile();

                GenerateRecordingDeviceFile();
            }
            catch(Exception ex)
            {
                string tmp = ex.Message.ToString();
                if (TXevents != null)
                {
                    TXRXeventsArgs msg = new TXRXeventsArgs(tmp);
                    TXevents(this, msg);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stethoscopeIndex"></param>
       public void StartTX(int stethoscopeIndex)
        {
            try
            {
                  NotificationHandle = new CallbackDelegate(CallBackTX);
                  handle = Marshal.GetFunctionPointerForDelegate(NotificationHandle);
                RegisterCallback(handle);
                ReadTXConfigurationFile();
                StartStreaming(stethoscopeIndex);

            }
            catch(Exception ex)
            {
                string tmp = ex.Message.ToString();
                if (TXevents != null)
                {
                    TXRXeventsArgs msg = new TXRXeventsArgs(tmp);
                    TXevents(this, msg);
                }
            }

          
        }
        
        public void CallBackTX(string test)
        {
             
                if (TXevents != null)
                {
                    TXRXeventsArgs msg = new TXRXeventsArgs(test);
                    TXevents(this, msg);
                }
             
            
        }

       
    }
  
}
