﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;
using UMC.Presenter;
namespace UMC
{
    public enum DEVICEDATA
    {
        CABINMONITOR,
        SMARTCARD,
        ATTENDANTNP,

    }

    public class TXRXeventsArgs : EventArgs
    {
        public TXRXeventsArgs() { }
        public TXRXeventsArgs(string data) { msg = data; }

        public string msg;
    }

    /// <summary>
    /// 
    /// </summary>
    public class UMCPresenter : INotifyPropertyChanged
    {
        

        /// <summary>
        /// SMART CARD DATA
        /// </summary>
        string _valueOnCard = string.Empty;
        string _visitcharge = string.Empty;
        string _cleaningOnCard = string.Empty;
        string _totalCleaningOnCard = string.Empty;
        string _visitCost = string.Empty;

        /// <summary>
        /// CABIN MONITOR DATA
        /// </summary>
        string _cint_humidity = string.Empty;
        string _cabin_int_temp = string.Empty;
        string _cabin_pathogen_count = string.Empty;
        string _cabin_ext_temp = string.Empty;
        string _cabin_ext_humidity = string.Empty;

        ////NPI Attendant Data
        string _name_npi = string.Empty;
        string _dea = string.Empty;
        string _provider_data = string.Empty;
        string _facility_data = string.Empty;
        string _facility_zip = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }


        /// <summary>
        /// 
        /// </summary>
        public string VisitCharge
        {
            get { return _visitcharge; }
            set
            {
                _visitcharge = value;
                OnPropertyChanged(new PropertyChangedEventArgs("VisitCharge"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public string ValueOnCard
        {
            get { return _valueOnCard; }
            set
            {
                _valueOnCard = value;

                OnPropertyChanged(new PropertyChangedEventArgs("ValueOnCard"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CleaningOnCard
        {
            get { return _cleaningOnCard; }
            set
            {
                _cleaningOnCard = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CleaningOnCard"));
            }
        }

        public string TotalCleaningOnCard
        {
            get { return _totalCleaningOnCard; }
            set
            {
                _totalCleaningOnCard = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TotalCleaningOnCard"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public string VisitCost
        {
            get { return _visitCost; }
            set
            {
                _visitCost = value;
                OnPropertyChanged(new PropertyChangedEventArgs("VisitCost"));
            }
        }

        /// <summary>
        /// CABIN Monitor data
        /// </summary>
        /// 

        public string CINT_HUMIDITY
        {
            get { return _cint_humidity; }
            set
            {
                _cint_humidity = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CINT_HUMIDITY"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_INT_TEMP
        {
            get { return _cabin_int_temp; }
            set
            {
                _cabin_int_temp = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_INT_TEMP"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_PATHOGEN_COUNT
        {
            get { return _cabin_pathogen_count; }
            set
            {
                _cabin_pathogen_count = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_PATHOGEN_COUNT"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_EXT_TEMP
        {
            get { return _cabin_ext_temp; }
            set
            {
                _cabin_ext_temp = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_EXT_TEMP"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_EXT_HUMIDITY
        {
            get { return _cabin_ext_humidity; }
            set
            {
                _cabin_ext_humidity = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_EXT_HUMIDITY"));
            }
        }

        /// <summary>
        /// Read the simulated data from file.
        /// </summary>
        /// <param name="device"></param>
        public void ReadDeviceData(DEVICEDATA device)
        {
            switch (device)
            {
                case DEVICEDATA.SMARTCARD:
                    ReadSmartCard();
                    break;
                case DEVICEDATA.CABINMONITOR:
                    CabinMonitor();
                    break;
                case DEVICEDATA.ATTENDANTNP:
                    NPAttendant();
                    break;
            }
        }

        /// <summary>
        /// Smart Card reader
        /// </summary>
        private void ReadSmartCard()
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SimulatedData\\smartcard.txt");

            if (File.Exists(filePath))
            {
                StreamReader file = new StreamReader(filePath);
                string line;

                try
                {
                    //temporary implementation
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "VISIT_CHARGE_PER_MIN":
                                VisitCharge = data[1];
                                break;
                            case "PREPAID_VISIT_VALUE_ON_CARD":
                                ValueOnCard = data[1];
                                break;
                            case "PREPAID_CLEANING_ON_CARD":
                                CleaningOnCard = data[1];
                                break;
                            case "TOTAL_PREPAID_ON_CARD":
                                TotalCleaningOnCard = data[1];
                                break;
                            case "VISIT_COST":
                                VisitCost = data[1];
                                break;


                            default:
                                break;
                        }

                    }
                }
                catch (Exception) { file.Close(); }
                file.Close();
            }//

        }


        /// <summary>
        /// Cabin monitor data
        /// </summary>
        private void CabinMonitor()
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SimulatedData\\cabinmonitor.txt");

            if (File.Exists(filePath))
            {

                StreamReader file = new StreamReader(filePath);
                string line;
                try
                {
                    //temporary implementation
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "CABIN_INT_HUMIDITY":
                                CINT_HUMIDITY = data[1] + "%";
                                break;
                            case "CABIN_INT_TEMP":
                                CABIN_INT_TEMP = data[1];
                                break;
                            case "CABIN_PATHOGEN_COUNT":
                                CABIN_PATHOGEN_COUNT = data[1];
                                break;
                            case "CABIN_EXT_TEMP":
                                CABIN_EXT_TEMP = data[1];
                                break;
                            case "CABIN_EXT_HUMIDITY":
                                CABIN_EXT_HUMIDITY = data[1] + "%";
                                break;


                            default:
                                break;
                        }

                    }
                }
                catch (Exception) { file.Close(); }

                file.Close();
            }

        }

        /// <summary>
        /// NPI DATA
        /// </summary>
        public String NAME_NPI
        {
            get { return _name_npi; }
            set
            {
                _name_npi = value;
                OnPropertyChanged(new PropertyChangedEventArgs("NAME_NPI"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DEA
        {
            get { return _dea; }
            set
            {
                _dea = value;
                OnPropertyChanged(new PropertyChangedEventArgs("DEA"));
            }

        }

        public string PROVIDER_NPI
        {
            get { return _provider_data; }
            set
            {
                _provider_data = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PROVIDER_NPI"));
            }

        }

        public string FACILITY_NAME
        {
            get { return _facility_data; }
            set
            {
                _facility_data = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FACILITY_NAME"));
            }
        }

        public string FACILITY_ZIP
        {
            get { return _facility_zip; }
            set
            {
                _facility_zip = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FACILITY_ZIP"));
            }
        }

        /// <summary>
        /// NPI Attendant data
        /// </summary>
        private void NPAttendant()
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SimulatedData\\ATTENDANT_NP.txt");

            if (File.Exists(filePath))
            {
                StreamReader file = new StreamReader(filePath);
                string line;
                try
                {
                    //temporary implementation
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "NAME_NPI":
                                NAME_NPI = data[1];
                                break;
                            case "DEA":
                                DEA = data[1];
                                break;
                            case "PROVIDER_NPI":
                                PROVIDER_NPI = data[1];
                                break;
                            case "FACILITY_NAME":
                                FACILITY_NAME = data[1];
                                break;
                            case "FACILITY_ZIP":
                                FACILITY_ZIP = data[1];
                                break;


                            default:
                                break;
                        }

                    }
                }
                catch (Exception)
                {
                    file.Close();
                }
                file.Close();
            }

        }

        object _modelObject = null;
        public object ModelObject { get { return _modelObject; } set { _modelObject = value; } }

        PatientInformation _pinfo = new PatientInformation();

        public PatientInformation PatientInfo
        {
            get { return _pinfo; }
        }

        StethoscopeRx stethoscope = null;
        public void StartRX()
        {
            if (stethoscope == null)
            {
                stethoscope = new StethoscopeRx();
                stethoscope.TXevents += new EventHandler<EventArgs>(TXRXEventHandler);
            }

            stethoscope.StartRX();
        }

        public string RXLogFile 
        {
            get
            {
                return stethoscope.RXLogFile;
            }
        }
         
        public void StartTX(int stethoscopeIndx)
        {
            if (stethoscope == null)
            {
                stethoscope = new StethoscopeRx();
                stethoscope.TXevents += new EventHandler<EventArgs>(TXRXEventHandler);
                stethoscope.InitializeTX();
            }
            stethoscope.StartTX(stethoscopeIndx);

        }

        public void TXRXEventHandler(object sender,EventArgs args)
        {
            if(TXRXevents !=null)
            {
                TXRXevents(this, args);
            }
        }
        public event EventHandler<EventArgs> TXRXevents;

    }
}
