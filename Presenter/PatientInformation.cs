﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.IO;

namespace UMC
{
   public class PatientInformation: INotifyPropertyChanged
    {
        public enum PatientInfoTab
        {
            INFO,
            NOTE,
            VITAL,

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        string _fullname=string.Empty;
        string _dateofBirth = string.Empty;
        string _medicalRecordNo = string.Empty;
        string _noteTxt = string.Empty;
        string _genderselectionIndex = string.Empty;
        string _maritialselectionIndex = string.Empty;
        string _employmentstatus = string.Empty;
        string _age = "Age: ";
        string _referalSource = string.Empty;
        string _isActive = "False";
       
        public string FullName
        {
            get { return _fullname; }
            set
            {
                _fullname = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FullName"));
            }
        }

        public string DateOfBirth
        {
            get { return _dateofBirth; }
            set
            {
                _dateofBirth = value;
                OnPropertyChanged(new PropertyChangedEventArgs("DateOfBirth"));
            }
        }

        public string MedicalRecordNo
        {
            get { return _medicalRecordNo; }
            set
            {
                _medicalRecordNo = value;
                OnPropertyChanged(new PropertyChangedEventArgs("MedicalRecordNo"));
            }
        }

        

        public string Genderstatus
        {
            get { return _genderselectionIndex; }
            set
            {
                _genderselectionIndex = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Genderstatus"));
            }
        }

        public string Maritialstatus
        {
            get { return _genderselectionIndex; }
            set
            {
                _genderselectionIndex = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Maritialstatus"));
            }
        }

        public string Employmentstatus
        {
            get { return _employmentstatus; }
            set
            {
                _employmentstatus = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Employmentstatus"));
            }
        }

        public string Age
        {
            get { return _age; }
            set
            {
                _age = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Age"));
            }
        }

        public string ReferralSource
        {
            get { return _referalSource; }
            set
            {
                _referalSource = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ReferralSource"));
            }
        }

        
        public string NoteTxt
        {
            get { return _noteTxt; }
            set
            {
                _noteTxt = value;
                OnPropertyChanged(new PropertyChangedEventArgs("NoteTxt"));
            }
        }


        public string isActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                OnPropertyChanged(new PropertyChangedEventArgs("isActive"));
            }
        }

        string _pulseRate = string.Empty;
        string _bloodpressure = string.Empty;
        string _weight = string.Empty;
        string _temprature = string.Empty;
        string _resiratory = string.Empty;
        string _pulseRateComments = string.Empty;
        string _bloodpressureComments = string.Empty;
        string _weightComments = string.Empty;
        string _tempratureComments = string.Empty;
        string _resiratoryComments = string.Empty;
        public string PulseRate
        {
            get { return _pulseRate; }
            set
            {
                _pulseRate = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PulseRate"));
            }
        }

        public string PulseRateComments
        {
            get { return _pulseRateComments; }
            set
            {
                _pulseRateComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PulseRateComments"));
            }
        }

        public string BloodPressure
        {
            get { return _bloodpressure; }
            set
            {
                _bloodpressure = value;
                OnPropertyChanged(new PropertyChangedEventArgs("BloodPressure"));
            }
        }

        public string BloodPressureComments
        {
            get { return _bloodpressureComments; }
            set
            {
                _bloodpressureComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("BloodPressureComments"));
            }
        }

        public string Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Weight"));
            }
        }

        public string WeightComments
        {
            get { return _weightComments; }
            set
            {
                _weightComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WeightComments"));
            }
        }

        public string Temprature
        {
            get { return _temprature; }
            set
            {
                _temprature = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Temprature"));
            }
        }


        public string TempratureComments
        {
            get { return _tempratureComments; }
            set
            {
                _tempratureComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TempratureComments"));
            }
        }

        public string Resiratory
        {
            get { return _resiratory; }
            set
            {
                _resiratory = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Resiratory"));
            }
        }

        public string ResiratoryComments
        {
            get { return _resiratoryComments; }
            set
            {
                _resiratoryComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ResiratoryComments"));
            }
        }

        public void ReadPatientInformation(PatientInfoTab tab)
        {
            switch(tab)
            {
                case PatientInfoTab.INFO:
                case   PatientInfoTab.NOTE:
                    ReadPatientRecord();
                    break;
                case PatientInfoTab.VITAL:
                    ReadVitalData();
                    break;
            }
        }
        

        /// <summary_resiratory
        /// NPI Attendant data
        /// </summary>
        void ReadPatientRecord()
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SimulatedData\\PatientInformation.txt");

            if (File.Exists(filePath))
            {
                StreamReader file = new StreamReader(filePath);
                string line;
                try
                {
                    //temporary implementation
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "FullName":
                                FullName = data[1];
                                break;
                            case "DateOfBirth":
                                DateOfBirth = data[1];
                                break;
                            case "MedicalRecordNo":
                                MedicalRecordNo = data[1];
                                break;
                            case "NOTE":
                                NoteTxt = data[1];
                                break;
                            case "Gender":
                                Genderstatus = data[1];
                                break;
                            case "Maritalstatus":
                                Maritialstatus = data[1];
                                break;

                            case "EmploymentStatus":
                                Employmentstatus = data[1];
                                break;
                            case "ReferralSource":
                                ReferralSource = data[1];
                                break;
                            case "Active":
                            isActive = data[1];
                                break;
                            default:
                                break;
                        }

                    }
                }
                catch (Exception)
                {
                    file.Close();
                }
                file.Close();
            }

        }

        void ReadVitalData()
        {
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SimulatedData\\VitalData.txt");

            if (File.Exists(filePath))
            {
                StreamReader file = new StreamReader(filePath);
                string line;
                try
                {
                    //temporary implementation
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "PulseRate":
                                PulseRate = data[1];
                                PulseRateComments = data[2];
                                break;
                            case "BloodPressure":
                                BloodPressure = data[1];
                                BloodPressureComments = data[2];
                                break;
                            case "Weight":
                                Weight = data[1];
                                WeightComments = data[2];
                                break;
                            case "Temprature":
                                Temprature = data[1];
                                TempratureComments = data[2];
                                break;
                            case "Resiratory":
                                Resiratory = data[1];
                                ResiratoryComments = data[2];
                                break;

                              
                            default:
                                break;
                        }

                    }
                }
                catch (Exception)
                {
                    file.Close();
                }
                file.Close();
            }

        }

    }
}
