#pragma once
#include"ssoiptxapi.h"
#include<iostream>
#include<vector>
#include<map>
using namespace std;
typedef void(__stdcall *callbackFunc)(const char*);
class ssoipTX
{
public:
	ssoipTX();
	~ssoipTX();

public:
	

	struct RecordingDeviceInfo
	{
		string deviceName;
		int id;
		string deviceType;

	};

	int GetRecordingList(int id[], string deviceNamelist[], string deviceType="Windows WASAPI");
	int	   RecordingDevicesList();
	void ReadConfigFile();
	void GenerateRecordingDeviceFile();
	void RegisterCallback(callbackFunc);
private:
map<string,	vector<RecordingDeviceInfo>>AllRecordingDeivce;
map<string, string>configdata;
string StreamMessage;
char url[1024];
public:
void* stream;
void StartStreaming(int stethoscopeOption);
void StopStreaming();
callbackFunc NotifyClient;
static bool __stdcall SoipStreamStateCallback(bool sending, SoipRecorder* soipRecorder, void* userData);
bool SoipStreamStateCallback(bool sending, SoipRecorder* soipRecorder);

int GetDeviceID(int index);
};

