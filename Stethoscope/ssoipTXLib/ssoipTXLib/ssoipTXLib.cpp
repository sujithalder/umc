// ssoipTXLib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include"ssoipTXLib.h"
#include"ssoiptxapi.h"
#include"LogfileUtils.h"
#include"iostream"
#include<list>
#include<map>
#include<vector>
#include"ssoipTX.h"
using namespace std;
ssoipTX ssoiptx;


//generate recording devices
SSOIPTXEXPORT void GenerateRecordingDeviceFile()
{
	ssoiptx.GenerateRecordingDeviceFile();
}

//Read Configuration File
SSOIPTXEXPORT void ReadTXConfigurationFile()
{
	ssoiptx.ReadConfigFile();
}

 
//StartStreaming
SSOIPTXEXPORT void StartStreaming(int stethoscopeOption)
{
	ssoiptx.StartStreaming(stethoscopeOption);
}

SSOIPTXEXPORT void StopStreaming()
{
	ssoiptx.StopStreaming();
	return;  
}



//Retrives the list of recorded devices connected;
SSOIPTXEXPORT int GetRecordingDeviceList(int  id[], string  deviceNamelist[], string  deviceType)
{

	ssoiptx.RecordingDevicesList();
	return	ssoiptx.GetRecordingList(id, deviceNamelist, deviceType);

}


SSOIPTXEXPORT void RegisterCallback(callbackFunc func)
{
	ssoiptx.RegisterCallback(func);
}