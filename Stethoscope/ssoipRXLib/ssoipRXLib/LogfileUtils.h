#pragma once
#include<iostream>
#include<fstream>
using namespace std;

class LogfileUtils
{
	  string Logfile;
	 
public:
	LogfileUtils();
	~LogfileUtils();

	void LogFilePath(const char* logilePath);
	void LogMessage(const char* message, bool append=true);
	void LogCurrentDateTime();
	void LogData(const char*fileName, const char*data,  bool endlog=true, bool append=true);
};

