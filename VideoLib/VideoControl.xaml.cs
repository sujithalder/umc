﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebEye.Controls.Wpf;
namespace UMC.Video
{
    /// <summary>
    /// Interaction logic for VideoControl.xaml
    /// </summary>
    public partial class VideoControl : UserControl
    {
        public VideoControl()
        {
            InitializeComponent();
            
        }

    public    void StartStreaming()
        {
          IEnumerable<WebCameraId> listofCamera =    webCameraControl.GetVideoCaptureDevices();
            WebCameraId cameraId=null;
            foreach (var v in listofCamera)
            {
                cameraId = (WebCameraId)v;
                if (v.Name.Contains("USB Camera"))
                {
                   
                    break;
                }
            }
            if(cameraId != null)
            webCameraControl.StartCapture(cameraId);
        }

        
    }
}
