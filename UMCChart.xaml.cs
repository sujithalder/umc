﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UMC
{
    /// <summary>
    /// Interaction logic for UMCChart.xaml
    /// </summary>
    public partial class UMCChart : UserControl
    {
        public UMCChart()
        {
            InitializeComponent();


            
        }

        public void DrawPolyline()
        {
            SolidColorBrush blackBrush = new SolidColorBrush();

            blackBrush.Color = Colors.Black;
            Polyline Polyline = new Polyline();
            Polyline.Stroke = blackBrush;
            Polyline.StrokeThickness = 1;

            PointCollection polygonPoints = new PointCollection();

             Point p = new Point(200, 30);
            Point p1 = new Point(180, 30);
            Point p2 = new Point(16, 30);
            Point p3 = new Point(150, 30);

            polygonPoints.Add(p);
            polygonPoints.Add(p1);
            polygonPoints.Add(p2);
            polygonPoints.Add(p3);



            Polyline.Points = polygonPoints;
            ChartCanvas.Children.Add(Polyline);


        }
        public void DrawXAxisLines()
        {
            for (int lineno = 0; lineno < ActualHeight; lineno++)
            {
                Line line = new Line();
                line.Stroke = Brushes.LightGray;

                line.X1 = 0;
                line.X2 = this.ActualWidth;
                line.Y1 = lineno*5;
                line.Y2 = lineno * 5;

                line.StrokeThickness = 1;
                if(line.Y2< ActualHeight)
                ChartCanvas.Children.Add(line);
            }

        }

        public void DrawYAxisLines()
        {
            
         

          for(int lineno=0; lineno<ActualWidth; lineno++)
            {
                Line line = new Line();
                line.Stroke = Brushes.LightGray;

                line.X1 = lineno*5;
                line.X2 = lineno*5;
                line.Y1 = 0;
                line.Y2 = this.ActualHeight;

                line.StrokeThickness = 1;
                if (line.X2 < ActualWidth)
                    ChartCanvas.Children.Add(line);
            }
          
        }

       
        public void DrawChart()
        {
            ChartCanvas.Children.Clear();

            DrawYAxisLines();
            DrawXAxisLines();
           // DrawPolyline();



        }

        public int MAXWidth { get; set; }
        public int MAXHeight { get; set; }
        
        

    }
}
