﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UMC
{
    /// <summary>
    /// Interaction logic for Stethoscopelungs.xaml
    /// </summary>
    public partial class Stethoscopelungs : UserControl
    {
        //SolidColorBrush colorbrush;
        int SelectedStethoscope = 1;
        public Stethoscopelungs()
        {
            InitializeComponent();
         //   Brush b= new Brush(#FFBEE6FD");
        btn1.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFBEE6FD")); ;
        }

        UMCPresenter _presener;
        public UMCPresenter PresenterObject
        {
            set { _presener = value; }
        }

        bool _iscabinsw = false;
        public bool CABINSW
        {
            set { _iscabinsw = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 1;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(1);
            this.Cursor = Cursors.Arrow;
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 2;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(2);
            this.Cursor = Cursors.Arrow;
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 3;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(3);
            this.Cursor = Cursors.Arrow;
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 4;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(4);
            this.Cursor = Cursors.Arrow;
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 5;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(5);
            this.Cursor = Cursors.Arrow;
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 6;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(6);
            this.Cursor = Cursors.Arrow;
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 7;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(7);
            this.Cursor = Cursors.Arrow;
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 8;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(8);
            this.Cursor = Cursors.Arrow;
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 9;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(9);
            this.Cursor = Cursors.Arrow;
        }

        private void btn10_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 10;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(10);
            this.Cursor = Cursors.Arrow;
        }

        private void btn11_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 11;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(11);
            this.Cursor = Cursors.Arrow;
        }

        private void btn12_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 12;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(12);
            this.Cursor = Cursors.Arrow;
        }

        private void btn13_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 13;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(13);
            this.Cursor = Cursors.Arrow;
        }

        private void btn14_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 14;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(14);
            this.Cursor = Cursors.Arrow;
        }

        private void btn15_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 15;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(15);
            this.Cursor = Cursors.Arrow;
        }

        private void btn16_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            SelectedStethoscope = 16;
            if (!_iscabinsw)
                _presener.StartRX();
            else
                _presener.StartTX(16);
            this.Cursor = Cursors.Arrow;
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if(btnApply.Content.Equals("Stop"))
                   Apply();
            SelectedStethoscope = 0;
            // 
        }

        public void UpdateColor(int reset)
        {
            Brush br = Brushes.White;
            if (reset==1)
            {
                br = Brushes.LightSkyBlue;

                btnApply.Content = "Stop";

            }
            else if(reset ==2)
            {
              br=  (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFBEE6FD")); ;
               
            }
            else if(reset==0)
            {
                if (btnApply.Content.Equals("Stop"))
                    btnApply.Content = "Start";
            }
          
            switch (SelectedStethoscope)
            {
                case 1:
                    btn1.Background = br;
                    break;
                case 2:
                    btn2.Background = br;
                    break;
                case 3:
                    btn3.Background = br;
                    break;
                case 4:
                    btn4.Background = br;
                    break;
                case 5:
                    btn5.Background = br;
                    break;
                case 6:
                    btn6.Background = br;
                    break;
                case 7:
                    btn7.Background = br;
                    break;
                case 8:
                    btn8.Background = br;
                    break;
                case 9:
                    btn9.Background = br;
                    break;
                case 10:
                    btn10.Background = br;
                    break;
                case 11:
                    btn11.Background = br;
                    break;
                case 12:
                    btn12.Background = br;
                    break;
                case 13:
                    btn13.Background = br;
                    break;
                case 14:
                    btn14.Background = br;
                    break;
                case 15:
                    btn15.Background = br;
                    break;
                case 16:
                    btn16.Background = br;
                    break;
            }

           
        }

        private void btnUP_Click(object sender, RoutedEventArgs e)
        {
            if (btnApply.Content.Equals("Stop"))
                return;
            UpdateColor(0);
            SelectedStethoscope++;
            if (SelectedStethoscope >= 16)
                SelectedStethoscope = 16;
            UpdateColor(2);
        }

        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            if (btnApply.Content.Equals("Stop"))
                return;
                UpdateColor(0);
            SelectedStethoscope--;
            if (SelectedStethoscope <=0)
                SelectedStethoscope = 1;
            UpdateColor(2);
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
            if (btnApply.Content.Equals("Stop"))
                btnApply.Content = "Start";
        }

        void Apply()
        {
            switch (SelectedStethoscope)
            {
                case 1:
                    btn1_Click(null, null);
                    break;
                case 2:
                    btn2_Click(null, null);
                    break;
                case 3:
                    btn3_Click(null, null);
                    break;
                case 4:
                    btn4_Click(null, null);
                    break;
                case 5:
                    btn5_Click(null, null);
                    break;
                case 6:
                    btn6_Click(null, null);
                    break;
                case 7:
                    btn7_Click(null, null);
                    break;
                case 8:
                    btn8_Click(null, null);
                    break;
                case 9:
                    btn9_Click(null, null);
                    break;
                case 10:
                    btn10_Click(null, null);
                    break;
                case 11:
                    btn11_Click(null, null);
                    break;
                case 12:
                    btn12_Click(null, null);
                    break;
                case 13:
                    btn13_Click(null, null);
                    break;
                case 14:
                    btn14_Click(null, null);
                    break;
                case 15:
                    btn15_Click(null, null);
                    break;
                case 16:
                    btn16_Click(null, null);
                    break;
            }
        }

        private void btnApply_MouseEnter(object sender, MouseEventArgs e)
        {
            btnApply.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFBEE6FD")); ;
        }

        private void btnApply_MouseLeave(object sender, MouseEventArgs e)
        {
            btnApply.Background = null;
        }
    }
}
