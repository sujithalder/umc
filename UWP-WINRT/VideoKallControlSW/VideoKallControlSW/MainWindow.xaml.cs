﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO.Ports;
using System.Drawing;
using ControlSoftware;
using System.Windows.Threading;
using System.Text.RegularExpressions;

namespace VideoKallControlSW
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SerialDeviceMgr _serialDevice = new SerialDeviceMgr();
        DispatcherTimer timer = new DispatcherTimer();
        DAQ daq = new DAQ();
        CommunicationModule commCabinstatus = new CommunicationModule();

        public MainWindow()
        {
            InitializeComponent();
            LogMessage.Log(DateTime.Now.ToString(), false);

            SerialDeviceMgr.Notify += new EventHandler<EventArgs>(NotifyHandlerSerialDevice);
            daq.UpdateBitValues += new EventHandler<EventArgs>(UpdateBitValues);
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += ReadDAQValues;

            _serialDevice.Daq = daq;

            commCabinstatus.NotifyConnection += new EventHandler<EventArgs>(NotifyUpdateCabinsStatus);
            commCabinstatus.NotifyTestRigCMD += new EventHandler<EventArgs>(HandleTestRigCmd);
            commCabinstatus.Initialize();
            daq.Comm = commCabinstatus;

         }

        void HandleTestRigCmd(object sender, EventArgs e)
        {
            SerialEventMsg m = (SerialEventMsg)e;
            SERIALDEVICEOPERATIONS panel = SERIALDEVICEOPERATIONS.UNKNOWN;
            int state = -1;
            string[] statusmsg = m.msg.Split(':');

            if (statusmsg.Length > 2)
            {
                panel = (SERIALDEVICEOPERATIONS)Convert.ToInt32(statusmsg[1]);
                state = Convert.ToInt32(statusmsg[2]);
            }

            switch(panel)
            {
                case SERIALDEVICEOPERATIONS.AIRINLETFAN:
                    Img18_MouseDown(null, null);
                    break;
                case SERIALDEVICEOPERATIONS.H2O2FOGFAN:
                    Img19_MouseDown(null, null);
                    break;
                case SERIALDEVICEOPERATIONS.H2O2EXHUSTFAN:
                    Img26_MouseDown(null, null);
                    break;
                case SERIALDEVICEOPERATIONS.CABINDOORCONTROLLER:
                    Img27_MouseDown(null, null);
                    break;
                case SERIALDEVICEOPERATIONS.CLEAN_WASH_DRY:

                    InstCleanOperation();
                    break;
                case SERIALDEVICEOPERATIONS.DEP_DERMASCOPE:
                case SERIALDEVICEOPERATIONS.RET_DERMASCOPE:
                    DermascopeOperation();
                    break;
                case SERIALDEVICEOPERATIONS.RET_THERMOMETER:
                case SERIALDEVICEOPERATIONS.DEP_THERMOMETER:
                    ThermoMeterOperation();
                    break;
                case SERIALDEVICEOPERATIONS.DEP_SH_HW:
                case SERIALDEVICEOPERATIONS.DEP_SEAT_HW:
                   
                    break;
                case SERIALDEVICEOPERATIONS.RET_SH_HW:
                case SERIALDEVICEOPERATIONS.RET_SEAT_HW:
                    break;

                case SERIALDEVICEOPERATIONS.DEP_PO:
                case SERIALDEVICEOPERATIONS.RET_PO:
                    PulseOxymeter();
                    break;
                case SERIALDEVICEOPERATIONS.DEP_BP:
                case SERIALDEVICEOPERATIONS.RET_BP:
                    
                    break;
                case SERIALDEVICEOPERATIONS.DEP_CHESTSTETHOSCOPE:
                    MicrophoneSelection(0, true);
                    break;
                case SERIALDEVICEOPERATIONS.RET_CHESTTHOSCOPE:
                    MicrophoneSelection(1,true);
                    break;
                case SERIALDEVICEOPERATIONS.RET_EKG:
                    break;
                case SERIALDEVICEOPERATIONS.DEP_EKG:
                    break;
                case SERIALDEVICEOPERATIONS.DEP_GLUCOMETER:
                    break;
                case SERIALDEVICEOPERATIONS.RET_GLUCOMETER:
                    break;
                case SERIALDEVICEOPERATIONS.RET_SPIROMETER:
                    break;
                case SERIALDEVICEOPERATIONS.DEP_SPIROMETER:
                    break;
                case SERIALDEVICEOPERATIONS.SELECTMICROFONE:
                    MicrophoneSelection(state, true);
                    break;
                case SERIALDEVICEOPERATIONS.SELECT_CHAIRPOSITION:
                    StethoscopeChairPosSelection(state, true);
                    break;
                case SERIALDEVICEOPERATIONS.DEP_OTOSCOPE:
                case SERIALDEVICEOPERATIONS.RET_OTOSCOPE:
                    OtoscopeCamera();
                        break;

            }
        }
        void NotifyUpdateCabinsStatus(object sender, EventArgs e)
        {
            SerialEventMsg m = (SerialEventMsg)e;

            Dispatcher.BeginInvoke((Action)delegate ()
            {
                statustxt.Text = m.msg;
            });

                UpdateCabinstatus(true);
        }

        int deviceStatus = -1;
         void UpdateCabinstatus(bool forcesend=false)
        {
            int status = -1;

            if (daq.DeviceStatus(Instruments.CLEANING_STARTED))
            {
                status = 3;
                string teststatus =  string.Format(CommunicationModule.Serialdevicestatusformat, ((int)(TestRigPannel.CLEANING_STATUS)).ToString(),  1);

                commCabinstatus.UpdateCabinSW(teststatus);

            }
            else
            {
                if (daq.DeviceStatus(Instruments.CABIN_READY_FORUSE) ||(!daq.DeviceStatus(Instruments.CLEANING_STARTED)&& deviceStatus == 3))
                    status = 1;
                else if (!daq.DeviceStatus(Instruments.CABIN_READY_FORUSE))
                    status = 2;
            }

            if(!daq.DeviceStatus(Instruments.CLEANING_STARTED) && deviceStatus == 3)
            {
                string teststatus = string.Format(CommunicationModule.Serialdevicestatusformat, ((int)(TestRigPannel.CLEANING_STATUS)).ToString(), 0);

                commCabinstatus.UpdateCabinSW(teststatus);
            }

            if (deviceStatus != status || forcesend)
            {
                deviceStatus = status;
                //if(!forcesend)
               // LogMessage.Log(DateTime.Now.ToString() + " Device status-:" + status.ToString());

                commCabinstatus.UpdateCabinStatus(status);
            }

            status = -1;
            string testrigstatus = string.Empty;
            //
            //UPDATE TESTRIG STATUS
             status =   daq.DeviceStatus(Instruments.MOTION_DETECTOR) == true ? 0 : 1;
            if(motiondetectorstatus != status || forcesend)
            {
                motiondetectorstatus = status;
                testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.MOTIONDETECTOR)).ToString(), daq.DeviceStatus(Instruments.MOTION_DETECTOR) == true ? 0 : 1);
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }
            

            testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.AIRINLETFAN)).ToString(), daq.DeviceStatus(Instruments.AIR_SCRUB_FAN) == true ? 0 : 1);
            status = daq.DeviceStatus(Instruments.AIR_SCRUB_FAN) == true ? 0 : 1;
            if(airinletfanstatus !=status|| forcesend)
            {
                airinletfanstatus = status;
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }

             status = daq.DeviceStatus(Instruments.H202_FLUID_EMPTY) == true ? 0 : 1;
            if (h2o2leveldetectorstatus != status|| forcesend)
            {
                h2o2leveldetectorstatus = status;
                testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.H2O2LEVELDETECTOR)).ToString(), daq.DeviceStatus(Instruments.H202_FLUID_EMPTY) == true ? 0 : 1);
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }

              status = daq.DeviceStatus(Instruments.H202_FUG_FAN) == true ? 0 : 1;

            if (h2o2fogfanstatus != status||forcesend)
            {
                testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.H2O2FOGGINGFAN)).ToString(), daq.DeviceStatus(Instruments.H202_FUG_FAN) == true ? 0 : 1);

                h2o2fogfanstatus = status;
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }

            status = daq.DeviceStatus(Instruments.H202_EXHAUST_FAN) == true ? 0 : 1;

            if (h2o2exhustfanstatus != status||forcesend)
            {
                h2o2exhustfanstatus = status;
                testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.H2O2EXHUSTFAN)).ToString(), daq.DeviceStatus(Instruments.H202_EXHAUST_FAN) == true ? 0 : 1);
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }

            status = daq.DeviceStatus(Instruments.CABINDOOR_CONTROLLER) == true ? 0 : 1;
            if (cabondoorcontrollerstatus != status||forcesend)
            {
                cabondoorcontrollerstatus = status;
                testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.CABINDOORCONROLLER)).ToString(), daq.DeviceStatus(Instruments.CABINDOOR_CONTROLLER) == true ? 0 : 1);
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }

            status = daq.DeviceStatus(Instruments.CABIN_DOOR_OPEN) == true ? 0 : 1;
            if (cabindooropenstatus != status||forcesend)
            {
                cabindooropenstatus = status;
                testrigstatus = string.Format(CommunicationModule.Testrigstatusformat, ((int)(TestRigPannel.CABINDOOROPEN)).ToString(), daq.DeviceStatus(Instruments.CABIN_DOOR_OPEN) == true ? 0 : 1);
                commCabinstatus.UpdateCabinSW(testrigstatus);
            }



        }
        int motiondetectorstatus = -1;
        int airinletfanstatus = -1;
        int h2o2leveldetectorstatus = -1;
        int h2o2fogfanstatus = -1;
        int h2o2exhustfanstatus = -1;
        int cabondoorcontrollerstatus = -1;
        int cabindooropenstatus = -1;
        void MonitorWaterStatus()
        {
            daq.OutOfService();
        }

        void ReadDAQValues(object sender, EventArgs e)
        {
            timer.Stop();
            try
            {
                daq.ReadDI();
                DisplayRawData();
                UpdateCabinstatus();

             MonitorWaterStatus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            timer.Start();
        }

        void NotifyHandlerSerialDevice(object sender, EventArgs e)
        {
            SerialEventMsg m = (SerialEventMsg)e;
            LogMessage.Log(DateTime.Now.ToString() + "-" + m.msg + " l-: " + m.msg.Length);
            string msg = string.Empty;
            //S0+E
            string cmd = m.msg.Trim();
            cmd = cmd.ToUpper();
           // LogMessage.Log(DateTime.Now.ToString() + "-" +cmd+" l-: "+cmd.Length);

            switch (cmd)
            {
                case "CABINDOOROPEN":
                    msg = "CABIN DOOR OPEN.";
                    break;
                case "CABINISNOTEMPTY":
                    msg = "CABIN IS NOT EMPTY.";
                    break;
                case "CLEANSOLUTIONEMPTY":
                    msg = "CLEAN SOLUTION EMPTY.";
                    break;
                case "WASTEWATERFULL":
                    msg = "WASTE WATER FULL.";
                    break;
                case "S0WE":
                    daq.UpdateCleaningStatus(true);
                    break;
                case "S0+E":
                    //S0+E
                    LogMessage.Log(DateTime.Now.ToString()+"case -"+msg);
                    daq.UpdateCleaningStatus(false);
                    break;
                case "CLEAN_WASH_DRY":
                    break;
                case "RET_DERMASCOPE":
                    break;
                case "DEP_DERMASCOPE":
                    break;
                case "DEP_THERMOMETER":
                    break;
                case "RET_THERMOMETER":
                    break;
                case "DEP_PO":
                    break;
                case "RET_PO":
                break;

            }

            //Cleaning operation failed. 
            if (msg.Length > 0)
            {
                //MessageBox.Show(msg, "Cleaning PreCondition Failed.", MessageBoxButton.OK, MessageBoxImage.Error);
              m.msg= msg+ "Cleaning PreCondition Failed.";

             String   testrigstatus = string.Format(CommunicationModule.Testrigstatusformat,"ERROR", msg + "Cleaning PreCondition Failed.");
             commCabinstatus.UpdateCabinSW(testrigstatus);

                
            }

            //if (cmd.Contains("S0+E"))
            //{
            //    LogMessage.Log(DateTime.Now.ToString() + "case -" + msg);
            //    daq.UpdateCleaningStatus(false);
            //}

            Dispatcher.BeginInvoke((Action)delegate ()
                 {
                     if(m.msg.Contains("S0+E"))
                     {
                         BtnClean.Background = System.Windows.Media.Brushes.LightGray;
                         BtnClean.IsEnabled = true;

                     }
                    statustxt.Text = m.msg;
                 });
        }

        void UpdateBitValues(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)delegate ()
            {
                BitValueArgs m = (BitValueArgs)e;
                int port = m.port;
                if (port == 0)
                    txthex3.Text = m.msg;
                else if (port == 1)
                    txthex4.Text = m.msg;
                else if (port == -1 || port == -2)
                    MessageBox.Show(m.msg);

            });
        }

        void UpdateStatus(TextBlock txtb, Instruments inst, int col)
        {
            
            if (daq.DIStatus[inst] == true)
                txtb.Background= System.Windows.Media.Brushes.LightBlue;
            // dataGridView1.Rows[0].Cells[col].Style.BackColor = Color.Blue;
            else
                txtb.Background = System.Windows.Media.Brushes.LightSalmon;
            // dataGridView1.Rows[0].Cells[col].Style.BackColor = Color.Red;

          //  dataGridView1.Rows[0].Cells[col].Value = daq.DIStatus[inst];
            txtb.Text= daq.DIStatus[inst].ToString();
        }
         
        void DisplayRawData()
        {
            //Device status
            foreach (var v in daq.DIStatus)
            {
                Instruments ins = v.Key;
                switch (ins)
                {
                    case Instruments.AIR_SCRUB_FAN:
                        UpdateStatus(txtcol1,Instruments.AIR_SCRUB_FAN, 0);
                        break;
                    case Instruments.H202_FUG_FAN:
                        UpdateStatus(txtcol2,Instruments.H202_FUG_FAN, 1);
                        break;
                    case Instruments.H202_EXHAUST_FAN:
                        UpdateStatus(txtcol3,Instruments.H202_EXHAUST_FAN, 2);
                        break;
                    case Instruments.MOTION_DETECTOR:

                        UpdateStatus(txtcol4, Instruments.MOTION_DETECTOR, 3);
                        break;

                    case Instruments.H202_FLUID_EMPTY:
                        UpdateStatus(txtcol5, Instruments.H202_FLUID_EMPTY, 4);
                        break;
                    case Instruments.CLEAN_FLUID_EMPTY:
                        UpdateStatus(txtcol6, Instruments.CLEAN_FLUID_EMPTY, 5);
                        break;
                    case Instruments.WASTE_FLUID_EMPTY:
                        UpdateStatus(txtcol7,Instruments.WASTE_FLUID_EMPTY, 6);
                        break;
                    case Instruments.CABINDOOR_CONTROLLER:
                        UpdateStatus(txtcol8, Instruments.CABINDOOR_CONTROLLER, 7);
                        break;
                    case Instruments.CABIN_DOOR_OPEN:
                        UpdateStatus(txtcol9, Instruments.CABIN_DOOR_OPEN, 8);
                        break;
                    case Instruments.INST_CLEANING_SYSTEM_READY:
                        if (daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true)
                            txtRes1.Background = System.Windows.Media.Brushes.LightBlue;
                        else
                            txtRes1.Background = System.Windows.Media.Brushes.LightSalmon;
                        txtRes1.Text = daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_READY].ToString();
                        break;
                    case Instruments.INST_CLEANING_SYSTEM_OPERATING:
                        if (daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == true)
                            txtRes2.Background = System.Windows.Media.Brushes.LightBlue;
                        else
                            txtRes2.Background = System.Windows.Media.Brushes.LightSalmon;
                        txtRes2.Text = daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING].ToString();
                        break;
                    case Instruments.CABIN_EMPTY:
                        if (daq.DIStatus[Instruments.CABIN_EMPTY] == true)
                            txtRes3.Background = System.Windows.Media.Brushes.LightBlue;
                        else
                            txtRes3.Background = System.Windows.Media.Brushes.LightSalmon;
                        txtRes3.Text = daq.DIStatus[Instruments.CABIN_EMPTY].ToString();

                        break;

                    case Instruments.CABIN_READY_FORUSE:
                        if (daq.DIStatus[Instruments.CABIN_READY_FORUSE] == true)
                            txtRes4.Background = System.Windows.Media.Brushes.LightBlue;
                        else
                            txtRes4.Background = System.Windows.Media.Brushes.LightSalmon;
                        txtRes4.Text = daq.DIStatus[Instruments.CABIN_READY_FORUSE].ToString();
                        break;
                    case Instruments.USB_4750_POWERED_ON:
                        if (daq.DIStatus[Instruments.USB_4750_POWERED_ON] == true)
                        {
                           // usbstatus.Text = "Powered On";
                           // usbstatus.BackColor = Color.Blue;
                        }
                        else if (daq.DIStatus[Instruments.USB_4750_POWERED_ON] == false)
                        {
                          //  usbstatus.Text = "Powered Off";
                          //  usbstatus.BackColor = Color.Red;
                        }
                        break;
                }
            }

            //Port 0 data
            for (int j = 0; j < daq.RawDataPort0.Count; j++)
            {
                int val0 = (daq.RawDataPort0[j] >> j) & 0x1;
                txthex1.Text = daq.RawDataPort0[j].ToString("X");
                switch (j)
                {
                    case 0:

                        AddPictureBox(Img9, val0);
                        break;
                    case 1:

                        AddPictureBox(Img8, val0);
                        break;

                    case 2:

                        AddPictureBox(Img7, val0);
                        break;
                    case 3:

                        AddPictureBox(Img6, val0);
                        break;
                    case 4:

                        AddPictureBox(Img4, val0);
                        break;
                    case 5:

                        AddPictureBox(Img3, val0);
                        break;
                    case 6:

                        AddPictureBox(Img2, val0);
                        break;
                    case 7:

                        AddPictureBox(Img1, val0);
                        break;
                }
            }

            //pORT1 Channels
            for (int j = 0; j < daq.RawDataPort1.Count; j++)
            {
                int val0 = (daq.RawDataPort1[j] >> j) & 0x1;
                txthex2.Text = daq.RawDataPort1[j].ToString("X");
                switch (j)
                {
                    case 0:
                        AddPictureBox(Img17, val0);
                        break;
                    case 1:
                        AddPictureBox(Img16, val0);
                        break;
                    case 2:
                        AddPictureBox(Img15, val0);
                        break;
                    case 3:
                        AddPictureBox(Img14, val0);
                        break;
                    case 4:
                        AddPictureBox(Img13, val0);
                        break;
                    case 5:
                        AddPictureBox(Img12, val0);
                        break;
                    case 6:
                        AddPictureBox(Img11, val0);
                        break;
                    case 7:
                        AddPictureBox(Img10, val0);
                        break;
                }
            }
        }

        private void PatientTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        
        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            DermascopeOperation();
        }

        void DermascopeOperation()
        {
            if (btn5)
            {
                 MessageBox.Show("Otoscope Deployed.", "Serial Device", MessageBoxButton.OK, MessageBoxImage.Information);

                return;
            }

            btn1 = !btn1;
            if (btn1)
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.DEP_DERMASCOPE );
            else
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.RET_DERMASCOPE );

            ToggleButtonName(Btn1, btn1);

        }

      

        bool btninst = false;
        private void BtnClean_Click(object sender, RoutedEventArgs e)
        {
            InstCleanOperation();
        }

        void InstCleanOperation()
        {
            btninst = !btninst;
            if (btninst)
            {
              PRECONDITIONERRORCODE err=  _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.CLEAN_WASH_DRY);
              if(err == PRECONDITIONERRORCODE.NOERROR)
                {
                    BtnClean.Background = System.Windows.Media.Brushes.Blue;

                   // BtnClean.IsEnabled = false;


                }
              else
                    btninst = !btninst;

            }
            else
            {
                BtnClean.Background = System.Windows.Media.Brushes.LightGray;

            }
        }

        bool btn1 = false;
        bool btn2 = false;
        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            ThermoMeterOperation();
        }

        public void ThermoMeterOperation()
        {
            btn2 = !btn2;
            if (btn2)
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.DEP_THERMOMETER );
            else
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.RET_THERMOMETER);

            ToggleButtonName(Btn2, btn2);

        }

        void ToggleButtonName(Button Btn, bool toggle)
        {
            Dispatcher.BeginInvoke((Action)delegate ()
            {
                if (toggle)
                {
                    Btn.Content = "Retract";
                    Btn.Background = System.Windows.Media.Brushes.Blue;
                }
                else
                {
                    Btn.Content = "Deploy";
                    Btn.Background = System.Windows.Media.Brushes.LightGray;
                }
            });
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {  
            PulseOxymeter();
        }

        bool btn3 = false;
        void PulseOxymeter()
        {
            btn3 = !btn3;
            if (btn3)
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.DEP_PO );
            else
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.RET_PO );


            ToggleButtonName(Btn3, btn3);
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
           
            ChestStetoscope();
        }

        bool btn4 = false;
        void ChestStetoscope()
        {
            btn4 = !btn4;
            if (btn4)
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.DEP_CHESTSTETHOSCOPE );
            else
                _serialDevice.WriteData(DeviceList.CONTROLSW, SERIALDEVICEOPERATIONS.RET_CHESTTHOSCOPE );

            ToggleButtonName(Btn4, btn4);
        }


        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            if (btn1)
            {
                MessageBox.Show("Dermascope Deployed.", "Serial Device",MessageBoxButton.OK,MessageBoxImage.Information);

                
                return;
            }
           
            OtoscopeCamera();
        }

        bool btn5 = false;
        void OtoscopeCamera()
        {
            btn5 = !btn5;
            if (btn5)
                _serialDevice.WriteData(DeviceList.CONTROLSW, "s5de");
            else
                _serialDevice.WriteData(DeviceList.CONTROLSW, "s5re");

            ToggleButtonName(Btn5, btn5);
        }

        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {  
            string[] comports = _serialDevice.ComPortsList();
            lstPorts.ItemsSource = comports;
            lstPorts.SelectedIndex = 0;
            if (comports.Count() == 1)
            {
                txtInstPort.Text = comports[0];
                _serialDevice.InitializeInsControl(DeviceList.CONTROLSW, comports[0], 57600);
            }
            

        }

        private void BtnInstComSelect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.CONTROLSW, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE57600);
                if (res)
                    txtInstPort.Text = lstPorts.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }

        }


        private void BtnTempReading_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.TEMPREADING, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE9600);
                if (res)
                    txtTempReadingPort.Text = lstPorts.SelectedValue.ToString();

            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }

            
        }

        private void BtnPOREADING_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.POREADING, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE9600);
                if (res)
                    txtPOPort.Text = lstPorts.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }

           
        }

        private void BtnsmartCard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.SMARDCARD_DOOROPENER, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE19200);
                if (res)
                    txtsmartCardAndDoor.Text = lstPorts.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }
            
        }

        private void BtnBP_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.BLOODPRESSURE, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE9600);
                if (res)
                    txtBP.Text = lstPorts.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }
           
        }

        private void BtnHW_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.HEIGHTANDWEIGHT, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE9600);
                if (res)
                    txtHW.Text = lstPorts.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }
           
        }

        private void Btngm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.GLOCOMETER, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE9600);
                if (res)
                    txtgm.Text = lstPorts.SelectedValue.ToString();

            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }

        }

        private void Btnstc_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool res = _serialDevice.InitializeInsControl(DeviceList.CHAIRSETHOSCOPE, lstPorts.SelectedValue.ToString(), (int)SerialDeviceBoudRate.BOUDRATE9600);
                if (res)
                    txtstc.Text = lstPorts.SelectedValue.ToString();

            }
            catch (Exception ex)
            {
                statustxt.Text = ex.Message;
            }

        }




        void AddPictureBox(System.Windows.Controls.Image img,int onOff)
        {
            if (onOff ==1)
            {
                var uriSource = new Uri(@"pack://application:,,,/Resources/on.png", UriKind.RelativeOrAbsolute);
                img.Source = new BitmapImage(uriSource); ;
            }
            else
            {
                var uriSource = new Uri(@"pack://application:,,,/Resources/off.png", UriKind.RelativeOrAbsolute);
                img.Source = new BitmapImage(uriSource); ;

            }
        }
        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            // ChangeImage(Img1, false);
            // MessageBox.Show("sfs");

            int deviceNumber = 0;
            if (daq.instantDiCtrl1.Initialized || daq.instantDoCtrl1.Initialized)
            {
                // MessageBox.Show("No device be selected or device open failed!", "instantDiCtrl");
                MessageBox.Show("Device : " + daq.instantDiCtrl1.SelectedDevice.Description + " already selected. Close the application and re-open to select other device.", "Error Message");
                return;
            }

            try
            {
                deviceNumber = Convert.ToInt32(txtDeviceID.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            try
            {
                daq.Initialize(deviceNumber, this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }

            if (!daq.instantDiCtrl1.Initialized || !daq.instantDoCtrl1.Initialized)
            {
                MessageBox.Show("No device be selected or device open failed!", "instantDiCtrl");
                return;
            }

            statustxt.Text = daq.instantDiCtrl1.SelectedDevice.Description;
                
            statustxt.Text = statustxt.Text + " No of Port: " + daq.instantDiCtrl1.Features.PortCount.ToString() + " No. Of Channels " + daq.instantDiCtrl1.Features.ChannelCountMax.ToString();

          //  timer1.Interval = 100;
            timer.Start();

        }

       
        bool DisplayErrorMessage()
        {
            if (!daq.instantDiCtrl1.Initialized || !daq.instantDoCtrl1.Initialized)
            {
                MessageBox.Show("No device be selected or device open failed!", "Control Software");
                return true;
            }
            return false;
        }

        
        void ChangeImageHighLow(System.Windows.Controls.Image img, bool onOff)
        {
            Dispatcher.Invoke(() =>
            {
                
           
            if (onOff)
            {
                var uriSource = new Uri(@"pack://application:,,,/Resources/high.png", UriKind.RelativeOrAbsolute);
                img.Source = new BitmapImage(uriSource); ;
            }
            else
            {
                var uriSource = new Uri(@"pack://application:,,,/Resources/low.png", UriKind.RelativeOrAbsolute);
                img.Source = new BitmapImage(uriSource); ;

            }
            });
        }


        bool do7 = false;
        private void Img18_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            DO7();
        }

        void DO7()
        {

            do7 = !do7;
            daq.AirScrubFan(do7);
            if (do7)
                ChangeImageHighLow(Img18, true);
            else
                ChangeImageHighLow(Img18, false);
        }
        bool do6 = false;
        private void Img19_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do6 = !do6;
            daq.H202FogFan(do6);
            ChangeImageHighLow(Img19, do6);
        }

        bool do26 = false;
        private void Img26_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do26 = !do26;
            daq.H202ExhaustFan(do26);
            ChangeImageHighLow(Img26, do26);

        }

        bool do27 = false;
        private void Img27_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do27 = !do27;
            daq.DetectCabinDoorController(do27);
            ChangeImageHighLow(Img27, do27);
        }

        private void txtDeviceID_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }

        bool d020 = false;
        private void Img20_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            d020 = !d020;
            daq.UnUsedBits(0,5,d020);
            ChangeImageHighLow(Img20, d020);

        }

        bool do21 = false;
        private void Img21_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do21 = !do21;
            daq.UnUsedBits(0, 4, do21);
            ChangeImageHighLow(Img21, do21);

        }

        bool do22 = false;
        private void Img22_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do22 = !do22;
            daq.UnUsedBits(0, 3, do22);
            ChangeImageHighLow(Img22, do22);

        }

        bool do23 = false;
        private void Img23_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do23 = !do23;
            daq.UnUsedBits(0, 2, do23);
            ChangeImageHighLow(Img23, do23);
        }

        bool do24 = false;
        private void Img24_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do24 = !do24;
            daq.UnUsedBits(0, 1, do24);
            ChangeImageHighLow(Img24, do24);
        }

        bool do25 = false;
        private void Img25_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do25 = !do25;
            daq.UnUsedBits(0, 0, do25);
            ChangeImageHighLow(Img25, do25);

        }


        bool do28 = false;
        private void Img28_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do28 = !do28;
            daq.UnUsedBits(1, 5, do28);
            ChangeImageHighLow(Img28, do28);

        }

        bool do29 = false;
        private void Img29_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do29 = !do29;
            daq.UnUsedBits(1, 4, do29);
            ChangeImageHighLow(Img29, do29);

        }


        bool do30 = false;
        private void Img30_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do30 = !do30;
            daq.UnUsedBits(1, 3, do30);
            ChangeImageHighLow(Img30, do30);


        }

        bool do31 = false;
        private void Img31_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do31 = !do31;
            daq.UnUsedBits(1, 2, do30);
            ChangeImageHighLow(Img31, do31);


        }


        bool do32 = false;
        private void Img32_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do32 = !do32;
            daq.UnUsedBits(1, 1, do32);
            ChangeImageHighLow(Img32, do32);

        }

        bool do33 = false;
        private void Img33_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            do33 = !do33;
            daq.UnUsedBits(1, 0, do33);
            ChangeImageHighLow(Img33, do33);
        }

        private void ComboMicrophoneSlection_SelectionChanged(object s, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)s;
            int index = combo.SelectedIndex;
            MicrophoneSelection(index);
        }

        void MicrophoneSelection(int index, bool updateUI=false)
        {
           
            if (updateUI)
            {
                Dispatcher.BeginInvoke((Action)delegate ()
                {
                    // statustxt.Text = m.msg;
                    comboMicrophoneSlection.SelectedIndex = index;
                   
                });
            }
            else
                 _serialDevice.WriteData(DeviceList.CHAIRSETHOSCOPE, index.ToString());

             
        }
        private void ComboStethoscopeChair_SelectionChanged(object s, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)s;
            StethoscopeChairPosSelection(combo.SelectedIndex);
            
        }

        void StethoscopeChairPosSelection(int Index, bool updateUI=false)
        {
            string cmd = string.Empty;
            switch (Index)
            {
                case 0:
                    cmd = "a";
                    break;
                case 1:
                    cmd = "u";
                    break;
                case 2:
                    cmd = "d";
                    break;
                case 3:
                    cmd = "t";
                    break;
                case 4:
                    cmd = "c";
                    break;
                case 5:
                    cmd = "b";
                    break;
            }

           

            if(updateUI)
            {
                Dispatcher.BeginInvoke((Action)delegate ()
                {
                    comboStethoscopeChair.SelectedIndex = Index;
                });
            }
            else
            {
                if (cmd.Length > 0)
                    _serialDevice.WriteData(DeviceList.CHAIRSETHOSCOPE, cmd);
            }
        }
        private void DoorOpen_SelectionChanged(object s, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)s;
            SmartCard_DoorOpen(combo.SelectedIndex);
        }

        void SmartCard_DoorOpen(int index)
        {
            string cmd = "$R" + index.ToString();
            _serialDevice.WriteData(DeviceList.SMARDCARD_DOOROPENER, cmd);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            commCabinstatus.ClosePort();
        }
    }
}
