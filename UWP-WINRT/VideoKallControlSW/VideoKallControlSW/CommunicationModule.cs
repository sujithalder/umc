﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Net.NetworkInformation;

namespace VideoKallControlSW
{
    public enum ClientID
    {
        CABINSTATUS_APP,
        CABINSW_APP,
        UNKOWN_APP
    }

    public enum TestRigPannel
    {
        MOTIONDETECTOR,
        AIRINLETFAN,
        H2O2LEVELDETECTOR,
        H2O2FOGGINGFAN,
        H2O2EXHUSTFAN,
        CABINDOORCONROLLER,
        CABINDOOROPEN,
        CLEANING_STATUS,
        
        UNKNOWN
    }
    class CommunicationModule
    {
        public static string Testrigstatusformat = "TESTRIGSTATUS:{0}:{1}";
        public static string Serialdevicestatusformat = "SERIALDEVICESTATUS:{0}:{1}";
        UdpClient client;
      //  IPEndPoint endpoint = null; 
        int clientport = 4480;
        public  EventHandler<EventArgs> NotifyConnection = null;
        public EventHandler<EventArgs> NotifyTestRigCMD = null;
        Dictionary<ClientID, IPEndPoint> ClientList = new Dictionary<ClientID, IPEndPoint>();
        bool stopListening = false;
        public   void Initialize()
        {
            client = new UdpClient(clientport);

            Thread th = new Thread(new ThreadStart(RecvDataThread));
            th.Start();

            var host = Dns.GetHostEntry(Dns.GetHostName());
            string Text = "";
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                     Text += ip.ToString();
                    Text += "==";
                   
                }
            }

        //  List<IPAddress> App=  GetEndpoints();
            NotifyConnection?.Invoke(null, new SerialEventMsg(Text));

        }

        void RecvDataThread()
        {
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, 0);
            while (!stopListening)
            {
                try
                {
                                      
                    byte[] buffer = client.Receive(ref endpoint);

                    IPEndPoint endpointcopy = new IPEndPoint(IPAddress.Any, 0)
                    {
                        Address = endpoint.Address,
                        Port = endpoint.Port
                    };
                    
                    string str = Encoding.ASCII.GetString(buffer);

                    if (str.Contains("CabinStatusApp"))
                    {
                        AddOrUpdateClient(ClientID.CABINSTATUS_APP, endpointcopy);
                        UpdateCabinStatus("CONNECTEDCONTROLSW");
                    }
                    else if (str.Contains("CabinSWApp"))
                    {
                        AddOrUpdateClient(ClientID.CABINSW_APP, endpointcopy);
                        UpdateCabinSW("CONNECTEDCONTROLSW");
                    }
                    else
                        AddOrUpdateClient(ClientID.UNKOWN_APP, endpointcopy);

                   // LogMessage.Log(DateTime.Now.ToString() + " Received: " + str);
                   
                    if(str.Contains("TESTRIGCMD")|| str.Contains("TESTRIGSERIALCMD"))
                    NotifyTestRigCMD?.Invoke(null, new SerialEventMsg(str));
                    else
                    {
                        NotifyConnection?.Invoke(null, new SerialEventMsg(str));
                    }

                }
                catch(Exception ex)
                {
                    string s = ex.ToString();
                }
            }
        }
        
        void AddOrUpdateClient(ClientID id, IPEndPoint ip)
        {
            
                if (ClientList.ContainsKey(id))
            
                   ClientList[id] = ip;
                else
                 ClientList.Add(id, ip);
        }
     public   void UpdateCabinStatus(int status)
        {
             
           byte[] buffer = Encoding.Unicode.GetBytes(status.ToString());

            
            if(ClientList.ContainsKey(ClientID.CABINSTATUS_APP)&& !ClientList[ClientID.CABINSTATUS_APP].Address.ToString().Equals( "0.0.0.0"))
            client.Send(buffer, buffer.Length, ClientList[ClientID.CABINSTATUS_APP]);
        }

        public void UpdateCabinStatus(string status)
        {

            byte[] buffer = Encoding.Unicode.GetBytes(status.ToString());
 
            if (ClientList.ContainsKey(ClientID.CABINSTATUS_APP) && !ClientList[ClientID.CABINSTATUS_APP].Address.ToString().Equals("0.0.0.0"))
                client.Send(buffer, buffer.Length, ClientList[ClientID.CABINSTATUS_APP]);
        }

        public void UpdateCabinSW(string status)
        {

            byte[] buffer = Encoding.Unicode.GetBytes(status.ToString());

            if (ClientList.ContainsKey(ClientID.CABINSW_APP) && !ClientList[ClientID.CABINSW_APP].Address.ToString().Equals("0.0.0.0"))
                client.Send(buffer, buffer.Length, ClientList[ClientID.CABINSW_APP]);
        }

        public void ClosePort()
        {
            stopListening = true;
            
            client.Close();
        }


        private List<IPAddress> GetEndpoints()
    {
        List<IPAddress> AddressList = new List<IPAddress>();
        NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();
        foreach(NetworkInterface I in Interfaces)
        {
            if ((I.NetworkInterfaceType == NetworkInterfaceType.Ethernet || I.NetworkInterfaceType == NetworkInterfaceType.Wireless80211) && I.OperationalStatus == OperationalStatus.Up)
            {
                foreach (var Unicast in I.GetIPProperties().UnicastAddresses)
                {
                    if (Unicast.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        AddressList.Add(Unicast.Address);
                    }
                }
            }
        }
        return AddressList;
    }


    }

  

}
