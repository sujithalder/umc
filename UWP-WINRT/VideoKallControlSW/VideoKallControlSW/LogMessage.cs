﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace VideoKallControlSW
{
   
    class LogMessage
    {
        private static readonly string fileName = "Controlswlog.txt";
     static   bool logmsg = true;
     static   StreamWriter logwriter = null;

        static object syncobject = new object();
        public static void Log(string msg, bool append = true)
        {
            lock (syncobject)
            {

                logwriter = new StreamWriter(fileName, append);

                if (logmsg)
                {
                    logwriter.WriteLine(msg);
                }

                logwriter.Close();
                logwriter = null;
            }
        }


    }
}
