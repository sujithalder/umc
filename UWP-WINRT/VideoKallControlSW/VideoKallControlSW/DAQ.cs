﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
using Automation.BDaq;
using VideoKallControlSW;

namespace ControlSoftware
{
    /// <summary>
    /// InstrumentID's
    /// </summary>
    enum Instruments
    {
        AIR_SCRUB_FAN,
        H202_FUG_FAN,
        H202_EXHAUST_FAN,
        MOTION_DETECTOR,
        H202_FLUID_EMPTY,
        CLEAN_FLUID_EMPTY,
        WASTE_FLUID_EMPTY,
        CABINDOOR_CONTROLLER,
        CABIN_DOOR_OPEN,
        INST_CLEANING_SYSTEM_READY,
        INST_CLEANING_SYSTEM_OPERATING,
        CABIN_EMPTY,
        USB_4750_CONNECTED,
        USB_4750_POWERED_ON,
        CABIN_READY_FORUSE,
        CLEANING_STARTED,
        
        TOTAL=16

    }

    /// <summary>
    /// This class is responsible to read/write data from Digital input/output Channels
    /// </summary>
    class DAQ
    {
        //Port and Channel-> Values
        Dictionary<Instruments, bool> InstrumentDIStatus = new Dictionary<Instruments, bool>();
        // ControlSMainForm mainGUIRef = null;
       public InstantDiCtrl instantDiCtrl1 = new InstantDiCtrl();
       public InstantDoCtrl instantDoCtrl1 = new InstantDoCtrl();
        Dictionary<int, int> rawdataPort0 = new Dictionary<int, int>();
        Dictionary<int, int> rawdataPort1 = new Dictionary<int, int>();
        Dictionary<int, string> ChannelbitValuesPort0 = new Dictionary<int, string>();
        Dictionary<int, string> ChannelbitValuesPort1 = new Dictionary<int, string>();

       public EventHandler<EventArgs> UpdateBitValues;

        int currentErrorState = 0;
        CommunicationModule _comm=null;
        /// <summary>
        /// Select the device and initialize the status to false
        /// </summary>
        /// <param name="DeviceNumber"></param>
        /// <param name="mainGUI"></param>
        public  void Initialize(int DeviceNumber, object mainGUI)
        {
            //mainGUIRef = (ControlSMainForm)mainGUI;
            instantDiCtrl1.SelectedDevice= new DeviceInformation(DeviceNumber);
            instantDoCtrl1.SelectedDevice = new DeviceInformation(DeviceNumber);
            for (int i=0; i<(int)Instruments.TOTAL; i++)
            {
                AddStatus((Instruments)i, false);
            }
            AddStatus(Instruments.CABINDOOR_CONTROLLER, true);

            //bitvalue initialization
            for (int i=0; i<8; i++)
            {
                ChannelbitValuesPort0.Add(i, "0");
                ChannelbitValuesPort1.Add(i, "0");
            }
        }

        /// <summary>
        /// Returns Instruments Status
        /// </summary>
        internal Dictionary<Instruments, bool>  DIStatus
        {
            get { return InstrumentDIStatus; }
        }

        /// <summary>
        /// returns devicestatus
        /// </summary>
        /// <param name="ins"></param>
        /// <returns></returns>
      public bool DeviceStatus(Instruments ins)
        { 
            if(InstrumentDIStatus.ContainsKey(ins))
                 return InstrumentDIStatus[ins];

            return false;
        }

        public bool IsOutService
        {
            get { return currentErrorState>0?true:false; }
        }
        public string OutOfService()
        {
            string msgstr = string.Empty;
            int state = 0;
            if (DeviceStatus(Instruments.WASTE_FLUID_EMPTY))
            {
                // commCabinstatus.UpdateStatus("Warning: Out of Service");
                  msgstr += "Waste fluid full.";
                  state++;
            }

            if(DeviceStatus(Instruments.CLEAN_FLUID_EMPTY))
            {
                msgstr += "\nClean fluid empty.";
                state++;
            }


            if (state != currentErrorState )
            {
                if(state!=0)
                {
                    msgstr = "Error:Out of Service:" + msgstr;
                }
                else
                {
                    msgstr = "Cleared";
                }
                if (_comm != null)
                {
                    _comm.UpdateCabinStatus(msgstr);
                  //  _comm.UpdateCabinSW(msgstr); temporary disabled
                }
            }
            currentErrorState = state;
            return msgstr;
        }

        public CommunicationModule Comm
        {
            set { _comm = value; }
        }

        public void UpdateCleaningStatus(bool startedOrCompleted)
        {
            InstrumentDIStatus[Instruments.CLEANING_STARTED] = startedOrCompleted;
        }
            
        

     
        public bool WashReservoirStatusFull()
        {
            return DeviceStatus(Instruments.WASTE_FLUID_EMPTY);
        }

        public bool CleanSolutionReserviorStatusEmpty()
        {
            return DeviceStatus(Instruments.CLEAN_FLUID_EMPTY);
        }
        /// <summary>
        /// Read DI port state and Initialize device status
        /// </summary>
        public void ReadDI()
        {
            //  instantDiCtrl1
            byte portData = 0;
            ErrorCode err = ErrorCode.Success;
            if (instantDiCtrl1.Features.PortCount <= 0)
                return;
            //Read Port0
           err = instantDiCtrl1.Read(0, out portData);

            HandleError(err);

            //Channels of Port 0

            for (int j = 0; j < 8; ++j)
            {
                int ChannelBitsValue = (portData >> j) & 0x1;
                AddRawData0(j, portData);

                //Cabin Door Open
                if (j == 0 && ChannelBitsValue==1)
                {
                    AddStatus(Instruments.CABIN_DOOR_OPEN, true);
                }
                else if (j == 0 && ChannelBitsValue == 0)
                {
                    AddStatus(Instruments.CABIN_DOOR_OPEN, false);
                }

                //H202 FLUID EMPTY
                if (j==1 && ChannelBitsValue==1)
                    AddStatus(Instruments.H202_FLUID_EMPTY, true);
                else if (j == 1 && ChannelBitsValue == 0)
                    AddStatus(Instruments.H202_FLUID_EMPTY, false);

                if (j == 2 && ChannelBitsValue == 1)
                    AddStatus(Instruments.MOTION_DETECTOR, true);
                else if (j == 2 && ChannelBitsValue == 0)
                    AddStatus(Instruments.MOTION_DETECTOR, false);
            }

            if (instantDiCtrl1.Features.PortCount <2)
                return;

            //Read Port1
            portData = 0;
            
            err =instantDiCtrl1.Read(1, out portData);

            HandleError(err);

            for (int j = 0; j < 8; ++j)
            {
                int ChannelBitsValue = (portData >> j) & 0x1;
                AddRawData1(j, portData);

                if (j == 0 && ChannelBitsValue == 1)
                {
                    AddStatus(Instruments.USB_4750_POWERED_ON, false);
                    AddStatus(Instruments.CLEAN_FLUID_EMPTY, false);
                }
                else if (j == 0 && ChannelBitsValue == 0)
                {
                    AddStatus(Instruments.USB_4750_POWERED_ON, true);
                    AddStatus(Instruments.CLEAN_FLUID_EMPTY, true);
                }

                if (j==1 && ChannelBitsValue==0)
                {
                    AddStatus(Instruments.WASTE_FLUID_EMPTY, true);
                }
                else if(j==1 && ChannelBitsValue ==1)
                {
                    AddStatus(Instruments.WASTE_FLUID_EMPTY, false);
                }

            }

            Ins_Cleaning_System_Ready();
            Ins_Cleaning_System_Operating();
            CabinEmpty();
            CabinReadyForNextUse();
        }


        /// <summary>
        /// Device status
        /// </summary>
        void AddStatus(Instruments ins, bool val)
        {
            if (InstrumentDIStatus.ContainsKey(ins))
            {
                InstrumentDIStatus[ins] = val;
            }
            else
                InstrumentDIStatus.Add(ins, val);
        }

        /// <summary>
        /// Utility for device test port
        /// </summary>
        /// <param name="chan"></param>
        /// <param name="val"></param>
        void AddRawData0(int chan, int val)
        {
            if (rawdataPort0.ContainsKey(chan))
            {
                rawdataPort0[chan] = val;
            }
            else
                rawdataPort0.Add(chan, val);
        }

        /// <summary>
        /// Utility for Device test -POrt1
        /// </summary>
        /// <param name="chan"></param>
        /// <param name="val"></param>
        void AddRawData1(int chan, int val)
        {
            if (rawdataPort1.ContainsKey(chan))
            {
                rawdataPort1[chan] = val;
            }
            else
                rawdataPort1.Add(chan, val);
        }

        public Dictionary<int, int> RawDataPort0
        {
            get { return rawdataPort0; }
        }



        public Dictionary<int, int> RawDataPort1
        {
            get { return rawdataPort1; }
        }
        /// <summary>
        /// Update bit value of the selected Channel
        /// </summary>
        /// <param name="portid"></param>
        /// <param name="channelID"></param>
        /// <param name="value"></param>
        public void SelectedDO(int portid,int channelID, int value)
        {
            if ((value < 0  || value > 1 ) || (channelID<0 || channelID>7))
                return;
            if(portid==0)
            ChannelbitValuesPort0[channelID]= value.ToString();
            else if(portid==1)
                ChannelbitValuesPort1[channelID]=value.ToString() ;
        }

        /// <summary>
        /// return Potdata
        /// </summary>
        /// <param name="portID"></param>
        /// <returns></returns>
        public int PortDataDO(int portID)
        {
            List<String> values = null;
            if(portID==0)
                 values= ChannelbitValuesPort0.Values.ToList();
            else
                values = ChannelbitValuesPort1.Values.ToList();

            values.Reverse();
            string st = string.Empty;
            for (int a=0;a<values.Count();a++)
            {
                st += values[a];
            }

            return Convert.ToInt32(st, 2);
             
        }
     
        
        /// <summary>
        /// Device-AirScrubFan
        /// </summary>
       public void AirScrubFan(bool onOff)
        {
            UpdateChannelState(0, 7, onOff);

            WriteDO(0);
            AddStatus(Instruments.AIR_SCRUB_FAN, onOff);
        }

        /// <summary>
        /// Device-Fogfan
        /// </summary>
        /// <param name="onOff"></param>
        public void H202FogFan(bool onOff)
        {
            UpdateChannelState(0, 6, onOff);
            WriteDO(0);

            AddStatus(Instruments.H202_FUG_FAN, onOff);
        }

        /// <summary>
        /// Device ExhaustFan
        /// </summary>
        /// <param name="onOff"></param>
        public void H202ExhaustFan( bool onOff)
        {
            UpdateChannelState(1, 7, onOff);
            WriteDO(1);
            AddStatus(Instruments.H202_EXHAUST_FAN, onOff);
        }

        /// <summary>
        /// device-cabin door controller
        /// </summary>
        /// <param name="onOff"></param>
        public void DetectCabinDoorController(bool onOff)
        {
            UpdateChannelState(1, 6, onOff);
            WriteDO(1);
            if(onOff)
            AddStatus(Instruments.CABINDOOR_CONTROLLER, false);
            else
            AddStatus(Instruments.CABINDOOR_CONTROLLER, true);
        }

        public void UnUsedBits( int portId,int channelID, bool onOff)
        {
            UpdateChannelState(portId,channelID, onOff);
            WriteDO(portId);
        }

        /// <summary>
        /// Derive result based on the device status
        /// </summary>
        void Ins_Cleaning_System_Ready()
        {
          if ( (InstrumentDIStatus[Instruments.AIR_SCRUB_FAN]==true )&&
                (InstrumentDIStatus[Instruments.H202_FUG_FAN] ==false )&&
               ( InstrumentDIStatus[Instruments.H202_EXHAUST_FAN]==false) &&
                (InstrumentDIStatus[Instruments.MOTION_DETECTOR]==false) &&
               ( InstrumentDIStatus[Instruments.H202_FLUID_EMPTY]==false) &&
                (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY]==true) &&
               ( (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)||
              ( InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == true)) &&
                ( InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == true)&&
                 (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false))
            {
                AddStatus(Instruments.INST_CLEANING_SYSTEM_READY, false); 
            }
          else
                AddStatus(Instruments.INST_CLEANING_SYSTEM_READY, true);
        }

        /// <summary>
        /// Cleaning System Operating
        /// </summary>
        void Ins_Cleaning_System_Operating()
        {
            if ((InstrumentDIStatus[Instruments.AIR_SCRUB_FAN] == false) &&
                  (InstrumentDIStatus[Instruments.H202_FUG_FAN]  == true) &&
                 (InstrumentDIStatus[Instruments.H202_EXHAUST_FAN] == false) &&
                  (InstrumentDIStatus[Instruments.MOTION_DETECTOR] == false) &&
                 (InstrumentDIStatus[Instruments.H202_FLUID_EMPTY] == false) &&
                  (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY] == false) &&
                 (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)&&
                  (InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == true) &&
                   (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false)&&
                     (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true))
            {
                AddStatus(Instruments.INST_CLEANING_SYSTEM_OPERATING, true);
            }
            else
                AddStatus(Instruments.INST_CLEANING_SYSTEM_OPERATING, false);
        }

        /// <summary>
        /// CABIN is Empty truth table
        /// </summary>
        void CabinEmpty()
        {
            bool condn1 = false;
            bool condn2 = false;
            bool condn3 = false;
            bool condn4 = false;
            //1
            if ((InstrumentDIStatus[Instruments.AIR_SCRUB_FAN] == true) &&
                (InstrumentDIStatus[Instruments.H202_FUG_FAN] == false) &&
               (InstrumentDIStatus[Instruments.H202_EXHAUST_FAN] == false) &&
                (InstrumentDIStatus[Instruments.MOTION_DETECTOR] == false) &&
               (InstrumentDIStatus[Instruments.H202_FLUID_EMPTY] == false) &&
                (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY] == false) &&
               ((InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false) ||
              (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)) &&
                (InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == true) &&
                 (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false)&&
                  (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true) &&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == false))  
            {
                condn1 = true;
            }
            
            //2
            if ((InstrumentDIStatus[Instruments.AIR_SCRUB_FAN] == false) &&
               (InstrumentDIStatus[Instruments.H202_FUG_FAN] == false) &&
              (InstrumentDIStatus[Instruments.H202_EXHAUST_FAN] == false) &&
               (InstrumentDIStatus[Instruments.MOTION_DETECTOR] == false) &&
              (InstrumentDIStatus[Instruments.H202_FLUID_EMPTY] == false) &&
               (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY] == false) &&
              ((InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false) ||
             (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)) &&
               (InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == true) &&
                (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false)&&
                 (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true) &&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == false) )
            {
                condn2 = true;
            }

            //3
            if ((InstrumentDIStatus[Instruments.AIR_SCRUB_FAN] == false) &&
              (InstrumentDIStatus[Instruments.H202_FUG_FAN] == true) &&
             (InstrumentDIStatus[Instruments.H202_EXHAUST_FAN] == false) &&
              (InstrumentDIStatus[Instruments.MOTION_DETECTOR] == false) &&
             (InstrumentDIStatus[Instruments.H202_FLUID_EMPTY] == false) &&
              (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY] == false) &&
             ((InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false) ||
            (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)) &&
              (InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == true) &&
               (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false)&&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true) &&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == true) )
            {
                condn3 = true;
            }

            //4

            if ((InstrumentDIStatus[Instruments.AIR_SCRUB_FAN] == true) &&
             (InstrumentDIStatus[Instruments.H202_FUG_FAN] == false) &&
            (InstrumentDIStatus[Instruments.H202_EXHAUST_FAN] == false) &&
             (InstrumentDIStatus[Instruments.MOTION_DETECTOR] == false) &&
            (InstrumentDIStatus[Instruments.H202_FLUID_EMPTY] == true) &&
             (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY] == false) &&
            ((InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false) ||
           (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)) &&
             (InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == false) &&
              (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false)&&
               (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true) &&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == false) )
            {
                condn4 = true;
            }


            if(condn1|| condn2|| condn3|| condn4)
            {
                AddStatus(Instruments.CABIN_EMPTY, true);
            }
            else
                AddStatus(Instruments.CABIN_EMPTY, false);
        }

        /// <summary>
        /// CABIN READY FOR NEXT USER TRUTHTABLE
        /// </summary>
        void CabinReadyForNextUse()
        {
            if ((InstrumentDIStatus[Instruments.AIR_SCRUB_FAN] == true) &&
                  (InstrumentDIStatus[Instruments.H202_FUG_FAN] == false) &&
                 (InstrumentDIStatus[Instruments.H202_EXHAUST_FAN] == false) &&
                  (InstrumentDIStatus[Instruments.MOTION_DETECTOR] == false) &&
                 (InstrumentDIStatus[Instruments.H202_FLUID_EMPTY] == false) &&
                  (InstrumentDIStatus[Instruments.CLEAN_FLUID_EMPTY] == false) &&
                 ((InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false) ||
                (InstrumentDIStatus[Instruments.WASTE_FLUID_EMPTY] == false)) &&
                  (InstrumentDIStatus[Instruments.CABINDOOR_CONTROLLER] == true) &&
                   (InstrumentDIStatus[Instruments.CABIN_DOOR_OPEN] == false)&&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true)&&
                (InstrumentDIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == false) &&
                (InstrumentDIStatus[Instruments.CABIN_EMPTY] == true))
            {
                AddStatus(Instruments.CABIN_READY_FORUSE, true);
            }
            else
                AddStatus(Instruments.CABIN_READY_FORUSE, false);
        }


        /// <summary>
        /// Update the Channel bit values
        /// </summary>
        /// <param name="portid"></param>
        /// <param name="channelid"></param>
        /// <param name="onOff"></param>
        public void UpdateChannelState(int portid,int channelid,bool onOff)
        {
           // if (onOff)
          //       SelectedDO(portid, channelid, 1);
          //  else
           //      SelectedDO(portid, channelid, 0);
            UpdatedChannelBitValue(portid, channelid, onOff);
        }

        public void WriteChanneldata(int portid, int chanid,bool onoff)
        {
            UpdatedChannelBitValue(portid, chanid, onoff);
            WriteDO(portid);
        }
        public void UpdatedChannelBitValue(int portid, int channelid, bool onOff)
        {
            if (onOff)
                SelectedDO(portid, channelid, 1);
            else
                SelectedDO(portid, channelid, 0);
            string val;
            if (portid == 0)
            {
              // lblDO0.Text 
                    val= PortDataDO(portid).ToString("X");
            }
            else
            {
              //  lblDO1.Text 
                    val= PortDataDO(portid).ToString("X");
            }

             UpdateBitValues?.Invoke(this, new BitValueArgs(portid, val));//


        }

        /// <summary>
        /// Write data to Device
        /// </summary>
        /// <param name="portID"></param>
        public void WriteDO(int portID)
        {
            ErrorCode err = instantDoCtrl1.Write(portID, (byte)PortDataDO(portID));
            if (err != ErrorCode.Success)
            {
                HandleError(err);
            }
        }

        /// <summary>
        /// Handle Device read/write errors
        /// </summary>
        /// <param name="err"></param>
        private void HandleError(ErrorCode err)
        {
            if ((err >= ErrorCode.ErrorHandleNotValid) && (err != ErrorCode.Success))
            {
                UpdateBitValues?.Invoke(this, new BitValueArgs(-1, "Sorry ! Some errors happened, the error code is: " + err.ToString()));//
                //MessageBox.Show();
            }
        }

    }

    public class BitValueArgs: EventArgs
    {
        public BitValueArgs(int p,string arg)
        {
            msg = arg;
            port = p;
        }
        public string msg;
        public int port;
    }
}
