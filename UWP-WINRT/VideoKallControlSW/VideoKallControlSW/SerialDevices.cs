﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Threading;
using ControlSoftware;
namespace VideoKallControlSW
{
    enum DeviceList
    {
        CONTROLSW,
        POREADING,
        TEMPREADING,
        SMARDCARD_DOOROPENER,
        BLOODPRESSURE,
        HEIGHTANDWEIGHT,
        GLOCOMETER,
        CHAIRSETHOSCOPE,
    }
    enum SerialDeviceBoudRate
    {
        BOUDRATE57600 = 57600,
        BOUDRATE9600= 9600,
        BOUDRATE19200= 19200
    }

    enum SERIALDEVICEOPERATIONS
    {
        CLEAN_WASH_DRY,
        DEP_SH_HW,
        RET_SH_HW,
        DEP_SEAT_HW,
        RET_SEAT_HW,
        DEP_DERMASCOPE,
        RET_DERMASCOPE,
        DEP_OTOSCOPE,
        RET_OTOSCOPE,
        DEP_THERMOMETER,
        RET_THERMOMETER,
        DEP_SPIROMETER,
        RET_SPIROMETER,
        DEP_GLUCOMETER,
        RET_GLUCOMETER,
        DEP_PO,
        RET_PO,
        DEP_BP,
        RET_BP,
        SELECTMICROFONE,
        SELECT_CHAIRPOSITION,
        DEP_EKG,
        RET_EKG,
        OPENDOOR,
        CLOSEDOOR,
        AIRINLETFAN,
        H2O2FOGFAN,
        H2O2EXHUSTFAN,
        CABINDOORCONTROLLER,
        CHESTSTHETHOSCOPE,
        LUNGSSTHETHOSCOPE,
        DEP_CHESTSTETHOSCOPE,
        RET_CHESTTHOSCOPE,
        UNKNOWN,

        //CLEAN_WASH_DRY,
        //DEP_DERMASCOPE,
        //RET_DERMASCOPE,
        //DEP_THERMOMETER,
        //RET_THERMOMETER,
        //DEP_PO,
        //RET_PO,
        //DEP_CHESTSTETHOSCOPE,
        //RET_CHESTTHOSCOPE,
        //AIRINLETFAN,
        //H2O2FOGFAN,
        //H2O2EXHUSTFAN,
        //CABINDOORCONTROLLER,
        //UNKNOWN,

    }



    enum PRECONDITIONERRORCODE
    {
        CABINISNOTEMPTY,
        CLEANSOLUTIONEMPTY,
        WASTEWATERFULL,
        CABINDOOROPEN,
        NOERROR
    }
     
    class SerialDeviceMgr
    {
        Dictionary<DeviceList, SerialDevices> SerialDevicceList = new Dictionary<DeviceList, SerialDevices>();
        public static EventHandler<EventArgs> Notify = null;
      

        private  DAQ _daq=null;
        public bool InitializeInsControl(DeviceList dev,string portName,int boudrate)
        {
            SerialDevices  serialDev= new SerialDevices();
            SerialDevicceList[dev] = serialDev;
            return serialDev.InitializeInsControl(portName, boudrate);
        }

        public string[] ComPortsList()
        {
            return SerialDevices.ComPortsList();
        }

       

        public PRECONDITIONERRORCODE WriteData(DeviceList dv, SERIALDEVICEOPERATIONS cmdin)
        {
            PRECONDITIONERRORCODE ret = PRECONDITIONERRORCODE.NOERROR;
            string cmd = string.Empty;
            switch (cmdin)
            {
                case SERIALDEVICEOPERATIONS.CLEAN_WASH_DRY:
                ret = StartCleaningWithPreCondtions();
                    Notify?.Invoke(this, new SerialEventMsg(ret.ToString()));
                    break;
                case SERIALDEVICEOPERATIONS.DEP_DERMASCOPE:
                    cmd = "s1de";
                    break;
                case SERIALDEVICEOPERATIONS.RET_DERMASCOPE:
                    cmd = "s1re";
                    break;
                case SERIALDEVICEOPERATIONS.DEP_THERMOMETER:
                    cmd = "s2de";
                    break;
                case SERIALDEVICEOPERATIONS.RET_THERMOMETER:
                    cmd = "s2re";
                    break;
                case SERIALDEVICEOPERATIONS.DEP_PO:
                    cmd="s3de";
                        break;
                case SERIALDEVICEOPERATIONS.RET_PO:
                    cmd = "s3re";
                        break;
                case SERIALDEVICEOPERATIONS.DEP_CHESTSTETHOSCOPE:
                    cmd = "s4de";
                    break;
                case SERIALDEVICEOPERATIONS.RET_CHESTTHOSCOPE:
                    cmd = "s4re";
                    break;


            }
            
            if(cmd.Length>0)
                WriteData(dv, cmd);
            return ret;
        }

        public void WriteData(DeviceList dv, string cmd)
        {
            if(SerialDevicceList.ContainsKey(dv) && cmd.Length>0)
            SerialDevicceList[dv].WriteData(cmd);
        }

       public PRECONDITIONERRORCODE StartCleaningWithPreCondtions()
        {
             
            if (_daq.DeviceStatus(Instruments.CLEAN_FLUID_EMPTY))
                return PRECONDITIONERRORCODE.CLEANSOLUTIONEMPTY;

            if (_daq.DeviceStatus(Instruments.WASTE_FLUID_EMPTY))
                return  PRECONDITIONERRORCODE.WASTEWATERFULL;

            if (!_daq.DeviceStatus(Instruments.CABIN_EMPTY))
                return PRECONDITIONERRORCODE.CABINISNOTEMPTY;

            if (_daq.DeviceStatus(Instruments.CABIN_DOOR_OPEN))
                return PRECONDITIONERRORCODE.CABINDOOROPEN;

            //disable the door
            WriteData(DeviceList.SMARDCARD_DOOROPENER, "$R0");

            if (SerialDevicceList.ContainsKey(DeviceList.CONTROLSW))
                SerialDevicceList[DeviceList.CONTROLSW].WriteData("s0we");

            return PRECONDITIONERRORCODE.NOERROR; 
        }


        public DAQ Daq
        {
            set { _daq = value; }
        }
    }

    class SerialDevices
    {
        SerialPort _serialDevice = new SerialPort();
        Queue<string> portData = new Queue<string>();
        DispatcherTimer readTimer = new DispatcherTimer();
       // bool stopTimer = false;
        public bool InitializeInsControl(string portname,int boudrate)
        {
            bool status = false;
            try
            {
                if (_serialDevice.IsOpen)
                    _serialDevice.Close();

                _serialDevice.PortName = portname;
                _serialDevice.BaudRate = boudrate;
                _serialDevice.DataReceived += _serialDevice_DataReceived;
                _serialDevice.ReadTimeout = 1000;
                _serialDevice.NewLine = "\r\n";

                _serialDevice.Parity = Parity.None;
                _serialDevice.DataBits = 8;
                _serialDevice.StopBits = StopBits.One;

                _serialDevice.Handshake = Handshake.None;
                _serialDevice.Open();

                readTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                readTimer.Tick += ReadTimer_Tick;
               SerialDeviceMgr.Notify?.Invoke(this, new SerialEventMsg("InitializeInsControl"));
                status = true;
            }
            catch(Exception ex)
            {
                if (_serialDevice.IsOpen)
                    _serialDevice.Close(); 
                SerialDeviceMgr.Notify?.Invoke(this, new SerialEventMsg(ex.ToString()));
            }

            return status;

        }

        private void ReadTimer_Tick(object sender, EventArgs e)
        {
            try
            {

                if (!readTimer.IsEnabled)
                    readTimer.Stop();
                string msg = "";
                if(portData.Count()>0)
                  msg =  portData.Dequeue();


                if(msg.Length>0)
               SerialDeviceMgr.Notify?.Invoke(this, new SerialEventMsg(msg));
                
                readTimer.Start();
            }
            catch (Exception ex)
            {
                SerialDeviceMgr.Notify?.Invoke(this, new SerialEventMsg(DateTime.Now.ToString() + ex.Message.ToString()));

              //  SerialDeviceMgr.Notify?.Invoke(this, new SerialEventMsg(ex.ToString()));
                readTimer.Start();
            }
        }

        private void _serialDevice_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (!readTimer.IsEnabled)
                readTimer.Stop();

            SerialPort sp = (SerialPort)sender;
            //    string indata = sp.ReadExisting();

            byte[] data = new byte[1024];
            int bytesRead = _serialDevice.Read(data, 0, data.Length);
            string indata = Encoding.ASCII.GetString(data, 0, bytesRead);

            portData.Enqueue(indata);


            if (SerialDeviceMgr.Notify != null)
                SerialDeviceMgr.Notify(this, new SerialEventMsg("Response received:  "));

        }

     
        public void WriteData(string cmd)
        {
            try
            {
                if(!readTimer.IsEnabled)
                 readTimer.Start();
                _serialDevice.Write(cmd);

                if (SerialDeviceMgr.Notify != null)
                    SerialDeviceMgr.Notify(this, new SerialEventMsg(cmd));

            }
            catch (Exception ex)
            {
                if (SerialDeviceMgr.Notify != null)
                    SerialDeviceMgr.Notify(this, new SerialEventMsg(ex.ToString()));
            }

        }

      public static  string[] ComPortsList()
        {
          return  SerialPort.GetPortNames();
        }
        
    }

   public class SerialEventMsg: EventArgs
    {
        public SerialEventMsg(string m)
        {
            msg = m;
        }
        public string msg;
    }
}
