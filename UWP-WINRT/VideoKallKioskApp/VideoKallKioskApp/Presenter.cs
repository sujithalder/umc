﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoKallKioskApp
{
    public class Presenter
    {
        public delegate void Navigatemgt(int pageid);
        Navigatemgt nanmgt;
        public Presenter()
        {
            _AvailableSlots.Add("10-1-18 9AM");
            _AvailableSlots.Add("10-1-18 9:30AM");
            _AvailableSlots.Add("10-1-18 10AM");
            _AvailableSlots.Add("10-1-18 10:30AM");
            _AvailableSlots.Add("10-1-18 11 AM");
            _AvailableSlots.Add("10-2-18 9AM");
            _AvailableSlots.Add("10-3-18 9AM");
        }
        public void Navigatedeligate(Navigatemgt dl)
        {
            nanmgt = new Navigatemgt( dl);
        }

        public void NavigateTo(int pageid)
        {
            nanmgt?.Invoke(pageid);
        }

        public string HostPageMsg1
        {
           get { return "Do you have appointment?"; }
        }

        public string ApptPage1Text1
        {
            get { return "Please scan the QR Code."; }
        }

        public string ApptPageText2
        {
            get { return "Or \n Enter the phone number."; }
        }
        public string AptNoMsg1
        {
            get { return "Do you have account with ClinicStop?"; }
        }

        public string SignInMsg
        {
            get { return "SignIn"; }
        }

        public string EmailID
        {
            get { return "Email Address"; }
        }
        

        long ? _phoneNo =1234567890;
       public string TxtPhoneNo
        {
           get {
                if (_phoneNo == null)
                    return "";
                else
                    return FormatPhoneNo(_phoneNo.ToString()); }
            set
            {
                string tx = value;
                tx = tx.Replace("-", "");
                tx = tx.Replace("(", "");
                tx = tx.Replace(")", "");
                tx = tx.Replace(" ", "");
              
                _phoneNo = Convert.ToUInt32(tx);
            }
            
        }

     public   string FormatPhoneNo(string phoneNo)
        {
            
            string tx = phoneNo;
            //if (tx.Contains("-"))
            tx = tx.Replace("-", "");
            tx = tx.Replace("(", "");
            tx = tx.Replace(")", "");
            tx = tx.Replace(" ", "");
            if (tx.Length > 3 && tx.Length < 7)
            {
                tx = String.Format("{0}-{1}", tx.Substring(0, 3), tx.Substring(3));// (TxtPhoneNo.Text

            }
            else if (tx.Length > 6 && tx.Length<11)
            {
                tx = String.Format("({0}) {1}-{2}", tx.Substring(0, 3), tx.Substring(3, 3), tx.Substring(6));// (TxtPhoneNo.Text

            }
            return tx;
        }

      public string  ConfirmationPageTxt
        {
            get { return "Please Confirm the details."; }
        }

      public string  SlotSelectionText1
        {
            get { return "Do you want to buy Priority Pass?"; }
        }
        public string GenderTxt
        {
            get { return "Gender"; }
        }

        public string TxtNote
        {
            get { return "Note:\n 1. $55 will charge for first 10 minutes and there after per minute charges \n" +
                    " 2. $5 will charge for Priority pass \n 3. Cancellation/No show will be charged in full."; }
        }

        string _emailid = string.Empty;
        public string CustomerEmailID
        {
            set { _emailid = value; }
            get { return _emailid; }
        }

        public string LastName
        {
            get { return "Last Name"; }
        }

        public string FirstName
        {
            get { return "First Name"; }
        }

        public string MiddleName
        {
            get { return "Middle Name"; }
        }
        public string PersonalInfotxt1
        {
            get { return "Primary Information"; }
        }

        string _customerName = "Steven Ford";
        public string CustomerName
        {
            get { return _customerName; }
        }

       public string Addresstxt1
        {
            get { return "Address1"; }
        }

        public string Addresstxt2
        {
            get { return "Address2"; }
        }

        public string Addresstxt3
        {
            get { return "City"; }
        }

        public string CabinAddressTxt
        {
            get { return "P.O.BOX 60841\n POTOMAC\nPOTOMAC       \n 20859\n MD"; }
        }
         

        public string CabinPhoneNo
        {
            get { return "Tel: 1-805-233 7844\nFAX: 1-801-812 8659\nEmail: Info@videokall.com"; }
        }
        public string Addresstxt4
        {
            get { return "State"; }
        }
        public string Addresstxt5
        {
            get { return "Zip Code"; }
        }

        public string InsuranceTxt1
        {
            get { return "Subscriber's Name"; }
        }

        public string InsuranceTxt2
        {
            get { return "Insurance ID"; }
        }
        public string InsuranceTxt3
        {
            get { return "Contact no"; }
        }
        public string InsuranceTxt4
        {
            get { return "SSN"; }
        }
        public string InsuranceTxt5
        {
            get { return "Insurance Provider"; }
        }
        public string InsuranceTxt6
        {
            get { return "Zip Code"; }
        }


        string emergencyContactName = "Contact Name";
        public string EmergencyContactName
        {
            get { return emergencyContactName; }
        }
        string _cabinID = "C0005";
        public string CabinID
        {
            get { return _cabinID; }
        }

        string _date = "09-15-2018";
        public string ApptDate
        {
            get { return _date; }
        }

        public int FontsizeValue
        {
            get { return 20; }
        }

        public string CabinIDtxt
        {
            get { return "C0005"; }
        }
        public int BtnWidht
        {
            get { return 150; }
        }

     public int  BtnFontsize
        {
            get { return 20; }
        }

        public int MarginValue
        {
            get { return 5; }
        }

        
            public string PaymentText
        {
            get { return "Please swipe the card to complete the payment."; }
        }

          ObservableCollection<string> _AvailableSlots = new ObservableCollection<string>();

        public ObservableCollection<string> AvailableSlots
        {
            get { return this._AvailableSlots; }
        }
        
         public int FontsMedicalrecord
        {
            get { return 14; }
        }
        public int FontsizeHealth
        {
            get { return 20; }
        }
        public int BtnFontsize1
        {
            get { return 18; }
        }
    }
}
