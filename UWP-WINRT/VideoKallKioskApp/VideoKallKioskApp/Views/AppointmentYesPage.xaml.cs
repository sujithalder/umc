﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using VideoKallKioskApp.Views;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Devices;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VideoKallKioskApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AppointmentYesPage : Page
    {
        public AppointmentYesPage()
        {
            this.InitializeComponent();

            PlayDemo();
        }

        public static int PageID
        {
            get { return 2; }
        }
        static Presenter _presenter = null;
        public static Presenter PresenterObject
        {
            set { _presenter = value; }
        }

        private void BTNTEST_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(HostPage.PageID);
        }

        private void BtnSubmit_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(ConfirmationPage.PageID);
        }

        private void BtnCance_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(HostPage.PageID);
        }

        private void TxtPhoneNo_KeyUp(object sender, KeyRoutedEventArgs e)
        {
           
            TxtPhoneNo.Text = _presenter.FormatPhoneNo(TxtPhoneNo.Text);
            TxtPhoneNo.Select(TxtPhoneNo.Text.Length, 0);
        }

        private void TxtPhoneNo_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            string s = e.Key.ToString();
            if (s.ToUpper().Equals("ENTER") || s.ToUpper().Equals("BACK"))
            {
                e.Handled = false;
                return;
            }
            else if (s.ToUpper().Contains("NUMBER"))
            {
                s = s.Substring(s.Length - 1);
                Regex _regex = new Regex("[^0-9.-]+");
                bool b = _regex.IsMatch(s);

                e.Handled = b;
            }
            else
                e.Handled = true;
            
           
        }
        MediaPlayer mediaPlayer = null;
        void PlayDemo()
        {
           
            mediaPlayer = new MediaPlayer();
            mediaPlayer.Source = MediaSource.CreateFromUri(new Uri("ms-appx:///Assets/Boat_13.mp4"));
            
            mediaPlayer.Volume = 100;


            _mediaPlayerElement.SetMediaPlayer(mediaPlayer);

        }

        bool _paypause = false;
        private   void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            _paypause = !_paypause;
            if (mediaPlayer == null)
                PlayDemo();

            if (_paypause)
            {
                mediaPlayer.Play();
                BtnPlay.Content = "Pause";
            }
            else
            {
                mediaPlayer.Pause();
                BtnPlay.Content = "Play";
            }

        }
    }
}
