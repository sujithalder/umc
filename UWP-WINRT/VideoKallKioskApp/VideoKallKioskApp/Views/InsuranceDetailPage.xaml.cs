﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VideoKallKioskApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InsuranceDetailPage : Page
    {
        public InsuranceDetailPage()
        {
            this.InitializeComponent();
        }


        public static int PageID
        {
            get { return 9; }
        }

        static Presenter _presenter = null;
        public static Presenter PresenterObject
        {
            set { _presenter = value; }
            get { return _presenter; }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(8);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(PaymentPage.PageID);
        }
    }
}
