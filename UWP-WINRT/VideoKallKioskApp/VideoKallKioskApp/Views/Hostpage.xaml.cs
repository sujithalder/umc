﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using VideoKallKioskApp.Views;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VideoKallKioskApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HostPage : Page
    {
        public HostPage()
        {
            this.InitializeComponent();
            HostPageDatacontext.DataContext = _presenter;
        }


       public static int PageID
        {
            get { return 1; }
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(AppointmentYesPage.PageID);
        }
       static  Presenter _presenter = null;
        public static Presenter PresenterObject
        {
            set { _presenter = value; }
            get { return _presenter; }
        }

        private void BtnNO_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(AppointmentNOPage.PageID);
        }
    }
}
