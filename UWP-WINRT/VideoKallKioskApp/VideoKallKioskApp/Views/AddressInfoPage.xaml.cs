﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace VideoKallKioskApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddressInfoPage : Page
    {
        public AddressInfoPage()
        {
            this.InitializeComponent();
        }

        public static int PageID
        {
            get { return 8; }
        }

        static Presenter _presenter = null;
        public static Presenter PresenterObject
        {
            set { _presenter = value; }
            get { return _presenter; }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(7);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            _presenter?.NavigateTo(InsuranceDetailPage.PageID);
        }

        private void TxtPhoneNo_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            string s = e.Key.ToString();
            if (s.ToUpper().Equals("ENTER") || s.ToUpper().Equals("BACK"))
            {
                e.Handled = false;
                return;
            }
            else if (s.ToUpper().Contains("NUMBER"))
            {
                s = s.Substring(s.Length - 1);
                Regex _regex = new Regex("[^0-9.-]+");
                bool b = _regex.IsMatch(s);

                e.Handled = b;
            }
            else
                e.Handled = true;

        }

        private void TxtPhoneNo_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            TxtPhoneNo.Text = _presenter.FormatPhoneNo(TxtPhoneNo.Text);
            TxtPhoneNo.Select(TxtPhoneNo.Text.Length, 0);
        }
    }
}
