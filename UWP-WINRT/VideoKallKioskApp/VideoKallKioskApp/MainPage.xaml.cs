﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using VideoKallKioskApp.Views;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace VideoKallKioskApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Presenter _presenter = new Presenter();
        int _currentPage = 0;
        public MainPage()
        {
            this.InitializeComponent();
            ApplicationView view = ApplicationView.GetForCurrentView();
            view.TryEnterFullScreenMode();
            _presenter.Navigatedeligate(NavigateTo);
            HostPage.PresenterObject = _presenter;
            AppointmentYesPage.PresenterObject = _presenter;
            ConfirmationPage.PresenterObject = _presenter;
            AppointmentNOPage.PresenterObject = _presenter;
            RegistrationPage.PresenterObject = _presenter;
            SlotSelectionPage.PresenterObject = _presenter;
            PrimaryInformationPage.PresenterObject = _presenter;
            AddressInfoPage.PresenterObject = _presenter;
            InsuranceDetailPage.PresenterObject = _presenter;
            WelcomePge.PresenterObject = _presenter;
            HealthHabits.PresenterObject = _presenter;
            HealthmaintenancePage.PresenterObject = _presenter;
            MedicalHistorypage.PresenterObject = _presenter;
            //
            PaymentPage.PresenterObject = _presenter;
            rootgrid.DataContext = _presenter;
            NavigateTo(0);
        }

        private void BtnBackwdNav_Click(object sender, RoutedEventArgs e)
        {
            _currentPage--;
            if (_currentPage <= 0)
                _currentPage = 0;
            NavigateTo(_currentPage);
            // NavigateMgt.Navigate(typeof(HostPage));
        }

        private void BtnFwdNav_Click(object sender, RoutedEventArgs e)
        {
            _currentPage++;
            if (_currentPage >=14)
                _currentPage = 14;
            NavigateTo(_currentPage); 
        //    NavigateMgt.Navigate(typeof(AppointmentYesPage));
        }

        public void NavigateTo(int pageID)
        {
            _currentPage = pageID;
            switch (pageID)
            {
                case 0:
                    
                         NavigateMgt.Navigate(typeof(WelcomePge));
                    break;
                   
                case 1:
                    NavigateMgt.Navigate(typeof(HostPage));
                    break;
                case 2:
                    NavigateMgt.Navigate(typeof(AppointmentYesPage));
                    break;
                case 3:                    
                        NavigateMgt.Navigate(typeof(ConfirmationPage));
                    break;

                case 4:
                    NavigateMgt.Navigate(typeof(AppointmentNOPage));
                    break;

                case 5:
                    NavigateMgt.Navigate(typeof(RegistrationPage));
                    break;

                case 6:
                    NavigateMgt.Navigate(typeof(SlotSelectionPage));
                    break;

                case 7:
                    NavigateMgt.Navigate(typeof(PrimaryInformationPage));
                    break;

                case 8:
                    NavigateMgt.Navigate(typeof(AddressInfoPage));
                    break;
                case 9:
                    NavigateMgt.Navigate(typeof(InsuranceDetailPage));
                    break;
                case 10:
                    NavigateMgt.Navigate(typeof(PaymentPage));
                    
                    break;

                case 11:
                    NavigateMgt.Navigate(typeof(HealthHabits));
                    
                    break;
                case 12:
                    NavigateMgt.Navigate(typeof(HealthmaintenancePage));
                    
                    break;
                case 13:
                    NavigateMgt.Navigate(typeof(MedicalHistorypage));
                    break;
            }

        }
    }
}
