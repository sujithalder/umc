﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using CommunicationModule;
using Windows.UI.Popups;
using System.Threading.Tasks;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace slideshow
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
         
      //  int state = 1;
        CommunicationLib controlpccomm = new CommunicationLib();
        DispatcherTimer WatchDogtimer = new DispatcherTimer();
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView view = ApplicationView.GetForCurrentView();
                 view.TryEnterFullScreenMode();

            WatchDogtimer.Interval = TimeSpan.FromMilliseconds(1000);
            WatchDogtimer.Tick += WatchDogtimer_Tick; ;
        }

        int connectTestRetryCount = 0;
        private void WatchDogtimer_Tick(object sender, object e)
        {
           
            controlpccomm.SendData("CabinStatusApp: ");

            if(connectTestRetryCount>2)
            {
                txtStatus.Text = "Failed to Connect. Please Verify ControlSoftware.";
                connectTestRetryCount = 0;

            }
            connectTestRetryCount++;

        }

       private void DeviceStatuMonitorFunc(int state)
        {
            //ImageSource img= new ImageSource(".\\Assets\\")
            switch(state)
            {
                case 1:
                    Imageslide.Source = Slide1;
                    state = 2;
                    break;
                case 2:
                    Imageslide.Source = Slide2;
                    state = 3;
                    break;
                case 3:
                    Imageslide.Source = Slide3;
                    state = 4;
                    break;
                case 4:
                    Imageslide.Source = Slide4;
                    state = 1;
                    break;
            }
            
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            
            Imageslide.Source = Slide1;
            Imageslide.Source = Slide2;
            Imageslide.Source = Slide3;
            Imageslide.Source = Slide4;
            Imageslide.Source = Slide1;
          
           

        }
        ContentDialog statusMessagedlg = null;
        
        private async void Controlpccomm_MessageReceived(object sender, CommunicationMsg e)
        {

            // statusMessagedlg = null;
            if(e.msg.Contains("CONNECTEDCONTROLSW"))
            {
                connectTestRetryCount = 0;
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    txtStatus.Text = "Connected With Control Software.";

                });
                    return;
            }

            if (e.msg.Contains("Error") || e.msg.Contains("Warning") || e.msg.Contains("Cleared"))
            {
                string[] msg = e.msg.Split(':');
               // string msgsec = msg[2];
                //  await a.ShowAsync();
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
                {
                    if (statusMessagedlg != null)
                        statusMessagedlg.Hide();
                    txtStatus.Text = e.msg;
                    statusMessagedlg = null;
                    if (statusMessagedlg == null && !e.msg.Contains("Cleared"))
                    {
                        statusMessagedlg = new ContentDialog()
                        {
                            Title = msg[1],//"Cabin Status",
                            Content = msg[2], //msgsec,
                        CloseButtonText = "Ok"
                        };

                        await statusMessagedlg.ShowAsync();

                    }


                });
                return;
            }

            int state = 0;
            try
            {

                state = int.Parse(e.msg);

            }
            catch(Exception)
            {

            }

            await  Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {


                btnIP.IsEnabled = false;
              //  txtStatus.Text = e.msg;
                DeviceStatuMonitorFunc(state);

                WatchDogtimer.Start();


            });
            
            
        }

        private async void btnIP_Click(object sender, RoutedEventArgs e)
        {
          //  var a = new MessageDialog(controlpccomm.LocalIPAddress());

        //    a.ShowAsync();
            controlpccomm.IPAddress = ipaddress.Text;  // "192.168.0.58";
            controlpccomm.Port = "4480";
            controlpccomm.CrearteCommunicationChannel();

            controlpccomm.MessageReceived += Controlpccomm_MessageReceived;

         Task<int> acc= controlpccomm.Connect();

         await     Task.Delay(1000);
            controlpccomm.SendData("CabinStatusApp: CabinStatus Application Connected.");

            //  controlpccomm.SendData("CabinStatus Application Connected.");

            //   var a = new MessageDialog("send");
            // await a.ShowAsync();


        }

    }
}
