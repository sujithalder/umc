﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Automation.BDaq;
namespace ControlSoftware
{
    /// <summary>
    /// main GUI Form, instantDiCtrl1 are used in this form
    /// </summary>
    public partial class ControlSMainForm : Form
    {
        DAQ daq = new DAQ();
        public ControlSMainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the form and controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlSMainForm_Load(object sender, EventArgs e)
        {
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);

            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.LightYellow;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.Black;

            for(int r=0; r< dataGridView1.Rows[0].Cells.Count; r++)
            {
                dataGridView1.Rows[0].Cells[r].Value = "F";
                dataGridView1.Rows[0].Cells[r].Style.BackColor = Color.LightGray;
            }
        }

        /// <summary>
        /// Select the device using device number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectDevice_Click(object sender, EventArgs e)
        {
            int deviceNumber = 0;
            if (instantDiCtrl1.Initialized || instantDoCtrl1.Initialized)
            {
                // MessageBox.Show("No device be selected or device open failed!", "instantDiCtrl");
                MessageBox.Show( "Device : " + instantDiCtrl1.SelectedDevice.Description + " already selected. Close the application and re-open to select other device.","Error Message");
                return;
            }

            try
            {
                deviceNumber = Convert.ToInt32(txtDeviceNumber.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            try
            {
                daq.Initialize(deviceNumber, this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }

            if (!instantDiCtrl1.Initialized || !instantDoCtrl1.Initialized)
            {
                MessageBox.Show("No device be selected or device open failed!", "instantDiCtrl");
                return;
            }

            lbldeviceInfo.Text =   instantDiCtrl1.SelectedDevice.Description +
                "  Boadrd ID:  " + instantDiCtrl1.BoardId.ToString() + " Board Version " + instantDiCtrl1.BoardVersion;

            lbldeviceInfo.Text = lbldeviceInfo.Text + " No of Port: " + instantDiCtrl1.Features.PortCount.ToString() + " No. Of Channels " + instantDiCtrl1.Features.ChannelCountMax.ToString();

            timer1.Interval = 100;
            timer1.Start();
        }

        /// <summary>
        /// Read Device status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            try
            {
                daq.ReadDI();
                DisplayRawData();

            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            timer1.Start();
        }

        /// <summary>
        /// Display device status
        /// </summary>
        void DisplayRawData()
        {
            //Device status
            foreach(var v in daq.DIStatus)
            {
                Instruments ins = v.Key;
                switch (ins)
                {
                    case Instruments.AIR_SCRUB_FAN:
                        UpdateStatus(Instruments.AIR_SCRUB_FAN, 0);
                        break;
                    case Instruments.H202_FUG_FAN:
                        UpdateStatus(Instruments.H202_FUG_FAN, 1);
                        break;
                    case Instruments.H202_EXHAUST_FAN:
                        UpdateStatus(Instruments.H202_EXHAUST_FAN, 2);
                        break;
                    case Instruments.MOTION_DETECTOR:

                        UpdateStatus(Instruments.MOTION_DETECTOR, 3);
                        break;

                    case Instruments.H202_FLUID_EMPTY:
                        UpdateStatus(Instruments.H202_FLUID_EMPTY, 4);
                        break;
                    case Instruments.CLEAN_FLUID_EMPTY:
                        UpdateStatus(Instruments.CLEAN_FLUID_EMPTY, 5);
                        break;
                    case Instruments.WASTE_FLUID_EMPTY:
                        UpdateStatus(Instruments.WASTE_FLUID_EMPTY, 6);
                        break;
                    case Instruments.CABINDOOR_CONTROLLER:
                        UpdateStatus(Instruments.CABINDOOR_CONTROLLER, 7);
                        break;
                    case Instruments.CABIN_DOOR_OPEN:
                        UpdateStatus(Instruments.CABIN_DOOR_OPEN,8 );
                        break;
                    case Instruments.INST_CLEANING_SYSTEM_READY:
                        if (daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_READY] == true)
                            lblstatus1.BackColor = Color.Blue;
                        else
                            lblstatus1.BackColor = Color.Red;
                        lblstatus1.Text = daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_READY].ToString();
                        break;
                    case Instruments.INST_CLEANING_SYSTEM_OPERATING:
                        if (daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING] == true)
                            lblstatus2.BackColor = Color.Blue;
                        else
                            lblstatus2.BackColor = Color.Red;
                        lblstatus2.Text = daq.DIStatus[Instruments.INST_CLEANING_SYSTEM_OPERATING].ToString();
                        break;
                    case Instruments.CABIN_EMPTY:
                        if (daq.DIStatus[Instruments.CABIN_EMPTY] == true)
                            lblstatus3.BackColor = Color.Blue;
                        else
                            lblstatus3.BackColor = Color.Red;
                        lblstatus3.Text = daq.DIStatus[Instruments.CABIN_EMPTY].ToString();

                        break;

                    case Instruments.CABIN_READY_FORUSE:
                        if (daq.DIStatus[Instruments.CABIN_READY_FORUSE] == true)
                            lblstatus4.BackColor = Color.Blue;
                        else
                            lblstatus4.BackColor = Color.Red;
                        lblstatus4.Text = daq.DIStatus[Instruments.CABIN_READY_FORUSE].ToString();
                        break;
                    case Instruments.USB_4750_POWERED_ON:
                        if (daq.DIStatus[Instruments.USB_4750_POWERED_ON] == true)
                        {
                            usbstatus.Text = "Powered On";
                            usbstatus.BackColor = Color.Blue;
                        }
                        else if (daq.DIStatus[Instruments.USB_4750_POWERED_ON] == false)
                        {
                            usbstatus.Text = "Powered Off";
                            usbstatus.BackColor = Color.Red;
                        }
                        break;
                }
            }

            //Port 0 data
            for (int j = 0; j < daq.RawDataPort0.Count; j++)
            {
                int val0 = (daq.RawDataPort0[j] >> j) & 0x1;
                lbl0.Text = daq.RawDataPort0[j].ToString("X");
                switch (j)
                {
                    case 0:

                        AddPictureBox(pictureBox6, val0);
                        break;
                    case 1:

                        AddPictureBox(pictureBox8, val0);
                        break;

                    case 2:

                        AddPictureBox(pictureBox7, val0);
                        break;
                    case 3:

                        AddPictureBox(pictureBox3, val0);
                        break;
                    case 4:

                        AddPictureBox(pictureBox4, val0);
                        break;
                    case 5:

                        AddPictureBox(pictureBox5, val0);
                        break;
                    case 6:

                        AddPictureBox(pictureBox2, val0);
                        break;
                    case 7:

                        AddPictureBox(pictureBox1, val0);
                        break;
                }
            }

            //pORT1 Channels
            for (int j = 0; j < daq.RawDataPort1.Count; j++)
            {
                int val0 = (daq.RawDataPort1[j] >> j) & 0x1;
                lbl1.Text = daq.RawDataPort1[j].ToString("X");
                switch (j)
                {
                    case 0:
                        AddPictureBox(pictureBox16, val0);
                        break;
                    case 1:
                        AddPictureBox(pictureBox15, val0);
                        break;
                    case 2:
                        AddPictureBox(pictureBox14, val0);
                        break;
                    case 3:
                        AddPictureBox(pictureBox13, val0);
                        break;
                    case 4:
                        AddPictureBox(pictureBox12, val0);
                        break;
                    case 5:
                        AddPictureBox(pictureBox11, val0);
                        break;
                    case 6:
                        AddPictureBox(pictureBox10, val0);
                        break;
                    case 7:
                        AddPictureBox(pictureBox9, val0);
                        break;
                }
            }
        }

        void AddPictureBox(PictureBox pic,int val0 )
        {
            if (val0 == 1)
                pic.Image = global::ControlSoftware.Properties.Resources.on; //Image.FromFile("on.png");
            else
                pic.Image = global::ControlSoftware.Properties.Resources.off; //Image.FromFile("off.png");

        }

        void UpdateStatus(Instruments inst, int col )
        {
            if (daq.DIStatus[inst] == true)
                dataGridView1.Rows[0].Cells[col].Style.BackColor = Color.Blue;
            else
                dataGridView1.Rows[0].Cells[col].Style.BackColor = Color.Red;

            dataGridView1.Rows[0].Cells[col].Value = daq.DIStatus[ inst];
        }

        bool DisplayErrorMessage()
        {
            if (!instantDiCtrl1.Initialized || !instantDoCtrl1.Initialized)
            {
                MessageBox.Show("No device be selected or device open failed!", "Control Software");
                return true;
            }
            return false;
        }
        bool btn1 = false;
        private void button1_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn1 = !btn1;
            changeImage(button1, btn1);
            UpdatedChannelBitValue(0, 7, btn1);
            daq.AirScrubFan(btn1);
        }

        bool btn2 = false;
        private void button2_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            btn2 = !btn2;
            changeImage(button2, btn2);

            UpdatedChannelBitValue(0, 6, btn2);

            daq.H202FogFan(btn2);
        }

        bool btn3 = false;
        private void button3_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            btn3 = !btn3;
            changeImage(button3, btn3);

            UpdatedChannelBitValue(0, 5, btn3);
            daq.WriteDO(0);

        }

     public   void UpdatedChannelBitValue(int portid, int channelid, bool onOff)
        {
            if (onOff)
                daq.SelectedDO(portid, channelid, 1);
            else
                daq.SelectedDO(portid, channelid, 0);

            if(portid==0)
                lblDO0.Text = daq.PortDataDO(portid).ToString("X");
            else
                lblDO1.Text = daq.PortDataDO(portid).ToString("X");

        }

        void changeImage(Button btn, bool highlow)
        {
            Image img = global::ControlSoftware.Properties.Resources.low; // Image.FromFile("low.png");

            if (highlow)
                img = global::ControlSoftware.Properties.Resources.high; //Image.FromFile("high.png");
            btn.Image = img;
        }

        bool btn4 = false;
        private void button4_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            btn4 = !btn4;
            changeImage(button4, btn4);
            UpdatedChannelBitValue(0, 4, btn4);
            daq.WriteDO(0);
        }

        bool btn6 = false;
        private void button6_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            btn6 = !btn6;
            changeImage(button6, btn6);
            UpdatedChannelBitValue(0, 3, btn6);
            daq.WriteDO(0);
        }

        bool btn5 = false;
        private void button5_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            btn5 = !btn5;
            changeImage(button5, btn5);
            UpdatedChannelBitValue(0, 2, btn5);
            daq.WriteDO(0);
        }

        bool btn7 = false;
        private void button7_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;
            btn7 = !btn7;
            changeImage(button7, btn7);
            UpdatedChannelBitValue(0, 1, btn7);
            daq.WriteDO(0);
        }

        bool btn8 = false;
        private void button8_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn8 = !btn8;
            changeImage(button8, btn8);
            UpdatedChannelBitValue(0, 0, btn8);
            daq.WriteDO(0);
        }

        bool btn16 = false;
        bool btn15 = false;
        bool btn14 = false;
        bool btn13 = false;
        bool btn12 = false;
        bool btn10 = false;
        bool btn11 = false;
     
        bool btn9 = false;
        private void button16_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn16 = !btn16;
            changeImage(button16, btn16);
            UpdatedChannelBitValue(1, 0, btn16);
            daq.WriteDO(1);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn15 = !btn15;
            changeImage(button15, btn15);
            UpdatedChannelBitValue(1, 1, btn15);
           
            daq.WriteDO(1);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn14 = !btn14;
            changeImage(button14, btn14);
            UpdatedChannelBitValue(1, 2, btn14);
            daq.WriteDO(1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn13 = !btn13;
            changeImage(button13, btn13);
            UpdatedChannelBitValue(1, 3, btn13);
            daq.WriteDO(1);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn12 = !btn12;
            changeImage(button12, btn12);
            UpdatedChannelBitValue(1, 4, btn12);
            daq.WriteDO(1);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn11 = !btn11;
            changeImage(button11, btn11);
            UpdatedChannelBitValue(1, 5, btn11);
            daq.WriteDO(1);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn10 = !btn10;
            changeImage(button10, btn10);
            UpdatedChannelBitValue(1, 6, btn10);
            daq.DetectCabinDoorController(btn10);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (DisplayErrorMessage())
                return;

            btn9 = !btn9;
            changeImage(button9, btn9);
            UpdatedChannelBitValue(1, 7, btn9);
           
            daq.H202ExhaustFan(btn9);
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            txtRx.Text += "Tx: "+txtTX.Text + "\r\n";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDeviceNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar);
        }
    }
}
