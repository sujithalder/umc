﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TestRigFrame;
using Windows.Devices.Input;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using TestRigFrame;
namespace UMC
{
    /// <summary>
    /// logic for Stethoscopelungs.xaml
    /// </summary>
    public partial class Stethoscopelungs : UserControl
    {
       
        int SelectedStethoscope = 1;
        Dictionary<int, bool> _currentState = new Dictionary<int, bool>();
        UMCPresenter _presener;
        MainPage _rootwindow;
        bool manualOperationOrAutoMode = true;
        bool recvedstate = false;
        public Stethoscopelungs()
        {
            InitializeComponent();
            SolidColorBrush blackBrush = new SolidColorBrush
            {
                Color = Colors.LightBlue
            };
            
            btn1.Background = blackBrush;
        }


        private void StethoscopeArrayEvent(object sender, EventArgs e)
        {
            string msg = ((ChartDataeventMsg)e).Msg;
            switch (msg)
            {
                case "reset":
                    ResetStethoscope();
                    break;
            }
        }

        /// <summary>
        /// UMCPresenter Object
        /// </summary>
        public UMCPresenter PresenterObject
        {
            set {
                _presener = value;
                rootGrid.DataContext = this;
                if (_presener.IsCabinSW)
                {
                    btnAuto.Visibility = Visibility.Collapsed;
                    btnDone.IsEnabled = false;
                }
            }
        }

        /// <summary>
        /// Main window references
        /// </summary>
        public MainPage RootWindowHandle
        {
            set { _rootwindow = value; }
        }

      /// <summary>
      /// Current state of each stethoscope
      /// </summary>
      /// <param name="indx"></param>
        void CurrentState(int indx)
        {
            bool state = false;
            if (_currentState.ContainsKey(indx))
            {
                state = _currentState[indx];
                state = !state;
                if (!manualOperationOrAutoMode)
                    state = recvedstate;
                _currentState[indx] = state;
            }else
            {
                 state = !state;
                if (!manualOperationOrAutoMode)
                    state = recvedstate;
                _currentState[indx] = state;
            }
            manualOperationOrAutoMode = true;
        }


        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
           if(IsIntractiveMode)
            StartTXRX(1);
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(2);
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(3);

        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(4);

        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(5);

        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(6);
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(7);

        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(8);

        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(9);
        }

        private void Btn10_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(10);

        }

        private void Btn11_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(11);

        }

        private void Btn12_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(12);

        }

        private void Btn13_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(13);

        }

        private void Btn14_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(14);

        }

        private void Btn15_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(15);

        }

        private void Btn16_Click(object sender, RoutedEventArgs e)
        {
            if (IsIntractiveMode)
                StartTXRX(16);
        }
        
        /// <summary>
        /// StartTXRX command for stethoscope
        /// </summary>
        /// <param name="index"></param>
         void  StartTXRX(int index)
        {
            ChangeMousePtr(true);
            CurrentState(index);
            bool state = false;
            if(_currentState.ContainsKey(SelectedStethoscope))
                state = _currentState[SelectedStethoscope];
                if (state && SelectedStethoscope != index && !_presener.IsCabinSW)
            {
                //Stop the current Stethoscope on running state
                 CurrentState(SelectedStethoscope);
                  StartTXRXex(SelectedStethoscope);
                 Task.Delay(300).Wait();
            }
                //start the selected stethoscope
              StartTXRXex(index);
            ChangeMousePtr();
        }

        /// <summary>
        /// StartTXRX -send the command to CABINSW based on the current state
        /// </summary>
        /// <param name="index"></param>
        void StartTXRXex(int index)
        {
            UpdateColor(0);
           
            SelectedStethoscope = index;
            bool state = _currentState[index];
            if (!_presener.IsCabinSW)
            {
                if (state)
                {
                    UpdateColor(5);
                        string msg = string.Format(CoreCommunicationTXRX.coreTXex, "11", "1", index.ToString());
                        _presener.SendMessage(msg,true);
                        //After sending wait for few millisecond, temporary this time can be remove
                          Task.Delay( 300 ).Wait();
                        _presener.StartRX(state);
                }
                else
                {
                        UpdateColor(5);
                    //stop stethoscope and wait for few millisecond.
                        _presener.StartRX(state);
                        Task.Delay(300).Wait();
                        string msg = string.Format(CoreCommunicationTXRX.coreTXex, "11", "0", index.ToString());
                        _presener.SendMessage(msg);
                        UpdateColor(2);
                }
            }
          //  else
          if(_presener.IsCabinSW)
            {
                if(!state)
                    UpdateColor(5);
                string syscmd = "";
                if (state)
                    syscmd = index.ToString();
                else
                    syscmd = "0";
             //   string sysccmd = string.Format(TestrigframeUI.Serialdevicestatusformat, (int)TESTRIGOPERATIONS.SELECT_CHAIRPOSITION, cmdstr);
                string cmd = string.Format(TestrigframeUI.Serialdevicestatusformat, (int)TESTRIGOPERATIONS.SELECTMICROFONE, syscmd);
                //  _presener.SerialDevSerialDevMgrMgr.SystemCommand(CoreCommunicationTXRX.Stethoscope_Chair, syscmd);
                _presener.SendControlCMD(cmd);
                _presener.StartTX(index, state,true);
            }
        }

        /// <summary>
        /// Close the stethoscopelungs
        /// </summary>
        public  void Close()
        {
            if(btnApply.Content.Equals("Stop"))
                   Apply();
        }

        /// <summary>
        /// Used to update color of the Stethoscope,descibes different state. example-Streaming, stopped, failed.
        /// </summary>
        /// <param name="reset"></param>
        public void UpdateColor(int reset )
        {
            SolidColorBrush br = new SolidColorBrush(Windows.UI.Colors.White);
            
             
             streamingInProgress = false;

            switch (reset)
            {
                case 0:
                    br = new SolidColorBrush(Windows.UI.Colors.White);
                    break;
                case 1:
                    br = new SolidColorBrush(Windows.UI.Colors.LightSkyBlue);
                    btnApply.Content = "Stop";
                    btnApply.Background = br;
                    break;
                case 2:
                    br.Color = Colors.LightSkyBlue;
                    break;
                case 3:
                    if (btnApply.Content.Equals("Stop"))
                        btnApply.Content = "Start";
                    btnApply.Background = br;
                    br  = new SolidColorBrush( Colors.LightSkyBlue);
                    break;
                case 4:
                    br = new SolidColorBrush(Colors.Red);
                    break;
                case 5:
                    br = new SolidColorBrush(Colors.Green);
                    break;
                case 6:
                    br = new SolidColorBrush(Colors.DarkBlue);
                    streamingInProgress = true;
                    break;
            }

            switch (SelectedStethoscope)
            {
                case 1:
                    btn1.Background = br;
                    break;
                case 2:
                    btn2.Background = br;
                    break;
                case 3:
                    btn3.Background = br;
                    break;
                case 4:
                    btn4.Background = br;
                    break;
                case 5:
                    btn5.Background = br;
                    break;
                case 6:
                    btn6.Background = br;
                    break;
                case 7:
                    btn7.Background = br;
                    break;
                case 8:
                    btn8.Background = br;
                    break;
                case 9:
                    btn9.Background = br;
                    break;
                case 10:
                    btn10.Background = br;
                    break;
                case 11:
                    btn11.Background = br;
                    break;
                case 12:
                    btn12.Background = br;
                    break;
                case 13:
                    btn13.Background = br;
                    break;
                case 14:
                    btn14.Background = br;
                    break;
                case 15:
                    btn15.Background = br;
                    break;
                case 16:
                    btn16.Background = br;
                    break;
            }
        }

        
        private void BtnUP_Click(object sender, RoutedEventArgs e)
        {
            if (streamingInProgress || !IsIntractiveMode)
                return;
            UpdateColor(0);
            SelectedStethoscope--;
            if (SelectedStethoscope <= 0)
                SelectedStethoscope = 1;
            UpdateColor(2);
        }

       
        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            if (streamingInProgress || !IsIntractiveMode)
                return;

            UpdateColor(0);
            SelectedStethoscope++;
            if (SelectedStethoscope >= 16)
                SelectedStethoscope = 16;
            UpdateColor(2);
        }

        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
            if (btnApply.Content.Equals("Stop"))
                btnApply.Content = "Start";
        }

        void Apply()
        {
            SelectedStethoscopeButton(SelectedStethoscope);
        }

        private void BtnApply_MouseEnter(object sender, MouseEventArgs e)
        {
            SolidColorBrush br = new SolidColorBrush(Windows.UI.Colors.LightSkyBlue);
            btnApply.Background = br;
        }

        private void BtnApply_MouseLeave(object sender, MouseEventArgs e)
        {
            btnApply.Background = null;
        } 

        private void BtnApply_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {

            SolidColorBrush br = new SolidColorBrush(Windows.UI.Colors.LightSkyBlue);
              btnApply.Background = br;
        } 

        private void BtnApply_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            if(btnApply.Content.Equals("Start"))
            btnApply.Background = null;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
           // Close();
        }

        private void BtnDone_Click(object sender, RoutedEventArgs e)
        {
            Done();
        }

        void Done(bool send=true)
        {
             stopAutoStethoscope = true;
            if (autoStethoscopeTimerobj != null)
            {
                autoStethoscopeTimerobj.Stop();
                autoStethoscopeTimerobj = null;
            }
              btnAuto.IsChecked = false;

            if (_currentState.ContainsKey(SelectedStethoscope) && _currentState[SelectedStethoscope])
            {
                UpdateColor(2);
                CurrentState(SelectedStethoscope);
                if (!_presener.IsCabinSW)
                {
                    _presener.StartRX(false);
                }
                else
                    _presener.StartTX(SelectedStethoscope, false,true);
            }

            if (!_presener.IsCabinSW && send)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTXex, "11", "0", "17");
                _presener.SendMessage(msg);
            }
            
            _rootwindow.CloseStheoscope();

            if (autoStethoscopeTimerobj != null)
            {
                autoStethoscopeTimerobj.Stop();
                autoStethoscopeTimerobj = null;
            }
            btnAuto.IsChecked = false;
          
        }

        private void BtnDone_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SolidColorBrush br = new SolidColorBrush(Windows.UI.Colors.LightSkyBlue);
            btnDone.Background = br;
        }

        private void BtnDone_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            btnDone.Background = null;
        }

        void ChangeMousePtr(bool normalorwait =false)
        {
            if(normalorwait)
            Windows.UI.Xaml.Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Wait, 1);
            else
            Windows.UI.Xaml.Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Arrow, 1);

        }
        static double   _height= 0;
        public static double StHeight {
            set { _height = value;}
            get
            {
                return _height;
            }
        } 

        public void StethoscopeCommand(string msg)
        {
              manualOperationOrAutoMode = false; 
            switch (msg)
            {
                case "s1111e": 
                    recvedstate = true;
                    StartTXRX(1);
                    break;
                case "s1101e":
                    recvedstate = false;
                    StartTXRX(1);
                    break;
                case "s1112e":
                    recvedstate = true;
                    StartTXRX(2);
                    break;
                case "s1102e":
                    recvedstate = false;
                    StartTXRX(2);
                    break;
                case "s1113e":
                    recvedstate = true;
                    StartTXRX(3);
                    break;
                case "s1103e":
                    recvedstate = false;
                    StartTXRX(3);
                    break;
                case "s1114e":
                    recvedstate = true;
                    StartTXRX(4);
                    break;
                case "s1104e":
                    recvedstate = false;
                    StartTXRX(4);
                    break;
                case "s1115e":
                    recvedstate = true;
                    //startorstopflag = false;
                    StartTXRX(5);
                    break;
                case "s1105e":
                    recvedstate = false;
                    StartTXRX(5);
                    break;
                case "s1116e": 
                    recvedstate = true;
                    StartTXRX(6);
                    break;
                case "s1106e":
                    recvedstate = false;
                    StartTXRX(6);
                    break;
                case "s1117e": 
                    recvedstate = true;
                    StartTXRX(7);
                    break;
                case "s1107e":
                    recvedstate = false;
                    StartTXRX(7);
                    break;
                case "s1118e": 
                    recvedstate = true;
                    StartTXRX(8);
                    break;
                case "s1108e":
                    recvedstate = false;
                    StartTXRX(8);
                    break;
                case "s1119e": 
                    recvedstate = true;
                    StartTXRX(9);
                    break;
                case "s1109e": 
                    recvedstate = false;
                    StartTXRX(9);
                    break;
                case "s11110e":
                    recvedstate = true;
                    StartTXRX(10);
                    break;
                case "s11010e":
                    recvedstate = false;
                    StartTXRX(10);
                    break;
                case "s11111e":
                    recvedstate = true;
                    StartTXRX(11);
                    break;
                case "s11011e":
                    recvedstate = false;
                    StartTXRX(11);
                    break;
                case "s11112e": 
                    recvedstate = true;
                    StartTXRX(12);
                    break;
                case "s11012e": 
                    recvedstate = false;
                    StartTXRX(12);
                    break;
                case "s11113e": 
                    recvedstate = true;
                    StartTXRX(13);
                    break;
                case "s11013e":
                    recvedstate = false;
                    StartTXRX(13);
                    break;
                case "s11114e":
                    recvedstate = true;
                    StartTXRX(14);
                    break;
                case "s11014e":
                    recvedstate = false;
                    StartTXRX(14);
                    break;
                case "s11115e": 
                    recvedstate = true;
                    StartTXRX(15);
                    break;
                case "s11015e":
                    recvedstate = false;
                    StartTXRX(15);
                    break;
                case "s11116e": 
                    recvedstate = true;
                    StartTXRX(16);
                    break;
                case "s11016e":
                    recvedstate = false;
                    StartTXRX(16);
                    break;
                case "s11017e":
                    _presener.ModelObject.LogMessage("s11017e:" + msg);
                    stopAutoStethoscope = true;
                    Done();  
                    break; 
            }
        }

        int autoStethoscopeIndex = 0;
        bool stopAutoStethoscope = false;
        void AutoStheoscopeTimerhandler(object sender=null, object e=null)
        {
            if(stopAutoStethoscope)
            {
                if (autoStethoscopeTimerobj != null)
                    autoStethoscopeTimerobj.Stop();
                autoStethoscopeTimerobj = null;
                return;
            }

            autoStethoscopeIndex++; 
                SelectedStethoscopeButton(autoStethoscopeIndex); 

            if (autoStethoscopeIndex <= 17)
            {
                if (stopAutoStethoscope)
                {
                    if (autoStethoscopeTimerobj != null)
                    {
                        autoStethoscopeTimerobj.Stop();
                        autoStethoscopeTimerobj = null;
                    }
                     btnAuto.IsChecked = false;
                }
                 
            }
            else
            {
                if (autoStethoscopeTimerobj != null)
                {
                    autoStethoscopeTimerobj.Stop();
                    autoStethoscopeTimerobj = null;
                }
                   autoStethoscopeIndex = 0;
            }
        }

        public void StopAutoStethoscope()
        {
          
            stopAutoStethoscope = true;
            if (autoStethoscopeTimerobj != null)
            {
                autoStethoscopeTimerobj.Stop();
                autoStethoscopeTimerobj = null;
            }
            btnAuto.IsChecked = false;
        }
      public  void ResetStethoscope(bool stopstethoscope= true )
        {
            btnAuto.IsChecked = false;
            stopAutoStethoscope = true;
            if (autoStethoscopeTimerobj != null)
            {
                autoStethoscopeTimerobj.Stop();
                autoStethoscopeTimerobj = null;
            }
            
            if(_presener !=null && _presener.IsCabinSW && stopstethoscope)
            {
                _presener.StartTX(SelectedStethoscope,false,true);
               
            }
            else if (_presener != null && !_presener.IsCabinSW)
            {
                _presener.StartRX(false);
                Done(false);
            }
                 UpdateColor(0);
                autoStethoscopeIndex = 0;
           //  if(stopstethoscope)
           //    SelectedStethoscope = 1;

            for(int i=1; i<17;i++)
            {
                SelectedStethoscope = i;
                UpdateColor(0);
            }
            SelectedStethoscope = 0;
        }

        private void BtnAuto_Click(object sender, RoutedEventArgs e)
        {
           
            if (btnAuto.IsChecked ==true)
            {
                stopAutoStethoscope = false;
                if (!_presener.IsCabinSW)
                    AutoStheoscopeTimerhandler();

                autoStethoscopeTimerobj = new DispatcherTimer();
                autoStethoscopeTimerobj.Tick += AutoStheoscopeTimerhandler;
                int timeinsec = _presener.AutoTimeInterval;
                int hr = timeinsec / 3600;
                timeinsec = timeinsec % 3600;
                int min = timeinsec/60;
                timeinsec = timeinsec % 60;
                int sec = timeinsec;
                autoStethoscopeTimerobj.Interval = new TimeSpan(hr, min, sec);
                autoStethoscopeTimerobj.Start();
            }
            else
            {
                if (autoStethoscopeTimerobj != null)
                {
                    autoStethoscopeTimerobj.Stop();
                    autoStethoscopeTimerobj = null;
                }
                    btnAuto.IsChecked = false;
                    stopAutoStethoscope = true;
            }
        }

        /// <summary>
        /// SelectedStethoscope
        /// </summary>
        /// <param name="index"></param>
        void SelectedStethoscopeButton(int index)
        {
            switch (index)
            {
                case 1:
                    StartTXRX(1);
                    break;
                case 2:
                    StartTXRX(2);
                    break;
                case 3:
                    StartTXRX(3);
                    break;
                case 4:
                    StartTXRX(4);
                    break;
                case 5:
                    StartTXRX(5);
                    break;
                case 6:
                    StartTXRX(6);
                    break;
                case 7:
                    StartTXRX(7);
                    break;
                case 8:
                    StartTXRX(8);
                    break;
                case 9:
                    StartTXRX(9);
                    break;
                case 10:
                    StartTXRX(10);
                    break;
                case 11:
                    StartTXRX(11);
                    break;
                case 12:
                    StartTXRX(12);
                    break;
                case 13:
                    StartTXRX(13);
                    break;
                case 14:
                    StartTXRX(14);
                    break;
                case 15:
                    StartTXRX(15);
                    break;
                case 16:
                case 17:
                    StartTXRX(16);
                    break;
            }
        }

       bool IsIntractiveMode
        {
            get
            {
                if (_presener != null && _presener.IsCabinSW)
                    return false;

                return true;
            }




        }
      //   Timer autoStethoscopeTimer = null;
        DispatcherTimer autoStethoscopeTimerobj = null;
        bool streamingInProgress = false;
    }
}
