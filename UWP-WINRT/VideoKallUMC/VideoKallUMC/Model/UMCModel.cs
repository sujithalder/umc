﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using CommunicationModule;
namespace UMC
{
  public   class UMCModel: IDisposable
    {
        UMCPresenter _presenter = null;
        CommunicationLib coreCommunication = null; 
        Queue<string> incomingdata = new Queue<string>();
        bool stopdatamonitor = false;
        CommunicationLibControlSW controlswComm = new CommunicationLibControlSW();
        public UMCModel()
        {
           // ConnectToControlSW();


        }

    public   void SendControlCMD(string cmd)
        {
            controlswComm.SendData(cmd);
        }
    public    async void  ConnectToControlSW()
        {
            controlswComm.IPAddress =  _presenter.ControlSystemIPAddress;
            controlswComm.Port = "4480";
            controlswComm.CrearteCommunicationChannel();

            controlswComm.MessageReceived += Controlpccomm_MessageReceived;

            Task<int> acc = controlswComm.Connect();

            await Task.Delay(1000);
            controlswComm.SendData("CabinSWApp: CabinSWApp  Connected.");

        }

        private   void Controlpccomm_MessageReceived(object sender, CommunicationMsg e)
        {

            // statusMessagedlg = null;
            if (e.msg.Contains("CONNECTEDCONTROLSW"))
            {
                 
                return;
            }
           // incomingdata.Enqueue(e.msg);
             _presenter.TestRigMessageReceived(e.msg);
            if (_presenter.IsCabinSW)
                _presenter.SendMessage(e.msg,false,false,true);

        }
            public void InitializeCommunicationModule()
        {
            try
            {
                if (coreCommunication != null)
                {
                    stopdatamonitor = true;
                    coreCommunication.CleanUp();
                    coreCommunication = null;
                    GC.Collect();
                    Task.Delay(300).Wait();
                    GC.Collect();
                }
             
                _presenter.NotifyError(this, new TXRXeventsArgs("Model init:" + "Released."));
               
                // GC.Collect();
                coreCommunication = new CommunicationLib();

                    coreCommunication.MessageReceived += new EventHandler<CommunicationMsg>(MessageReceived);
                    stopdatamonitor = false;
                    ProcessReceivedMessageAsync();
               

                coreCommunication.IsCabinSW = _presenter.IsCabinSW;
                if (_presenter.IsCabinSW)
                {
                    coreCommunication.IPAddress = _presenter.IPAddress;//"10.10.0.148";// _presenter.IPAddress  ;
                }
                else
                {
                    string ip = coreCommunication.LocalIPAddress();
                    if(string.IsNullOrEmpty(_presenter.MyIPAddress) && ip==null)
                    {
                        LogMessage("Error: " + "local IP Address is empty");
                        return;
                    }
                    else if(!string.IsNullOrEmpty(_presenter.MyIPAddress))
                    {
                        LogMessage("localip: " + _presenter.MyIPAddress);
                        coreCommunication.IPAddress = _presenter.MyIPAddress;

                    }
                    else
                    {
                        LogMessage("localip: " + ip);
                        coreCommunication.IPAddress = _presenter.MyIPAddress;
                    }

                }
                coreCommunication.Port = "2244";
                coreCommunication.CrearteCommunicationChannel();
                 coreCommunication.Connect();
            }catch(Exception ex)
            {
                LogMessage("Erroe: "+ex.Message);
                _presenter.NotifyError(this, new TXRXeventsArgs("Model init:"+ex.Message));

            }
            return  ;
        }

        public async void SendMessage(string msg, bool delayaftersendingMessage = false, bool delaybeforesendingMsg= false, bool connectionMsg=false)
        {
            try
            {
               //if (_presenter.IsCabinSW || !_presenter.IsCabinSW && _presenter.IsConnected)
               if(_presenter.IsConnected || connectionMsg)
                {

                 // if(delaybeforesendingMsg)
                  //     await Task.Delay(100);
                    await coreCommunication.SendData(msg);

                    if(!msg.Contains("TESTRIGSTATUS"))
                      LogMessage("Send: "+msg);

                  if (delayaftersendingMessage)
                        Task.Delay(300).Wait();
                }
            }
            catch(Exception ex)
            {
                _presenter.NotifyError(this, new TXRXeventsArgs("Model send:"+ex.Message));

            }

        }
        public void MessageReceived(Object sender, CommunicationMsg msg)
        {   
            if(!msg.msg.Contains("TESTRIGSTATUS"))
            LogMessage("Recv: "+msg.msg);

            incomingdata.Enqueue(msg.msg);
        }

        private async void  ProcessReceivedMessageAsync()
        {
            while(true)
            {
               if(incomingdata.Count()>0)
                 ProcessTXRX(incomingdata.Dequeue());
                await  Task.Delay(TimeSpan.FromMilliseconds(1));
                if (stopdatamonitor)
                    break;
            }
        }
    
        public void ProcessTXRX(string msg)
        {
             
            try
            {
                if (msg.Contains("TESTRIGSTATUS")|| msg.Contains("TESTRIGCMD"))
                {
                    _presenter.TestRigMessageReceived(msg);
                    return;
                }

                msg = msg.Replace("\\r\\n", "\r");
                msg = msg.Replace("\r\n", "\r");
                string[] rxtx = msg.Split('\r');
                foreach (var v in rxtx)
                {
                    string str = v;
                    str = str.Trim();
                    if (string.IsNullOrEmpty(str))
                        continue;
                   
                    _presenter.MessageReceived(v);
                   
                }//

            }
            catch(Exception ex)
            {
                _presenter.NotifyError(this, new TXRXeventsArgs(ex.Message));

            }
        }

     
        public UMCPresenter Presenter
        {
            set { _presenter = value; }
           
        }

        /// <summary>
        /// Smart Card reader
        /// </summary>
        public async void ReadSmartCard()
        {
                try
                {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("SIMULATED_smartcard.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);

                foreach (var line in alltext)
                    {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "VISIT_CHARGE_PER_MIN":
                            _presenter.VisitCharge = data[1];
                                break;
                            case "PREPAID_VISIT_VALUE_ON_CARD":
                            _presenter.ValueOnCard = data[1];
                                break;
                            case "PREPAID_CLEANING_ON_CARD":
                            _presenter.CleaningOnCard = data[1];
                                break;
                            case "TOTAL_PREPAID_ON_CARD":
                            _presenter.TotalCleaningOnCard = data[1];
                                break;
                            case "VISIT_COST":
                            _presenter.VisitCost = data[1];
                                break;
                        case "CABINSTATUS":
                            _presenter.CabinStatus = data[1];
                            break;
                        case "CABINID":
                            _presenter.CabinID = data[1];
                            break;
                        case "CABIN_ZIPCODE":
                            _presenter.SmtzipCode = data[1];
                            break;
                        case "SMARTCARDID":
                            if(_presenter.IsConnected)
                            _presenter.SmartCardID = data[1];
                            break;

                            default:
                                break;
                        }

                    }
                }
                catch (Exception)
                {
  
                }
        }

        /// <summary>
        /// Cabin monitor data
        /// </summary>
        public async void CabinMonitor()
        {
                try
                {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("SIMULATED_cabinmonitor.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);

                //temporary implementation
                foreach (var line in alltext)
                {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "CABIN_INT_HUMIDITY":
                            _presenter.sensordata.CINT_HUMIDITY = data[1] + "%";
                                break;
                            case "CABIN_INT_TEMP":
                            _presenter.sensordata.CABIN_INT_TEMP = data[1];
                                break;
                            case "CABIN_PATHOGEN_COUNT":
                            _presenter.sensordata.CABIN_PATHOGEN_COUNT = data[1];
                                break;
                            case "CABIN_EXT_TEMP":
                            _presenter.sensordata.CABIN_EXT_TEMP = data[1];
                                break;
                            case "CABIN_EXT_HUMIDITY":
                            _presenter.sensordata.CABIN_EXT_HUMIDITY = data[1] + "%";
                                break;
                            default:
                                break;
                        }

                    }
                }
                catch (Exception)
                {
                    
                }
            }

        /// <summary>
        /// NPI Attendant data
        /// </summary>
        public async void NPAttendant()
        {
                try
                {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("SIMULATED_ATTENDANT_NP.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);

                //temporary implementation
                foreach (var line in alltext)
                {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "NAME_NPI":
                            _presenter.NAME_NPI = data[1];
                                break;
                            case "DEA":
                            _presenter.DEA = data[1];
                                break;
                            case "PROVIDER_NPI":
                            _presenter.PROVIDER_NPI = data[1];
                                break;
                            case "FACILITY_NAME":
                            _presenter.FACILITY_NAME = data[1];
                                break;
                            case "FACILITY_ZIP":
                            _presenter.FACILITY_ZIP = data[1];
                                break;
                            default:
                                break;
                        }
                    }
                }
                catch (Exception)
                {
                    
                }
                

        }

        /// <summary_resiratory
        /// NPI Attendant data
        /// </summary>
       public async void ReadPatientRecord()
        {
           
                try
                {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("SIMULATED_PatientInformation.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);
                
                //temporary implementation
                foreach (var line in alltext)
                {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "FullName":
                           
                            _presenter.PatientInfo.FullName = data[1];
                                break;
                            case "DateOfBirth":
                            
                            _presenter.PatientInfo.DateOfBirth = data[1];
                                break;
                            case "MedicalRecordNo":
                            
                            _presenter.PatientInfo.MedicalRecordNo = data[1];
                                break;
                            case "NOTE":
                            
                            _presenter.PatientInfo.NoteTxt = data[1];
                                break;
                            case "Gender":
                           
                            _presenter.PatientInfo.Genderstatus = data[1];
                                break;
                            case "Maritalstatus":
                           
                            _presenter.PatientInfo.Maritialstatus = data[1];
                                break;

                            case "EmploymentStatus":
                         
                            _presenter.PatientInfo.Employmentstatus = data[1];
                                break;
                            case "ReferralSource":
                          
                            _presenter.PatientInfo.ReferralSource = data[1];
                                break;
                            case "Active":
                            
                            _presenter.PatientInfo.IsActive = data[1];
                                break;
                            
                             
                            default:
                           
                            break;
                        }

                    }
                }
                catch (Exception ex)
                {
                string test = ex.ToString();
                }
        }

        public void ResetPatientInfo()
        {
            _presenter.PatientInfo.FullName = string.Empty;
            _presenter.PatientInfo.DateOfBirth = string.Empty;
            _presenter.PatientInfo.MedicalRecordNo = string.Empty;
            _presenter.PatientInfo.NoteTxt = string.Empty;
            _presenter.PatientInfo.Genderstatus = string.Empty;
            _presenter.PatientInfo.Maritialstatus = string.Empty;
            _presenter.PatientInfo.Employmentstatus = string.Empty;
            _presenter.PatientInfo.ReferralSource = string.Empty;
            _presenter.PatientInfo.IsActive = string.Empty;
            _presenter.SmartCardID = string.Empty;
        }
        public void ResetResults()
        {
            _presenter.PatientInfo.Result1 = string.Empty;
           _presenter.PatientInfo.Result2 = string.Empty;
            _presenter.PatientInfo.Result3 = string.Empty;
            _presenter.PatientInfo.Result4 = string.Empty;
            _presenter.PatientInfo.Result5 = string.Empty;
            _presenter.PatientInfo.Result6 = string.Empty;
            _presenter.PatientInfo.Result7 = string.Empty;
            _presenter.PatientInfo.Result8 = string.Empty;
            _presenter.PatientInfo.Result9 = string.Empty;
            _presenter.PatientInfo.Result10 = string.Empty;
            _presenter.PatientInfo.Result11 = string.Empty;
        }
        public async void ReadPatientInstrumentData(PatientInformation.PatientInfoTab tab ,bool deploy=true)
        {
          
            try
            {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("SIMULATED_PatientInformation.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);
                bool found = false;
                //temporary implementation
                foreach (var line in alltext)
                {
                    string[] data = line.Split(':');

                    switch (data[0])
                    {
                        case "HEIGHT":
                           
                            if (tab == PatientInformation.PatientInfoTab.HEIGHT )
                            {
                                found = true;
                                if (deploy)
                                    _presenter.PatientInfo.Result1 = data[1];
                                else
                                    _presenter.PatientInfo.Result1 = "";
                            }
                               
                            break;

                        case "WEIGHT":
                            if (tab == PatientInformation.PatientInfoTab.WEIGHT)
                            {
                                found = true;
                                if (deploy)
                                    _presenter.PatientInfo.Result2 = data[1];
                                else
                                    _presenter.PatientInfo.Result2 = "";
                            }
                            
                            break;
                        case "BPUP":
                            if (tab == PatientInformation.PatientInfoTab.BP)
                            {
                                found = true;
                                if (deploy)
                                    _presenter.PatientInfo.Result3 = data[1];
                                else
                                    _presenter.PatientInfo.Result3 = "";

                            }
                            break;
                        case "PO":
                            if (tab == PatientInformation.PatientInfoTab.PO)
                            {
                                found = true;
                                if (deploy)
                                    _presenter.PatientInfo.Result4 = data[1];
                                else
                                    _presenter.PatientInfo.Result4 = "";

                            }
                            break;
                        case "TEMP":
                            if (tab == PatientInformation.PatientInfoTab.TERMO)
                            {
                                
                                found = true;
                                if(deploy)
                                    _presenter.PatientInfo.Result5 = data[1];
                                else
                                    _presenter.PatientInfo.Result5 = "";
                            }
                            break;
                        case "DERMOSCOPE":
                            if (tab == PatientInformation.PatientInfoTab.DERMASCOPE)
                            {
                                if (deploy)
                                    _presenter.PatientInfo.Result6 = data[1];
                                else
                                    _presenter.PatientInfo.Result6 = "";
                                found = true;
                            }
                            break;
                        case "OTOSCOPE":
                            if (tab == PatientInformation.PatientInfoTab.OTOSCOPE)
                            {
                                if (deploy)
                                    _presenter.PatientInfo.Result7 = data[1];
                                else
                                    _presenter.PatientInfo.Result7 = "";
                               found = true;
                            }
                            break;
                        case "EKG":
                            if (tab == PatientInformation.PatientInfoTab.EKG)
                            {
                                if (deploy)
                                    _presenter.PatientInfo.Result9 = data[1];
                                else
                                    _presenter.PatientInfo.Result9 = "";
                                found = true;
                            }
                            break;
                        case "SPIROMETER":
                            if (tab == PatientInformation.PatientInfoTab.SPIROMETER)
                            {
                                if (deploy)
                                    _presenter.PatientInfo.Result10 = data[1];
                                else
                                    _presenter.PatientInfo.Result10 = "";
                                found = true;
                            }
                            break;
                        case "GLOCOSE":
                            if (tab == PatientInformation.PatientInfoTab.GLOCOSE)
                            {
                                if (deploy)
                                    _presenter.PatientInfo.Result11 = data[1];
                                else
                                _presenter.PatientInfo.Result11 = "";
                                found = true;
                            }
                            break;
                        default:

                            break;
                    }

                    if (found)
                        break;
                }
            }
            catch (Exception ex)
            {
                string test = ex.ToString();
            }


        }

        public async void ReadVitalData()
        {
            try
            {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("SIMULATED_VitalData.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);
                //temporary implementation
                foreach (var line in alltext)
                {
                        string[] data = line.Split(':');

                        switch (data[0])
                        {
                            case "PulseRate":
                            _presenter.PatientInfo.PulseRate = data[1];
                            _presenter.PatientInfo.PulseRateComments = data[2];
                                break;
                            case "BloodPressure":
                            _presenter.PatientInfo.BloodPressure = data[1];
                            _presenter.PatientInfo.BloodPressureComments = data[2];
                                break;
                            case "Weight":
                            _presenter.PatientInfo.Weight = data[1];
                            _presenter.PatientInfo.WeightComments = data[2];
                                break;
                            case "Temprature":
                            _presenter.PatientInfo.Temprature = data[1];
                            _presenter.PatientInfo.TempratureComments = data[2];
                                break;
                            case "Resiratory":
                            _presenter.PatientInfo.Resiratory = data[1];
                            _presenter.PatientInfo.ResiratoryComments = data[2];
                                break;
                            default:
                                break;
                        }

                    }
                }
                catch (Exception)
                {
                     
                }

        }

        bool _EKG = false;
        public   void StartEKG(bool ekg)
        {
            _EKG = ekg;

            if (ekg)
                EKG();
        }

        public void StartPO(bool ekg)
        {
            _EKG = ekg;

            if (ekg)
                PO();
        }

        bool  _spirometer=false;
        public void StartSpirometer(bool spirometer)
        {
            _spirometer = spirometer;

            if (_spirometer)
                Spirometer();
        }

        async void Spirometer()
        {
            Random _random = new Random();
            for (int i = 0; i < 10000; i++)
            {
                if (!_spirometer)
                    break;
                int a = _random.Next(50, 100);
               
                if (_presenter.IsCabinSW)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreRX,(int) InstrumentsID.SPIROMETER, "@" + a.ToString());

                    SendMessage(msg);
                }

                _presenter.StartChartDisplay(a.ToString());
                if (!_spirometer)
                    break;
                await Task.Delay(TimeSpan.FromSeconds(1));
                
            }
        }
        async void PO()
        {
            Random _random = new Random();
            for (int i = 0; i < 10000; i++)
            {
                if (!_EKG)
                    break;
                int a = _random.Next(50, 100);
               
                if (_presenter.IsCabinSW)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreRX,(int) InstrumentsID.PULSEOXYMETER, "@" + a.ToString());

                    SendMessage(msg);
                }

                _presenter.StartChartDisplay(a.ToString());

                if (!_EKG)
                    break;
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

       public void ClearChart()
        {
            _presenter.StartChartDisplay("clear");
        }
        async void EKG()
        { 
            Random _random = new Random();
            for (int i = 0; i < 10000; i++)
            {
                if (!_EKG)
                    break;
                int a = _random.Next(120,200);
               
                if (_presenter.IsCabinSW)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreRX, (int)InstrumentsID.EKG, "@"+a.ToString());
                 
                    SendMessage(msg);
                }
                _presenter.StartChartDisplay(a.ToString());
                if (!_EKG)
                    break;
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

       public async void SaveNote(string note)
        {
            try
            {
               string []notes=note.Split('\r');

                string name = _presenter.PatientInfo.FullName.Replace(" ", "_");
                string filename = _presenter.PatientInfo.MedicalRecordNo +"_"+ name+".txt";
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile pinfofile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);
                string   heading  = "NOTE:" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString()+"\r";
                string[] headings = heading.Split('\r');
                await Windows.Storage.FileIO.AppendLinesAsync(pinfofile, headings,Windows.Storage.Streams.UnicodeEncoding.Utf8);
                await Windows.Storage.FileIO.AppendLinesAsync(pinfofile, notes,Windows.Storage.Streams.UnicodeEncoding.Utf8);
            }
            catch(Exception)
            { }

        }

        public async void LogMessage(string msg)
        {
            try
            {
                msg = DateTime.Now.ToString() + ":"+Environment.NewLine + msg + Environment.NewLine;
                string filename = "commLog.txt";
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile pinfofile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);
                    await Windows.Storage.FileIO.AppendTextAsync(pinfofile, msg, Windows.Storage.Streams.UnicodeEncoding.Utf8 );
               

            }
            catch (Exception)
            { }
        }

        public async void LogSerialPortMessage(string msg)
        {
            try
            {
                msg =   msg + Environment.NewLine;
                string filename = "SerialDeviceLog.txt";
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile pinfofile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.OpenIfExists);
                await Windows.Storage.FileIO.AppendTextAsync(pinfofile, msg, Windows.Storage.Streams.UnicodeEncoding.Utf8);


            }
            catch (Exception)
            { }
        }

        public void Dispose()
        {
            if (coreCommunication != null)
                coreCommunication.Dispose();
        }
    }
}
