﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.SerialCommunication;
using Windows.Devices.Enumeration;
using System.Diagnostics;
using serialDevices;
using CommunicationModule;
using UMC;
namespace serialDevices
{
  public  class SerialDeviceCommMgr
    {
        Dictionary<string, Device> _deviceList = new Dictionary<string, Device>();
        // Target all Serial Devices present on the system
        DeviceWatcher deviceWatcher = null;
        bool resetAll = false;
        UMCPresenter _presenter = null;
        public void Initialize()
        {
            //Advanced Query String for all serial devices
            var deviceSelector = SerialDevice.GetDeviceSelector();
            // Create a device watcher to look for instances of the Serial Device  
            deviceWatcher = DeviceInformation.CreateWatcher(deviceSelector);
            deviceWatcher.Added += DeviceWatcher_Added;
            deviceWatcher.Removed += DeviceWatcher_Removed;
            deviceWatcher.Updated += DeviceWatcher_Updated;
            deviceWatcher.EnumerationCompleted += DeviceWatcher_EnumerationCompleted;
            deviceWatcher.Start();
        }

        public UMCPresenter Presenter
        {
            set { _presenter = value; }
            get { return _presenter; }
        }
        private void DeviceWatcher_EnumerationCompleted(DeviceWatcher sender, object args)
        {
            ReadSerialDev();
        }

        private void DeviceWatcher_Updated(DeviceWatcher sender, DeviceInformationUpdate devInfo)
        {
            if (_deviceList.ContainsKey(devInfo.Id))
            {
                Device dev = new Device(devInfo, devInfo.Id,_presenter);
            }
        }

        private void DeviceWatcher_Removed(DeviceWatcher sender, DeviceInformationUpdate dev)
        {
            if (_deviceList.ContainsKey(dev.Id))
            {
                Device dv = _deviceList[dev.Id];
                _deviceList.Remove(dev.Id);
                _presenter.MessageReceived( "SERIALDEVICE-REMOVED", dv.PortName + ": " + dv.DeviceName );
              //  _presenter.NotifyError(this, new TXRXeventsArgs("SERIALDEVICE-REMOVED",dv.PortName+": "+dv.DeviceName));
            }
        }

        private async void DeviceWatcher_Added(DeviceWatcher sender, DeviceInformation devInfo)
        {
            Debug.WriteLine(devInfo.Id);
            if (!_deviceList.ContainsKey(devInfo.Id))
            {
                Device dev = new Device(devInfo, devInfo.Id,_presenter);
                Debug.WriteLine(dev.DeviceInstanceId);
                try
                {
                    bool res = await dev.Open();
                    if (res)
                    { 
                        _deviceList.Add(devInfo.Id, dev);
                     //_presenter.NotifyError(this, new TXRXeventsArgs( dev.PortName+": "+dev.DeviceName));

                    }
                }
                catch (Exception    )
                {

                }
            }
        }

        public void SystemCommand(string instr, string cmd)
        {
            Write(instr,cmd);
        }
        /// <summary>
        /// Write data to a specific instruments
        /// </summary>
        /// <param name="instr"></param>
        /// <param name="cmd"></param>
        private void Write(string instr,string cmd)
        {
            Device dev = null;
            foreach( var v in _deviceList)
            {
                if (v.Value.PortName.Equals(instr))
                {
                    dev = v.Value;
                    break;
                }
            }

            if(dev !=null)
            {
                dev.WriteData(cmd);
            }

        }

       /// <summary>
       /// Read all Device data
       /// </summary>
        public async  void ReadSerialDev()
        {
            while (true)
            {
                if (resetAll)
                    break;
                List<string> keys = new List<string>(_deviceList.Keys);
                foreach (string key in keys)
                {
                    if (resetAll)
                        break;
                    try
                    {
                        _deviceList[key].ReadData();
                    }catch(Exception)
                    { }
                    await Task.Delay(1);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
       public void ResetAll()
        {
            resetAll = true;
            if (deviceWatcher != null)
            {
                deviceWatcher.Stop();
                deviceWatcher.Added -= DeviceWatcher_Added;
                deviceWatcher.Removed -= DeviceWatcher_Removed;
                deviceWatcher = null;
            }

            List<string> keys = new List<string>(_deviceList.Keys);
            foreach (string key in keys)
            {
                _deviceList[key].Close(); 
            }

            _deviceList.Clear(); 
        }

    }
}
