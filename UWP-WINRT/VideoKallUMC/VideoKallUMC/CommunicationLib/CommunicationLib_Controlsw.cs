﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Networking;
using Windows.Networking.Connectivity;
using Windows.Storage.Streams;
using System.Net;
namespace CommunicationModule
{
   public class CommunicationLibControlSW: IDisposable
    {
        string ipaddress = String.Empty;
        string portNo = String.Empty;
        DatagramSocket selfhost = null;
        uint inboundBufferSize = 2048;
        
        DataWriter writer = null;
         
        public event EventHandler<CommunicationMsg> MessageReceived;
        public CommunicationLibControlSW()
        {
           
        }

       public string IPAddress
        {
            set { ipaddress = value; }
            get { return ipaddress; }
        }

        public string Port
        {
            set { portNo = value; }
            get { return portNo; }
        }

        
        public void CrearteCommunicationChannel()
        {
             
            selfhost = new DatagramSocket();
            selfhost.MessageReceived += RemoteMessageReceived;
            // Refer to the DatagramSocketControl class' MSDN documentation for the full list of control options.
            selfhost.Control.InboundBufferSizeInBytes = inboundBufferSize;

            // Set the IP DF (Don't Fragment) flag.
            // Refer to the DatagramSocketControl class' MSDN documentation for the full list of control options.
            selfhost.Control.DontFragment = true;

             
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private  void RemoteMessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args)
        {
            try
            {
                uint stringLength = args.GetDataReader().UnconsumedBufferLength;
                //      string receivedMessage = args.GetDataReader().ReadString(stringLength);

                byte[] buffer = new byte[stringLength];
                args.GetDataReader().ReadBytes(buffer);
                string str = Encoding.Unicode.GetString(buffer);
                MessageReceived?.Invoke(this, new CommunicationMsg(str));
            }
            catch(Exception e)
            {
                string s = e.ToString();

                MessageReceived?.Invoke(this, new CommunicationMsg("Error:"+s)); ;
            }

        }

         

        /// <summary>
        /// 
        /// </summary>
        public async Task<int> Connect()
        {
            try
            {
              //  MessageReceived?.Invoke(this, new CommunicationMsg("C1")); ;

                HostName host = new HostName(ipaddress);
                  await selfhost.ConnectAsync(host, portNo);
              

            }
            catch(Exception ex)
            {
                string s = ex.ToString();
             //   string s = e.ToString();

                MessageReceived?.Invoke(this, new CommunicationMsg("Error:" + s)); ;

            }
            return 0;
        }

       

        /// <summary>
        /// 
        /// </summary>
        public void   SendData(string msg)
        {
           
            if ( selfhost != null)
            {
                try
                {
                   // MessageReceived?.Invoke(this, new CommunicationMsg("S1")); ;
                    if (writer == null)
                        writer = new DataWriter(selfhost.OutputStream);
                    string stringToSend = msg;
                    writer.WriteString(stringToSend);

                    writer?.StoreAsync();
                }catch(Exception e)
                {
                    string s = e.ToString();

                    MessageReceived?.Invoke(this, new CommunicationMsg("Error:" +"Send"+ s)); ;

                }
            }
            
        }

        public void CleanUp()
        {
            if (selfhost != null)
            {
                selfhost.Dispose();
            }
             

            if (writer != null)
                writer.DetachStream();
            writer = null;
        }

        public    string    LocalIPAddress()
        {
            var icp = NetworkInformation.GetInternetConnectionProfile();

            if (icp?.NetworkAdapter == null) return "";
            var hostname =
                NetworkInformation.GetHostNames()
                    .SingleOrDefault(
                        hn =>
                            hn.IPInformation?.NetworkAdapter != null && hn.IPInformation.NetworkAdapter.NetworkAdapterId
                            == icp.NetworkAdapter.NetworkAdapterId);
            return hostname?.CanonicalName;
        }

        public void Dispose()
        {
             if (selfhost != null)
            {
                selfhost.Dispose();
            }
            
            if (writer != null)
                writer.DetachStream();
            writer = null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
   
  }
