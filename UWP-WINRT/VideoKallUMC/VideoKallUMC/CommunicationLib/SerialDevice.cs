﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using System.Diagnostics;
using Windows.Storage.Streams;
using UMC;
using CommunicationModule;

namespace serialDevices
{
  public  struct DeviceConfiguration
    {
      public  uint BaudRate;
      public  SerialParity Parity;
      public  SerialStopBitCount StopBits;
      public  UInt16 _dataBits;
      public  SerialHandshake Handshake;
      public  Boolean BreakSignalState_false;
      public  Boolean BreakSignalState_true;

      public  Boolean IsDataTerminalReady_false;
      public  Boolean IsDataTerminalReady_true ;

      public  Boolean IsRequestToSendEnabled_false;
      public  Boolean IsRequestToSendEnabled_true ;
    }

    public class Device
    {
        DeviceInformation _deviceInfo = null;
        String _deviceSelector = String.Empty;
        DeviceInformationUpdate _deviceUpdate = null;
        DeviceConfiguration _deviceConfig;
        SerialDevice connectedDevice = null;
        DataReader _dataReaderObject = null;
        DataWriter _dataWriteObject = null;
        UMCPresenter _presenter = null;
        string _instrumentName = string.Empty;
        public Device(DeviceInformation info, string deviceSelector,UMCPresenter presenter)
        {
            _deviceInfo = info;
            _deviceSelector = deviceSelector;
            _presenter = presenter;
        }

        public Device(DeviceInformationUpdate info, string deviceSelector, UMCPresenter presenter)
        {
            _deviceUpdate = info;
            _deviceSelector = deviceSelector;
            _presenter = presenter;
        }

        public UMCPresenter Presenter
        {
            set { _presenter = value; }
            get { return _presenter; }
        }
        public DeviceConfiguration DeviceConfig
        {
            set { _deviceConfig = value; }
        }

        public SerialParity Parity
        {
             
            get { return connectedDevice.Parity; }
        }

        public SerialStopBitCount StopBits
        {
            get { return connectedDevice.StopBits; } 
        }

        public SerialHandshake Handshake
        {
            get { return connectedDevice.Handshake; } 
        }

        public UInt16 DataBits
        {
            get { return connectedDevice.DataBits; }
        }
        public DeviceInformation DeviceInfo
        {

            get { return _deviceInfo; }
        }

        public String DeviceSelector
        {

            get { return _deviceSelector; }
        }
        public string Id
        {
            get { return _deviceInfo.Id; } 
        }

        public string Name
        {
            get { return _deviceInfo.Name; }
        }

        public bool IsEnabled
        {
            get { return _deviceInfo.IsEnabled; }
        }

        public DeviceInformationPairing Pairing
        {
            get { return _deviceInfo.Pairing; }
        }

        public DeviceInformationKind Kind
        {
            get { return _deviceInfo.Kind; }
        }

        public string DeviceInstanceId
        {
            get { return _deviceInfo.Properties["System.Devices.DeviceInstanceId"] as String; }
        }

        public uint BaudRate
        { 
            get { return connectedDevice.BaudRate; }
        }

        public void Configure()
        {
            connectedDevice.ErrorReceived += ConnectedDevice_ErrorReceived;
            connectedDevice.PinChanged += ConnectedDevice_PinChanged ;
            switch (PortName)
            {
                case "COM1":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "Blood Pressure";
                    break;
                case "COM2":
                    connectedDevice.BaudRate = 4800;
                    _instrumentName = "TAT-5000 Thermometer";
                    break;
                case "COM3":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "Pulse Oxymeter";
                    break;
                case "COM4":
                    connectedDevice.BaudRate = 57600;
                    _instrumentName = "Not used";
                    break;
                case "COM5":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "Height & Weight";
                    break;
                case "COM6":
                    connectedDevice.BaudRate = 57600;
                    _instrumentName = "Stethoscope (& Chair)";
                    break;
                case "COM7":
                    connectedDevice.BaudRate = 57600;
                    _instrumentName = "Instrument cleaning";
                    break;
                case "COM8":
                    connectedDevice.BaudRate = 19200;
                    _instrumentName = "VSAT Modem";
                    break;
                case "COM9":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "none";
                    break;
                case "COM10":
                    connectedDevice.BaudRate = 19200;
                    _instrumentName = "Smart Card / Door opener";
                    break;
                case "COM11":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "Glucose Monitor";
                    break;
                case "COM12":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "DermoscopeCamera";
                    break;
                case "COM13":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "EKG";
                    break;
                case "COM14":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "OtoscopeCamera";
                    break;
                case "COM15":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "Spirometer";
                    break;
                case "COM16":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "Door";
                    break;
                case "COM17":
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = " ";
                    break;
                default:
                    connectedDevice.BaudRate = 9600;
                    _instrumentName = "None";
                    break;
            };
            _dataWriteObject = new DataWriter(connectedDevice.OutputStream);
            _dataReaderObject = new DataReader(connectedDevice.InputStream);
            _dataReaderObject.InputStreamOptions = InputStreamOptions.Partial;
            isConnected = true;
            
            _presenter.ModelObject.LogSerialPortMessage("Configuration");
            _presenter.ModelObject.LogSerialPortMessage("PortName :"+PortName);
            _presenter.ModelObject.LogSerialPortMessage("Instrument :" + _instrumentName);
            _presenter.ModelObject.LogSerialPortMessage("BaudRate: "+connectedDevice.BaudRate.ToString());
            _presenter.ModelObject.LogSerialPortMessage("DataBits: " + connectedDevice.DataBits.ToString());

            _presenter.ModelObject.LogSerialPortMessage("ReadTimeout: "+connectedDevice.ReadTimeout.ToString());
            _presenter.ModelObject.LogSerialPortMessage("Handshake: "+connectedDevice.Handshake.ToString());
            _presenter.ModelObject.LogSerialPortMessage("IsRequestToSendEnabled :"+connectedDevice.IsRequestToSendEnabled.ToString());
            _presenter.ModelObject.LogSerialPortMessage("Parity: "+connectedDevice.Parity.ToString());
            _presenter.ModelObject.LogSerialPortMessage("StopBits: "+connectedDevice.StopBits.ToString());
            _presenter.ModelObject.LogSerialPortMessage("UsbProductId: "+connectedDevice.UsbProductId.ToString());
            _presenter.ModelObject.LogSerialPortMessage("UsbVendorId: "+connectedDevice.UsbVendorId.ToString());
            _presenter.ModelObject.LogSerialPortMessage("WriteTimeout: "+connectedDevice.WriteTimeout.ToString());
            _presenter.ModelObject.LogSerialPortMessage("ReadTimeout: "+connectedDevice.ReadTimeout.ToString());
            _presenter.ModelObject.LogSerialPortMessage("IsDataTerminalReadyEnabled:"+connectedDevice.IsDataTerminalReadyEnabled.ToString());
            _presenter.ModelObject.LogSerialPortMessage("DataSetReadyState: "+connectedDevice.DataSetReadyState.ToString());
            _presenter.ModelObject.LogSerialPortMessage("BreakSignalState: "+connectedDevice.BreakSignalState.ToString()); 
        }


        public string DeviceName
        {
            get { return _instrumentName; }
        }
        private void ConnectedDevice_PinChanged(SerialDevice sender, PinChangedEventArgs args)
        {
            // SerialPort 
        }

        private void ConnectedDevice_ErrorReceived(SerialDevice sender, ErrorReceivedEventArgs args)
        {
            _presenter.ModelObject.LogSerialPortMessage(args.ToString());
        }

        public async Task<bool> Open() 
        {
            try
            {
                connectedDevice = await SerialDevice.FromIdAsync(Id);
                if(connectedDevice != null)
                {
                    Configure();
                }
                else
                {
                    var deviceAccessStatus = DeviceAccessInformation.CreateFromId(Id).CurrentStatus;

                    if (deviceAccessStatus == DeviceAccessStatus.DeniedByUser)
                    {
                        // "Access to the device was blocked by the user : " + deviceInfo.Id;
                        _presenter.ModelObject.LogSerialPortMessage("Access to the device was blocked by the user : " + Id.ToString()); 
                    }
                    else if (deviceAccessStatus == DeviceAccessStatus.DeniedBySystem)
                    {
                        // "Access to the device was blocked by the system : " + deviceInfo.Id;  
                        _presenter.ModelObject.LogSerialPortMessage("Access to the device was blocked by the system : " + Id.ToString());
                    }
                    else
                    {
                        // "Unknown error, possibly opened by another app : " + deviceInfo.Id;
                        _presenter.ModelObject.LogSerialPortMessage("Unknown error, possibly opened by another app : " + Id.ToString());
                    }
                }
            }
            catch(Exception ex)
            {
                string exp = ex.Message;
            }

            return (connectedDevice != null) ? true : false ;
        }

        public void Close()
        {
            if (connectedDevice != null)
            {
                connectedDevice.Dispose();
                connectedDevice = null;
            }

            if (_dataReaderObject != null)
            {
                _dataReaderObject.DetachStream();
                _dataReaderObject.Dispose();
                _dataReaderObject = null;
            }

            if (_dataWriteObject != null)
            {
                _dataWriteObject.DetachStream();
                _dataWriteObject.Dispose();
                _dataWriteObject = null;
            }

           
        }

        public async void ReadData()
        {
            try
            { 
                Task<UInt32> loadAsyncTask;

                uint ReadBufferLength = 1024;
                if (_dataReaderObject != null)
                {
                    loadAsyncTask = _dataReaderObject.LoadAsync(ReadBufferLength).AsTask();
                    UInt32 bytesRead = await loadAsyncTask;

                    string receivedStrings = "";
                    if (bytesRead > 0)
                    {

                         receivedStrings = _dataReaderObject.ReadString(bytesRead) ;
                        if (_presenter.IsCabinSW)
                            _presenter.SendMessage(receivedStrings);
                        _presenter.ModelObject.MessageReceived(null, new CommunicationMsg(receivedStrings));

                        _presenter.ModelObject.LogSerialPortMessage(PortName + "-" + "RX: " + receivedStrings );
                    }
                }
            }
            catch (Exception  )
            {

            }


            // Keep reading until we consume the complete stream.
            //  while (_dataReaderObject.UnconsumedBufferLength > 0)
            // {
            //  uint bytesToRead = _dataReaderObject.ReadUInt32();
            //  receivedStrings += _dataReaderObject.ReadString(bytesRead) + "\n";
            //  }
        }

        public async void WriteData(string cmd)
        {
            try
            { 
                char[] buffer = new char[4];
                cmd.CopyTo(0, buffer, 0, cmd.Length);
                String InputString = new string(buffer);

                UInt32 bytesWritten = 0;
                if (_dataWriteObject != null)
                {
                    Task<UInt32> storeAsyncTask;
                    _dataWriteObject.WriteString(InputString);
                    // Transfer data to the serial device now
                    storeAsyncTask = _dataWriteObject.StoreAsync().AsTask();
                    bytesWritten = await storeAsyncTask;
                }

                if (bytesWritten > 0)
                {
                    _presenter.ModelObject.LogSerialPortMessage(PortName + "-" + "TX:" + InputString);
                }
            }
            catch (Exception  )
            {

            }
        }

        public bool IsDeviceConnected
        {
            get { return isConnected; }
        }

        public string PortName
        {
            get { return connectedDevice.PortName; }
        }
        bool isConnected = false;
    }
}
