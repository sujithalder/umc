﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Networking;
using Windows.Networking.Connectivity;
using Windows.Storage.Streams;
using System.Net;
namespace CommunicationModule
{
   public class CommunicationLib: IDisposable
    {
        string ipaddress = String.Empty;
        string portNo = String.Empty;
        DatagramSocket npiSystem = null;
        DatagramSocket cabinSystem = null;
        bool _isCabinSW = true;
        uint inboundBufferSize = 2048;
       
        ConnectionInfo npiConnection = null;
        DataWriter writer = null;
        public event EventHandler<CommunicationMsg> MessageReceived;
        public CommunicationLib()
        {
           
        }

       public string IPAddress
        {
            set { ipaddress = value; }
            get { return ipaddress; }
        }

        public string Port
        {
            set { portNo = value; }
            get { return portNo; }
        }

       public bool IsCabinSW
        {
            set { _isCabinSW = value; }
            get { return _isCabinSW; }
        }
        public void CrearteCommunicationChannel()
        {
            if (_isCabinSW)
            {
                cabinSystem = new DatagramSocket();
                cabinSystem.MessageReceived += CABIN_MessageReceived;
                // Refer to the DatagramSocketControl class' MSDN documentation for the full list of control options.
                cabinSystem.Control.InboundBufferSizeInBytes = inboundBufferSize;

                // Set the IP DF (Don't Fragment) flag.
                // Refer to the DatagramSocketControl class' MSDN documentation for the full list of control options.
                cabinSystem.Control.DontFragment = true;

            }
            else
            {
                npiSystem = new DatagramSocket();
                npiSystem.MessageReceived += NPI_MessageReceived ;
                // Refer to the DatagramSocketControl class' MSDN documentation for the full list of control options.
                npiSystem.Control.InboundBufferSizeInBytes = inboundBufferSize;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private  void CABIN_MessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args)
        {
            try
            {
                uint stringLength = args.GetDataReader().UnconsumedBufferLength;
                string receivedMessage = args.GetDataReader().ReadString(stringLength);

                MessageReceived?.Invoke(this, new CommunicationMsg(receivedMessage));
            }
            catch(Exception )
            {

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public async void    NPI_MessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args)
        {
            try
            {
                if (npiConnection == null)
                {
                    IOutputStream outputStream = await npiSystem.GetOutputStreamAsync(
                      args.RemoteAddress,
                      args.RemotePort);
                    npiConnection = new ConnectionInfo(args.RemoteAddress.ToString(), args.RemotePort, outputStream);
                }

                uint stringLength = args.GetDataReader().UnconsumedBufferLength;
                string receivedMessage = args.GetDataReader().ReadString(stringLength);

                MessageReceived?.Invoke(this, new CommunicationMsg(receivedMessage));
            }
            catch(Exception)
            {
             
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public async void Connect()
        {
            try
            {
                HostName host = new HostName(ipaddress);
                if (_isCabinSW)
                {
                    await cabinSystem.ConnectAsync(host, portNo);

                }
                else
                {
                    await npiSystem.BindEndpointAsync(host, portNo);
                }
            }catch(Exception)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async  Task SendData(string msg)
        {

            if (_isCabinSW && cabinSystem != null)
            {
                try
                {
                    if (writer == null)
                        writer = new DataWriter(cabinSystem.OutputStream);
                    string stringToSend = msg;
                    writer.WriteString(stringToSend);
                    await writer?.StoreAsync();
                }catch(Exception)
                {

                }
            }
            else
            {
                if(npiConnection !=null)
                 await npiConnection.WriteData(msg);
            }
        }

        public void CleanUp()
        {
            if (npiSystem != null)
            {
                npiSystem.Dispose();
            }
            if (cabinSystem != null)
            {
                cabinSystem.Dispose();
            }
            if(npiConnection!=null)
            {
                npiConnection.CleanUp();
            }

            if (writer != null)
                writer.DetachStream();
            writer = null;
        }

        public    string    LocalIPAddress()
        {
            try
            {
                var icp = NetworkInformation.GetInternetConnectionProfile();

                if (icp?.NetworkAdapter == null) return "";
                var hostname =
                    NetworkInformation.GetHostNames()
                        .SingleOrDefault(
                            hn =>
                                hn.IPInformation?.NetworkAdapter != null && hn.IPInformation.NetworkAdapter.NetworkAdapterId
                                == icp.NetworkAdapter.NetworkAdapterId);

               return   hostname?.CanonicalName;
            }
            catch (Exception EX)
            {

            }
            return null;
        }

        public void Dispose()
        {
            if (npiSystem != null)
            {
                npiSystem.Dispose();
            }
            if (cabinSystem != null)
            {
                cabinSystem.Dispose();
            }
            if (npiConnection != null)
            {
                npiConnection.CleanUp();
            }

            if (writer != null)
                writer.DetachStream();
            writer = null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    class ConnectionInfo
    {
          string ipaddress = string.Empty;
          string portNo = string.Empty;
          IOutputStream outputstream = null;
        DataWriter writer = null;
        public  ConnectionInfo( string ip, string port, IOutputStream io)
        {
            outputstream = io;
            ipaddress = ip;
            portNo = port;
        }

        public async Task WriteData(string msg)
        {
            try
            {
                if (writer == null)
                    writer = new DataWriter(outputstream);
                writer.WriteString(msg);
                await writer?.StoreAsync();
            }
            catch(Exception)
            {

            }
           
         }

       public void CleanUp()
        {
            if(writer !=null)
            writer.DetachStream();

          if(outputstream !=null)
            outputstream.Dispose();
            writer = null;
            outputstream = null;

        }
    }

   public class CommunicationMsg:EventArgs
    {
        public CommunicationMsg(string msgin)
        {
            msg = msgin;
        }

        public CommunicationMsg(string msgin, string msgin2)
        {
            msg = msgin;
            msg2 = msgin2;
        }
        public  string msg = string.Empty;
        public string msg2 = string.Empty;
    }
}
