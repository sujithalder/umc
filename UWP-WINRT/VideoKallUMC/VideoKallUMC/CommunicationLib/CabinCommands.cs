﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMC
{
    public enum InstrumentsID
    {
        DERMASCOPE=1,
        THERMOMETER=2,
        PULSEOXYMETER=3,
        STETHOSCOPECHECST=4,
        STANDINGHEIGHT=5,
        SEATEDSCALWEIGHT=6,
        OTOSCOPECAMERA=7,
        EKG=8,
        SPIROMETER=9,
        GLUCOSEMONITOR=10,
        STETHOSCOPELUNGS=11,
         SMARTCARD=12,
         NPI=13,
         BP=14,
    }


    /// <summary>
    /// Command format, s<deviceid><command>e
    /// 1. start, 0-stop
    ///</summary>

    class CoreCommunicationTXRX
    { 
        //s-start of command
        /// <summary>
        /// s-start of command
        /// e-end of command
        /// {0} instrumentID - Command for the intended instruments
        /// {1} command 1-start, 0 -stop
        /// 
        /// </summary>
       public static readonly string coreTX = "s{0}{1}e";
       public static readonly string coreTXex = "s{0}{1}{2}e";
        //example- rx8@data\r\n
      public static readonly string coreRX = "rx{0}{1}\r\n";
      public static readonly string coreRXex = "rx{0}{1}{2}\r\n";

        public static   string BloodPressure = "COM1";
        public static  string TAT_5000Thermometer = "COM2";
        public  static string Pulse_Oxymeter = "COM3";
        public static  string Height_Weight = "COM5";
        public  static string Stethoscope_Chair = "COM6";
        public static  string Instrumentcleaning = "COM7";
        public static  string VSAT_Modem = "COM8";
        public static  string Smart_Card_Door_opener = "COM10";
        public static  string Glucose_Monitor = "COM11";
        public static   string DermoscopeCamera = "COM12";
        public static string EKG = "COM13";
        public static string OtoscopeCamera = "COM14";
        public static string Spirometer = "COM15";
        public static string SMARTCARD_DOOR_OPENER_WIEIGHSCALES_INDICATOR = "COM16";
        public static string Ins_BloodPressure_Indx = "1";
        public static string Ins_Thermometer_Indx = "2";
        public static string Ins_Pulse_Oxymeter_Indx = "3";
        public static string Ins_Height_Weight_Indx = "5";
        public static string Ins_Stethoscope_Chair_Indx = "6";
        public static string Ins_Instrumentcleaning_Indx = "7";
        public static string Ins_VSAT_Modem_Indx = "8";
        public static string Ins_Smart_Card_Door_opener_Indx = "10";
        public static string Ins_Glucose_Monitor_Indx = "11";
        public static string Ins_DermoscopeCamera_Indx = "12";
        public static string Ins_EKG_Indx = "13";
        public static string Ins_OtoscopeCamera_Indx = "14";
        public static string Ins_Spirometer_Indx = "15";

        public static bool simulatedResponse = false;
    }


}
