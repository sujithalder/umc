﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.Samples.SimpleCommunication;
using System.Threading;
using System.Threading.Tasks;
using Windows.Media.MediaProperties;
using Windows.UI.Core;
using Windows.Media.Capture;
using VideoStreamingModule;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using UMC;
// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace UMC.VideoConference
{
    /// <summary>
    /// This class is responsible for Video call operations
    /// </summary>
    public sealed partial class VideoUI : UserControl
    {

        CaptureDevice device = null;
        bool? roleIsActive = null;
        int isTerminator = 0;
        bool activated = true;
        public static event EventHandler<EventArgs> InitializationEvent;
        private static Windows.Media.MediaExtensionManager mediaExtensionMgr;
        static UMCPresenter _presenter;
        public   event EventHandler<EventArgs> CameraCommands;
       
        public VideoUI()
        {
            this.InitializeComponent();
            EnsureMediaExtensionManager();
            CameraCommands += new EventHandler<EventArgs>(HandleCameraCommands);
            if (_presenter != null)
                _presenter.Cameracommands += new EventHandler<EventArgs>(HandleCameraCommands);
        }

        /// <summary>
        /// CAMERA COMMANDS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async  void HandleCameraCommands(object sender, EventArgs e)
        {
            string msg = ((VideostreamingMsg)e).msg;
            switch (msg)
            {
                case "START":
                    StartCall();
                    break;
                case "STOPCAMERA":
                    await EndVideoCall();
                    break;
                case "RESTART":
                    await EndCallAndReinitialize();
                    break;
                case "TAKEPIC":
                    CapturePhoto(null, null);
                    break;
                case "MUTE":
                    MuteAudio();
                    break;
                case "ACCEPTCALL":
                    await  AcceptCall();
                    break;
                case "REFUSEDCALL":
                    await  RejectCall();
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static UMCPresenter Presenter
        {
            set
            {
                _presenter = value;
            }
            get { return _presenter; }
        }

        string _ip = string.Empty;
        public string IP 
            {
            set { _ip = value; }
            get { return _ip; }
            }

         
        /// <summary>
        /// read IP Address
        /// </summary>
       public async void IPAddress()
        {
            try
            {
                var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("config.txt");
                var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);

                foreach (var line in alltext)
                {
                    if (line.Contains("IP:"))
                    {
                        string[] data = line.Split(':');
                        IP = data[1];
                        break;
                    }
                }
            }catch(Exception)
            { }
            
        }
        /// <summary>
        /// Initialize the Camera and select USB microphone or any microphone.
        /// </summary>
        /// <returns></returns>
        public async Task   InitializeDevice()
        {
            IPAddress();
            var cameraFound = await CaptureDevice.CheckForRecordingDeviceAsync(true);
          
            if (cameraFound)
            {
                  cameraFound = await CaptureDevice.CheckForRecordingDeviceAsync(false);
                if(!cameraFound)
                {
                    string msg = "Please Connect the Microphone.";
                    _presenter.NotifyError(this, new TXRXeventsArgs(msg));
                }

            }
            else
            {
                string msg = "Please Connect the Camera.";
                _presenter.NotifyError(this, new TXRXeventsArgs(msg));
            }
            
            if (cameraFound)
            {
                device = new CaptureDevice();
                device._presenter = _presenter;
                await InitializeAsync();
                device.IncomingConnectionArrived += Device_IncomingConnectionArrived;
                device.CaptureFailed += Device_CaptureFailed;
                RemoteVideo.MediaFailed += RemoteVideo_MediaFailed;

                InitializationEvent?.Invoke(this, new VideostreamingMsg("CameraInitializationCompleted"));

                string msg = "Ready to start call.";
                _presenter.NotifyError(this, new TXRXeventsArgs(msg));
            }
            
        }

        
        /// <summary>
        /// shutdown camera
        /// </summary>
        /// <returns></returns>
        public async Task Cleanup()
        {

            if (activated)
            {
                RemoteVideo.Stop();
                RemoteVideo.Source = null;
            }

            if (device != null)
            {
                await device.CleanUpAsync();
                device = null;
            }
        }

        /// <summary>
        /// start the call.
        /// </summary>
        public  void StartCall()
        {
            var address = IP;
           
            if (!String.IsNullOrEmpty(address))
            {
                roleIsActive = true;
               RemoteVideo.Source = new Uri("stsp://" + address);
                string msg = "initiating connection. IP: " + address;
                _presenter.NotifyError(this, new TXRXeventsArgs(msg));;
                
            }
            else
            {
                _presenter.NotifyError(this, new TXRXeventsArgs("IP Address is Not configured."));
                InitializationEvent?.Invoke(this, new VideostreamingMsg("IPADDRESSISNOTCONFIGURED"));

            }
        }

        /// <summary>
        /// end the video call
        /// </summary>
        /// <returns></returns>
        async Task EndVideoCall()
        {
            try
            {
                incommingCall = null;
                if (Interlocked.CompareExchange(ref isTerminator, 1, 0) == 0)
                {
                    await EndCallAsync();
                }
            }catch(Exception)
            { }
        }
     
        /// <summary>
        /// reinitialize the camera.
        /// </summary>
        /// <returns></returns>
        public async Task EndCallAndReinitialize()
        {
          try
            {
                incommingCall = null;
                await EndVideoCall();
            }
            catch(Exception)
            { }
         
          await Cleanup();
          await InitializeDevice();
         }

        double vol = 0;
        void MuteAudio()
        {
            if (RemoteVideo.Volume > 0)
            {
                vol = RemoteVideo.Volume;
                RemoteVideo.Volume = 0;

                _presenter.NotifyError(this, new TXRXeventsArgs("Video in Mute,Receiving stethoscope stream."));
            }
            else
            {
                RemoteVideo.Volume = vol;
                _presenter.NotifyError(this, new TXRXeventsArgs(""));
            }

        }
        
        /// <summary>
        /// capture photo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async void CapturePhoto(object sender, RoutedEventArgs e)
        {
            try
            {
                ImageEncodingProperties imgFormat = ImageEncodingProperties.CreateJpeg();

                var test = Windows.ApplicationModel.Package.Current.InstalledLocation;
                DateTime dt = DateTime.Now;
                String IMAGENAME = String.Format("{0}_{1}_{2}_{3}_{4}_{5}", dt.Month.ToString(), dt.Day.ToString(),
                    dt.Year.ToString(), dt.Hour.ToString(), dt.Minute.ToString(), dt.Second.ToString());
                // create storage file in local app storage
                StorageFile file = await Windows.ApplicationModel.Package.Current.InstalledLocation.CreateFileAsync(IMAGENAME + ".jpg",
                    CreationCollisionOption.GenerateUniqueName);

                await device.CapturePhotoToStorageFileAsync(imgFormat, file);
            }catch(Exception)
            { }
        }

      

        private void BtnPhoto_Click(object sender, RoutedEventArgs e)
        {
            CameraCommands?.Invoke(this, new VideostreamingMsg("TAKEPIC"));
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            CameraCommands?.Invoke(this, new VideostreamingMsg("START"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnInitialize_Click(object sender, RoutedEventArgs e)
        {
            CameraCommands?.Invoke(this, new VideostreamingMsg("RESTART"));
        }

       
        private void BtnMute_Click(object sender, RoutedEventArgs e)
        {
            CameraCommands?.Invoke(this, new VideostreamingMsg("MUTE"));
        }

        #region CAMERA API

        /// <summary>
        /// media failure. ex connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void RemoteVideo_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            string msg = e.ErrorMessage;
             _presenter.NotifyError(this, new TXRXeventsArgs("Remote call ended."));

            InitializationEvent?.Invoke(this, new VideostreamingMsg("RemoteVideoFailed"));
            if (Interlocked.CompareExchange(ref isTerminator, 1, 0) == 0)
            {
                await EndCallAsync();
            }

        }

        /// <summary>
        /// Connected with remote system and streaming video
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       IncomingConnectionEventArgs incommingCall = null;
        private    async   void Device_IncomingConnectionArrived(object sender, IncomingConnectionEventArgs e)
        {
            incommingCall = e;
            InitializationEvent?.Invoke(this, new VideostreamingMsg("INCOMINGCALL"));

            if (!_presenter.IsCabinSW)
            {
                _presenter.NotifyError(this, new TXRXeventsArgs("Incoming call."));
            }
            else
            { 
                incommingCall.Accept();
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                {
                    activated = true;
                    var remoteAddress = incommingCall.RemoteUrl;

                    Interlocked.Exchange(ref isTerminator, 0);

                    if (!((bool)roleIsActive))
                    {
                    // Passive client
                      RemoteVideo.Source = new Uri(remoteAddress);
                    }

                    InitializationEvent?.Invoke(this, new VideostreamingMsg("CABINCALLCONNECTED"));

                    remoteAddress = remoteAddress.Replace("stsp://", "");
                    _presenter.NotifyError(this, new TXRXeventsArgs("Connected.Remote machine address: " + remoteAddress));

                   
                }));
            }
        }

        private async Task   AcceptCall()
        {
            if (incommingCall == null)
                return;
             incommingCall.Accept();
             await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                activated = true;
                var remoteAddress = incommingCall.RemoteUrl;

                Interlocked.Exchange(ref isTerminator, 0);

                if (!((bool)roleIsActive))
                {
                    // Passive client
                    RemoteVideo.Source = new Uri(remoteAddress);
                }

                InitializationEvent?.Invoke(this, new VideostreamingMsg("CABINCALLCONNECTED"));

                remoteAddress = remoteAddress.Replace("stsp://", "");
               _presenter.NotifyError(this, new TXRXeventsArgs("Connected.Remote machine address: " + remoteAddress));
            }));

        }

        public async  Task RejectCall()
        {
            if (incommingCall == null)
                return;
         
            incommingCall.Refuse();
            await EndVideoCall();
         
        
            incommingCall = null;
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                InitializationEvent?.Invoke(this, new VideostreamingMsg("REFUSEDCALL"));
            }));
                
        }

        private async Task StartRecordToCustomSink()
        {
            MediaEncodingProfile mep = MediaEncodingProfile.CreateMp4(VideoEncodingQuality.Qvga);

            mep.Video.FrameRate.Numerator = 15;
            mep.Video.FrameRate.Denominator = 1;
            mep.Container = null;

            await device.StartRecordingAsync(mep);
            PreviewVideo.Source = device.CaptureSource;
            await device.CaptureSource.StartPreviewAsync();


            PreviewVideo.Visibility = Visibility.Visible;
        }

        private async Task EndCallAsync()
        {
            await device.CaptureSource.StopPreviewAsync();
            await device.CleanUpAsync();

            // end the CallButton. session
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, (() =>
            {
                RemoteVideo.Stop();
                RemoteVideo.Source = null;
            }));
        }

        private async Task InitializeAsync()
        {
            try
            {
                string msg = "Initializing..";
                _presenter.NotifyError(this, new TXRXeventsArgs(msg));
                await device.InitializeAsync();
                await StartRecordToCustomSink();
                RemoteVideo.Source = null;

                // Each client starts out as passive
                roleIsActive = false;
                Interlocked.Exchange(ref isTerminator, 0);
                InitializationEvent?.Invoke(this, new VideostreamingMsg("Initializing..."));
            }
            catch (Exception ex)
            {
                string msg = ex.ToString() + "Initialization error";
                _presenter.NotifyError(this, new TXRXeventsArgs(msg));
            }
        }


        async void Device_CaptureFailed(object sender, MediaCaptureFailedEventArgs e)
        {
            try
            {
                InitializationEvent?.Invoke(this, new VideostreamingMsg("CaptureFailed"));
                if (Interlocked.CompareExchange(ref isTerminator, 1, 0) == 0)
                {
                    await EndCallAsync();
                }
            }catch(Exception ex)
            {
                string[] msg = ex.Message.Split('\r');
                if (msg.Length > 0)
                {
                    _presenter.NotifyError(this, new TXRXeventsArgs(msg[0]));
                    InitializationEvent?.Invoke(this, new VideostreamingMsg("DeviceRemoved", msg[0]));
                }
            }
        }

        public void EnsureMediaExtensionManager()
        {
            if (mediaExtensionMgr == null)
            {
                mediaExtensionMgr = new Windows.Media.MediaExtensionManager();
                mediaExtensionMgr.RegisterSchemeHandler("Microsoft.Samples.SimpleCommunication.StspSchemeHandler", "stsp:");
            }
        }
        #endregion

       
    }

    public class VideostreamingMsg : EventArgs
    {
        public VideostreamingMsg()
        { }
        public VideostreamingMsg(string msgin)
        {
            msg = msgin;
        }
        public VideostreamingMsg(string msgin,string msgex)
        {
            msg = msgin;
            MsgEx = msgex;
        }
        public string msg;
       public string MsgEx;
    }
}
