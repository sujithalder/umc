﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using UMC.VideoConference;
using Microsoft.Toolkit.Uwp;
using Windows.Media.Devices;
using Windows.Storage;
using Windows.UI;
using Windows.System;
using CommunicationModule;
using Windows.UI.ViewManagement;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Core.Preview;
using Windows.Storage.Pickers;
using serialDevices;
using TestRigFrame;
using static TestRigFrame.TestrigframeUI;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UMC
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, IDisposable
    {
        UMCPresenter presenter = null;
        UMCModel umcmodel = null;
        //this flag set CABIN sw OR NPI
        bool isCabinSw = true;
        //TURE When incoming call received from CABIN
        bool bincomingCall = false;
        //    event EventHandler<EventArgs> DisplayMessage;
        //SerialDeviceCommMgr serialDeviceMgr = null;
        public MainPage()
        {
            presenter = new UMCPresenter();
            presenter.TXRXevents += new EventHandler<EventArgs>(TXRXeventHandlerFunc);
            presenter.CoreCommMessageReceived += new EventHandler<EventArgs>(CoreCommunicationMessage);
            presenter.TestCommRigMessageReceived += new EventHandler<EventArgs>(TestRigFrameCommunicationMessage);
            umcmodel = new UMCModel();
            presenter.ModelObject = umcmodel;
            VideoUI.Presenter = presenter;
            presenter.IsCabinSW = isCabinSw;
            TestrigframeUI.Presenter = presenter;
            VideoUI.InitializationEvent += new EventHandler<EventArgs>(HandleCameraEvents);
            ChartUI.Presenter = presenter; 
            //the above variable need to be set before GUI initialization.
            this.InitializeComponent();
            //set the datacontext for data binding
            SetDataContext();
            //chnage the title and Accept or Connect button 
            if (isCabinSw)
            {
                lblTitle.Text = "UMC CABIN ";
                btnConnect.Content = "Connect";
                _intractivemode = false;
            }

            //   ApplicationView view = ApplicationView.GetForCurrentView();
            //     view.TryEnterFullScreenMode();

        }

        private async  void TestRigFrameCommunicationMessage(object sender, EventArgs e)
        {
            string msg = ((ChartDataeventMsg)e).Msg;

            if(msg.Contains("ERROR"))
            {
                string []error = msg.Split(':');
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                {
                    lblVideoStatus.Text = error[2];
                    cleaningstate[0] = 2;
                    if (msg.Contains("Cleaning PreCondition Failed."))
                    {
                        presenter.ModelObject.LogMessage(msg);
                        BtnControllerOperation(0, false);
                    }
                }));
                }

            if(msg.Contains("SERIALDEVICESTATUS"))
            {
                string[] statusmsg = msg.Split(':');
                TestRigPannel panel = TestRigPannel.UNKNOWN;
                int state = -1;
                if(statusmsg.Length>0)
                {
                    panel = (TestRigPannel)Convert.ToInt32(statusmsg[1]);
                    state = Convert.ToInt32(statusmsg[2]);
                    switch(panel)
                    {
                        case TestRigPannel.CLEANING_STATUS:
                            HandleCleaningSystem("SOEC");
                            break;
                    }
                }

            }


            TestrigframeUICtrl.UpdateTestRig(msg); 
        }

        /// <summary>
        /// Handle the message received from different module and display the status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TXRXeventHandlerFunc(object sender, EventArgs e)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                lblVideoStatus.Visibility = Visibility.Visible;
                lblVideoStatus.Text = "Status: ";
                VideoStatus.Text = ((TXRXeventsArgs)e).msg;

                if (!isCabinSw)
                {
                    SolidColorBrush br = new SolidColorBrush
                    {
                        Color = Colors.LightSkyBlue
                    };
                    string recvmsg = ((TXRXeventsArgs)e).msg;

                    if (recvmsg.Contains("Receiving stream"))
                    {
                        presenter.MuteAudio(true);
                        if (isSthethoscopeChest)
                        {
                            btnStethoscopeChest.Background = br;
                        }
                        else
                        {
                            btnLungstethoscope.Background = br;
                            StethoscopeUI.UpdateColor(6);
                        }
                    }
                    else
                    {
                        if (recvmsg.Contains("Stopped receiving"))
                        {
                            presenter.MuteAudio(false);
                            btnStethoscopeChest.Background = null;
                            StethoscopeUI.UpdateColor(3);
                            presenter.ModelObject.LogMessage(recvmsg);

                        }
                        else if (recvmsg.Contains("Failed to contact transmitter and exchange session") ||
                        (recvmsg.Contains("Cannot connect to TX failed to connect to get AES key")))
                        {
                            if (!isSthethoscopeChest)
                                StethoscopeUI.UpdateColor(4);
                            else
                            {
                                br = new SolidColorBrush(Colors.Red);
                                btnStethoscopeChest.Background = br;
                            }

                        }

                        else if (recvmsg.Contains("Connected.Remote machine address"))
                        {

                        }
                    }
                }

                ///cabin sw
                if (isCabinSw)
                {
                    string msg = ((TXRXeventsArgs)e).msg;
                    if (msg.Contains("Ready for streaming at :rtsp:") || msg.Contains("Streaming at :rtsp:"))
                    {
                        SolidColorBrush br = new SolidColorBrush
                        {
                            Color = Colors.LightSkyBlue
                        };

                        if (isSthethoscopeChest && presenter.IsConnected)
                        {
                            btnStethoscopeChest.Background = br;
                        }
                        else
                        {
                            if (presenter.IsConnected)
                                btnLungstethoscope.Background = br;
                            StethoscopeUI.UpdateColor(1);
                        }
                    }
                    else
                    {
                        btnStethoscopeChest.Background = null;
                        if (msg.Contains("Streaming Stopped."))
                        {
                            if (!isSthethoscopeChest)
                            {
                                StethoscopeUI.UpdateColor(3);
                                presenter.ModelObject.LogMessage(msg);
                            }
                        }
                    }
                }

                HandleCleaningStatus(((TXRXeventsArgs)e).msg, ((TXRXeventsArgs)e).msg2);

            }));
        }//
         
        void HandleCleaningStatus(string msg,string id)
        {
            SolidColorBrush br = null;
            bool update = false;
            string contetentmsg = string.Empty;
             
            switch ( msg)
            {
                case "Wash":
                    VideoStatus.Text = "Cleaning operation is in progress.";
                    br = new SolidColorBrush
                    {
                        Color = Colors.LightSkyBlue
                    };
                    
                    update = true;
                    contetentmsg = "Abort";
                    break;
                case "Dry":
                    VideoStatus.Text = "Dry operation is in progress.";
                    break;
                case "Abort":
                    VideoStatus.Text = "Cleaning Operation Cancelled."; 
                    br = new SolidColorBrush
                    {
                        Color = Colors.Gray
                    };
                    contetentmsg = string.Empty;
                    update = true;
                    break;
                case "WashDone":
                    VideoStatus.Text = "Cleaning Operation completed.";
                  //  btnController.Content = "Controller";
                    br = new SolidColorBrush
                    {
                        Color = Colors.Gray
                    };
                    // 
                    update = true;
                    contetentmsg = string.Empty;
                    break;
                default:
                    update = false;
                    break;
            }

            if(update)
            {
                switch(Convert.ToInt32(id))
                {
                    case 0:
                        btnController.Background = br;
                        if (string.IsNullOrEmpty(contetentmsg))
                            btnController.Content = "Clean Wash Dry";
                        else
                            btnController.Content = contetentmsg;
                        break;
                    case 1:
                        btnDermascopeCln.Background = br;
                        if (string.IsNullOrEmpty(contetentmsg))
                            btnDermascopeCln.Content = "Dermascope";
                        else
                            btnDermascopeCln.Content = contetentmsg;
                        break;
                    case 2:
                        btnThermometerCln.Background = br;
                        if (string.IsNullOrEmpty(contetentmsg))
                            btnThermometerCln.Content = "Thermometer";
                        else
                            btnThermometerCln.Content = contetentmsg;
                        break;
                    case 3:
                        btnPulseOxymeterCln.Background = br;
                        if (string.IsNullOrEmpty(contetentmsg))
                            btnPulseOxymeterCln.Content = "Pulse Oxymeter"; 
                        else
                            btnPulseOxymeterCln.Content = contetentmsg;
                        break;
                    case 4:
                        btnstethoscopeCln.Background = br;
                        if (string.IsNullOrEmpty(contetentmsg))
                            btnstethoscopeCln.Content = "Stethoscope";
                        else
                            btnstethoscopeCln.Content = contetentmsg;
                        break;

                }
            }
        }
        
        /// <summary>
        /// Page Loading- initialize camera, corecommunication module
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                presenter.InitializeCamera();
                presenter.StartChartDisplay("Initialize");
                if (!isCabinSw)
                {
                    txtTimeduration.Visibility = Visibility.Visible;
                    txtTimeLeft.Visibility = Visibility.Collapsed;
                    btnOpenClose.Visibility = Visibility.Visible;
                    btnOpenClose.Content = "Copy files to EMR";
                    btnplacesmart.Visibility = Visibility.Collapsed;
                    btnOpenSatelite.Visibility = Visibility.Collapsed;
                    btnSendDataNPT.Visibility = Visibility.Collapsed;
                    btnConnect.IsEnabled = false;
                }
                else
                {
                    txtTimeduration.Visibility = Visibility.Collapsed;
                    txtTimeLeft.Visibility = Visibility.Visible;

                    btnOpenClose.Visibility = Visibility.Visible;
                    btnplacesmart.Visibility = Visibility.Visible;
                    btnOpenSatelite.Visibility = Visibility.Visible;
                    btnSendDataNPT.Visibility = Visibility.Visible;
                    txtNoteTextSave.Visibility = Visibility.Collapsed;
                    btnNotesSave.Visibility = Visibility.Collapsed;
                    imagenote.Visibility = Visibility.Collapsed;
                    PatientTabControl.Visibility = Visibility.Collapsed;

                }

                var result = Task.Run(async () => { return await presenter.ReadIPAddressAsync(); }).Result;
                //   result = Task.Run(async () => { return await presenter.InitializeCommunicationModule(); }).Result;
                presenter.InitializeCommunicationModule();
                if (isCabinSw)
                {
                    result = Task.Run(async () => { return await presenter.ReadInstrumentsCOMports(); }).Result;
                }
                presenter.ReadDeviceData(DEVICEDATA.SMARTCARD);
                
            }
            catch (Exception)
            {
            }

            try
            {
                if (isCabinSw)
                {
                    //serialDeviceMgr.Initialize();
                    presenter.ModelObject.ConnectToControlSW();
                    presenter.SendMessage("Core Comm: Cabin Application Connected. Cabin Application-Loaded", false, false, true);
                }
            }
            catch (Exception ex)
            {
                presenter.ModelObject.LogMessage(ex.Message);
            }


        }

        /// <summary>
        /// set the data context
        /// </summary>
        private void SetDataContext()
        {
            SMARTCARDGRID.DataContext = presenter;
            SenserDatagrid.DataContext = presenter.sensordata;
            GridTabPatientInfo.DataContext = presenter.PatientInfo;
            NoteTabGrid.DataContext = presenter.PatientInfo;
            VitalTabGrid.DataContext = presenter.PatientInfo;
            gridNPIData.DataContext = presenter;
            statusGrid.DataContext = presenter;
            MAINMENU.DataContext = presenter.PatientInfo;
        }

        /// <summary>
        /// Close the stethoscope popup
        /// </summary>
        public async void CloseStheoscope()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                if (Stethoscopelungpopup.IsOpen)
                {
                    Stethoscopelungpopup.IsOpen = false;
                    btnLungstethoscope.Background = null;
                }
            }));
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CABINMONITOR_Expanded(object sender, EventArgs e)
        {
            presenter.ReadDeviceData(DEVICEDATA.CABINMONITOR);
        }

        /// <summary>
        /// Handle camera events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void HandleCameraEvents(object sender, EventArgs e)
        {
            string msg = ((VideostreamingMsg)e).msg;
            switch (msg)
            {
                case "CameraInitializationCompleted":
                    break;

                case "ConnectedtoRemote":
                    break;
                case "RemoteVideoFailed":
                    //case "DeviceRemoved":
                    {
                        if (incomingCallTimer != null)
                        {
                            incomingCallTimer.Stop();
                            incomingCallTimer = null;
                        }

                        if (TimeMonitorTimer != null)
                        {
                            TimeMonitorTimer.Stop();
                            TimeMonitorTimer = null;
                        }
                        StartIncomingOREndCallTimer();
                        incomingCallTimer.Interval = new TimeSpan(0, 0, 1);


                        await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                          {
                              if (Stethoscopelungpopup.IsOpen)
                                  StethoscopeUI.StopAutoStethoscope();
                              bincomingCall = false;
                              // EnableConnect();
                              if (isCabinSw)
                              {
                                  btnConnect.IsEnabled = true;
                                  btnConnect.Content = "End Call";
                              }
                              //   incomingCallTimer = new Timer(ResetConnection, null, (int)TimeSpan.FromSeconds(1).TotalMilliseconds, Timeout.Infinite);

                          }));

                        incomingCallTimer.Start();
                    }
                    break;
                case "CaptureFailed":
                    {
                    }
                    break;
                case "INCOMINGCALL":
                    bincomingCall = true;
                    BlinkIncomingCall();
                    break;
                case "REFUSEDCALL":
                    {
                    }
                    break;
                case "IPADDRESSISNOTCONFIGURED":
                    EnableConnect();
                    break;
                case "CABINCALLCONNECTED":
                    CabinCallConnected();
                    break;
                case "DeviceRemoved":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        DeviceRemovedMsg(((VideostreamingMsg)e).MsgEx);
                    }));

                    break;
            }
        }


        void StartIncomingOREndCallTimer()
        {
            if (incomingCallTimer != null && incomingCallTimer.IsEnabled)
            {
                incomingCallTimer.Stop();
                incomingCallTimer = null;
            }

            incomingCallTimer = new DispatcherTimer();
            incomingCallTimer.Tick += ToggleAcceptAndENDcall;

        }

        async void DeviceRemovedMsg(string msg)
        {
            MessageDialog dlg = new MessageDialog(msg, "Device Removed.");
            await dlg.ShowAsync();
        }
        /// <summary>
        /// Accept the call
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BTNConnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AcceptOREndCall();
            }
            catch (Exception)
            { }

        }

        /// <summary>
        /// Enable the Connect button
        /// </summary>
        async void EnableConnect()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                if (isCabinSw)
                {
                    btnConnect.IsEnabled = true;
                    btnConnect.Content = "End Call";
                }
            }));
        }

        /// <summary>
        /// blink the incoming call
        /// </summary>
        async void BlinkIncomingCall()
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                if (!isCabinSw)
                {
                    btnConnect.IsEnabled = true;
                    IncomingCallBlink.Begin();
                    if (incomingCallTimer != null)
                        incomingCallTimer.Stop();

                    if (incomingCallTimer != null && incomingCallTimer.IsEnabled)
                    {
                        incomingCallTimer.Stop();
                        incomingCallTimer = null;
                    }

                    incomingCallTimer = new DispatcherTimer();
                    incomingCallTimer.Tick += IncomingcallTimercallBackFunction;
                    incomingCallTimer.Interval = new TimeSpan(0, 3, 0);
                    incomingCallTimer.Start();
                    //    incomingCallTimer = new Timer(IncomingcallTimercallBackFunction, null, (int)TimeSpan.FromMinutes(2).TotalMilliseconds, Timeout.Infinite);
                }
                else
                {
                    btnConnect.IsEnabled = true;
                    btnConnect.Content = "End Call";
                }
            }));
        }


        /// <summary>
        /// 
        /// </summary>
        void ResetUI()
        {
            try
            {
                if (Stethoscopelungpopup.IsOpen)
                {
                    Stethoscopelungpopup.IsOpen = false;
                }
                btnLungstethoscope.Background = null;
            }
            catch (Exception EX)
            {
                string s = EX.Message;
            }

        }

        private void HandlestethoscopeArrayDuringEndCall(bool endcall = false)
        {

            if (ekgflag)
            {
                presenter.ModelObject.StartEKG(false);
                //  BtnEKGOperation();
                ekgflag = false;
                presenter.ModelObject.StartEKG(false);
                UpdateBtnColor(btnEKG, ekgflag);
            }
            if (btnSpirometerToggle)
            {
                presenter.ModelObject.StartSpirometer(false);
                btnSpirometerToggle = false;
                UpdateBtnColor(btnSpirometer, btnSpirometerToggle);
            }
            if (oxymeter)
            {
                presenter.ModelObject.StartPO(false);
                oxymeter = false;
                UpdateBtnColor(btnPO, btnSpirometerToggle);
            }

            bool stethoscopearray = Stethoscopelungpopup.IsOpen;
            if (!isCabinSw && stethoscopearray)
            {
                StethoscopeUI.ResetStethoscope();
                if (endcall)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreTXex, "11", "0", "17");
                    presenter.SendMessage(msg);
                }

            }
            else if (stethoscopearray && isCabinSw)
            {
                if (endcall)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreTXex, "22", "0", "1");
                    presenter.SendMessage(msg);
                    StethoscopeUI.ResetStethoscope(false);
                }
            }


            if (isSthethoscopeChest)
            {

                if (!isCabinSw)
                {
                    presenter.StartRX(false);
                    if (endcall)
                    {
                        string msg = string.Format(CoreCommunicationTXRX.coreTXex, "22", "0", "3");
                        presenter.SendMessage(msg);
                    }
                }
                else
                {
                    if (endcall)
                    {
                        string msg = string.Format(CoreCommunicationTXRX.coreTXex, "22", "0", "2");
                        presenter.SendMessage(msg);
                    }

                }
            }

            try
            {
                var result = Task.Run(async () => { return await presenter.ReadIPAddressAsync(); }).Result;
                if (isCabinSw)
                    result = Task.Run(async () => { return await presenter.ReadInstrumentsCOMports(); }).Result;

            }
            catch (Exception ex)
            {
                string s = ex.Message;
                presenter.ModelObject.LogMessage(ex.Message);
            }

            try
            {
                presenter.IsConnected = false;
                presenter.InitializeCommunicationModule();
            } catch (Exception ex)
            {
                string s = ex.Message;
                presenter.ModelObject.LogMessage(ex.Message);
            }

            return;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        void ToggleAcceptAndENDcall(object sender = null, object e = null)
        {
            if (incomingCallTimer != null)
            {
                incomingCallTimer.Stop();
                incomingCallTimer = null;
            }

            if (TimeMonitorTimer != null)
            {
                TimeMonitorTimer.Stop();
                TimeMonitorTimer = null;
            }
            try
            {
                HandlestethoscopeArrayDuringEndCall();
            }
            catch (Exception)
            {

            }
            presenter.IsConnected = false;
            btnConnect.IsEnabled = false;
            SolidColorBrush br = new SolidColorBrush
            {
                Color = Colors.LightGray
            };
            btnConnect.Background = br;
            presenter.InitializeCamera();
            if (!isCabinSw)
                btnConnect.Content = "Accept Call";
            else
            {
                btnConnect.Content = "Connect";
                btnConnect.IsEnabled = true;
            }

            ResetUI();

            if (calEndMessageDlg != null)
            {
                calEndMessageDlg.Hide();
                calEndMessageDlg = null;
            }

            if (isCabinSw)
                btnConnect.IsEnabled = true;



            if (stethoscopechestStartStop)
            {
                BtnStethoscopeChestOperation();
            }
            if (btnSHToggle)
            {
                btnSHToggle = false;
                UpdateBtnColor(btnSH, btnSHToggle);
            }


            if (btnGlucoseMonitorToggle)
            {
                btnGlucoseMonitorToggle = false;
                UpdateBtnColor(btnGlucoseMonitor, btnGlucoseMonitorToggle);
            }
            if (btnBPToggle)
            {
                btnBPToggle = false;
                UpdateBtnColor(btnBP, btnBPToggle);
            }

            if (btnSeatedWeightToggle)
            {
                btnSeatedWeightToggle = false;
                UpdateBtnColor(btnSeatedWeight, btnSeatedWeightToggle);
            }

            if (btnSHToggle)
            {
                btnSHToggle = false;
                UpdateBtnColor(btnSH, btnSHToggle);
            }
            if (btnOtoscopeToggle)
            {
                btnOtoscopeToggle = false;
                UpdateBtnColor(btnOtoscope, btnOtoscopeToggle);
            }
            if (btntDarmascopeToggle)
            {
                btntDarmascopeToggle = false;
                UpdateBtnColor(btntDarmascope, btntDarmascopeToggle);
            }
            if (btnthermoThermoToggle)
            {
                btnthermoThermoToggle = false;
                UpdateBtnColor(btnthermo, btnthermoThermoToggle);
            }
            presenter.ClearChart();
            lblVideoStatus.Text = string.Empty;
            presenter.ModelObject.ResetPatientInfo();

            try
            {
                if (isCabinSw)
                {
                    presenter.SendMessage("Core Comm: Cabin Application Connected. Re-connection.", false, false, true);
                }
            }
            catch (Exception ex)
            {
                presenter.ModelObject.LogMessage(ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        void AcceptOREndCall()
        {

            if (incomingCallTimer != null)
            {
                incomingCallTimer.Stop();
                incomingCallTimer = null;
            }

            if (TimeMonitorTimer != null)
            {
                TimeMonitorTimer.Stop();
                TimeMonitorTimer = null;
            }

            if (!isCabinSw && bincomingCall)
            {
                IncomingCallBlink.Stop();
                if (btnConnect.Content.Equals("Accept Call"))
                {
                    presenter.AcceptCall();
                    btnConnect.Content = "End Call";
                    SolidColorBrush br = new SolidColorBrush
                    {
                        Color = Colors.LightSkyBlue
                    };
                    btnConnect.Background = br;
                    bincomingCall = false;
                    try
                    {
                        presenter.PatientInfo.ReadPatientInformation(PatientInformation.PatientInfoTab.INFO);
                        StethoscopeUI.ResetStethoscope();
                        presenter.ReadDeviceData(DEVICEDATA.SMARTCARD);
                    } catch (Exception)
                    { }
                }
            }
            else if (!isCabinSw && btnConnect.Content.Equals("End Call"))
            {
                try 
                {
                    string msg = string.Empty;
                    if (ekgflag)
                        msg = "EKG";
                    else if (btnSpirometerToggle)
                        msg = "Spirometer";
                    else if (oxymeter)
                        msg = "Pulse Oximeter";
                    if (msg.Length > 0)
                    {
                        lblVideoStatus.Text = msg + " is active. can't end call.";
                        return;
                    }
                    ToggleAcceptAndENDcall(null, true);
                }
                catch (Exception)
                {

                }
            }
            bincomingCall = false;
            if (isCabinSw)
            {
                if (btnConnect.Content.Equals("Connect"))
                {
                    try
                    {
                        StethoscopeUI.ResetStethoscope();
                        presenter.StartTX(0, false);
                    }
                    catch (Exception)
                    {

                    }
                    CheckSensorData();
                    if (outserviceFlag)
                        return;

                    SolidColorBrush br = new SolidColorBrush
                    {
                        Color = Colors.LightSkyBlue
                    };

                    presenter.StartCall();
                    btnConnect.IsEnabled = false;
                    btnConnect.Background = br;
                    try
                    {
                        if (isCabinSw)
                        {
                            presenter.SendMessage("Core Comm: Cabin Application Connected-CallConnection.", false, false, true);
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        presenter.ModelObject.LogMessage(ex.Message);
                    }
                }
                else if (btnConnect.Content.Equals("End Call"))
                {
                    try
                    {
                        ToggleAcceptAndENDcall(null, true);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PatientTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!presenter.IsConnected)
                return;

            int selectedIndx = PatientTabControl.SelectedIndex;
            switch (PatientTabControl.SelectedIndex)
            {
                case 0:
                case 1:
                    presenter.PatientInfo.ReadPatientInformation(PatientInformation.PatientInfoTab.INFO);
                    break;

                case 2:
                    presenter.PatientInfo.ReadPatientInformation(PatientInformation.PatientInfoTab.VITAL);
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    break;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void FolderBrowser_Click(object sender, RoutedEventArgs e)
        {
            StorageFolder appFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            await Launcher.LaunchFolderAsync(appFolder);
        }

        /// <summary>
        /// NPI POPUP data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNPI_Click(object sender, RoutedEventArgs e)
        {
            presenter.ReadDeviceData(DEVICEDATA.ATTENDANTNP);
        }

        /// <summary>
        /// Close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnShutdown_Click(object sender, RoutedEventArgs e)
        {
            ContentDialog dlg = new ContentDialog()
            {
                Title = "Application Shutdown",
                Content = "Are you sure you want to close the application?",
                PrimaryButtonText = "Yes",
                SecondaryButtonText = "No",
            };

            var res = await dlg.ShowAsync();
            if (res == ContentDialogResult.Primary)
                Application.Current.Exit();

        }

        /// <summary>
        /// Save Notes to a file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNotesSave_Click(object sender, RoutedEventArgs e)
        {
            presenter.SaveNote(txtNoteTextSave.Text);
        }


        /// <summary>
        /// handle incalling call, if not accepted within 2 mins reject the call.
        /// </summary>
        /// <param name="state"></param>
        private async void IncomingcallTimercallBackFunction(object state, object e)
        {
            if (incomingCallTimer != null)
            {
                incomingCallTimer.Stop();
                incomingCallTimer = null;
            }
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {

                IncomingCallBlink.Stop();
                bincomingCall = false;
                presenter.RefusedCall();
                VideoStatus.Text = "Call Rejected.";
            }));

            StartIncomingOREndCallTimer();
            incomingCallTimer.Interval = new TimeSpan(0, 0, 1);
            incomingCallTimer.Start();
            // incomingCallTimer = new Timer( ResetConnection, null, (int)TimeSpan.FromSeconds(1).TotalMilliseconds, Timeout.Infinite);
        }

        /// <summary>
        /// Call timeout timer
        /// </summary>
        void CabinCallConnected()
        {
            StartTime = DateTime.Now;
            presenter.IsConnected = true;
            if (TimeMonitorTimer != null)
            {
                TimeMonitorTimer.Stop();
                TimeMonitorTimer = null;
            }
            //   TimeMonitorTimer = new Timer(TimeMonitorTimerFunc, null, (int)TimeSpan.FromSeconds(1).TotalMilliseconds, Timeout.Infinite);
            TimeMonitorTimer = new DispatcherTimer();
            TimeMonitorTimer.Tick += TimeMonitorTimerFunc;
            TimeMonitorTimer.Interval = new TimeSpan(0, 0, 0, 0);
            TimeMonitorTimer.Start();
            try
            {
                presenter.PatientInfo.ReadPatientInformation(PatientInformation.PatientInfoTab.INFO);
                presenter.ReadDeviceData(DEVICEDATA.SMARTCARD);
            }
            catch (Exception)
            {

            }
        }

        void TimeMonitorTimerFunc(object state, object e)
        {
            DateTimeOffset currentTime = DateTimeOffset.Now;
            long currentTimeTicks = currentTime.Ticks;
            long starttimeTicks = StartTime.Ticks;
            long calldurationticks = currentTimeTicks - starttimeTicks; 

            int timedoutvalue = presenter.MaxTimedOut;
            int hr = timedoutvalue / 60;
            int min = timedoutvalue % 60;
            TimeSpan duration = new TimeSpan(hr, min,0);
            long maxtimeTicks = duration.Ticks;

            long remainingTime = maxtimeTicks - calldurationticks;
            TimeSpan TotalremainingTime = new TimeSpan(remainingTime);
            TimeSpan TotaldurationTime = new TimeSpan(calldurationticks);

           if (isCabinSw)
            {
                if (TotalremainingTime.TotalMinutes <= 1 && calEndMessageDlg == null && !calExTended)
                {
                    CallEndingWarning();
                }

                if (TotalremainingTime.TotalMinutes <= 0 && !calExTended)
                {
                    if (TimeMonitorTimer != null)
                    {
                        TimeMonitorTimer.Stop();
                        TimeMonitorTimer = null;
                    }
                    ToggleAcceptAndENDcall(null, true);
                    return;
                }

                if (TotalremainingTime.TotalMinutes <= 0 && calExTended)
                {
                    StartTime = DateTimeOffset.Now;
                    calExTended = false;
                }
            }

            if (isCabinSw)
            {
                presenter.sensordata.TimedurationValue = String.Format("{0}:{1}:{2}", FormatTime(TotalremainingTime.Hours), FormatTime(TotalremainingTime.Minutes), FormatTime(TotalremainingTime.Seconds));
            }
            else
            {
               //  presenter.sensordata.TimedurationValue = String.Format("{0}:{1}:{2}", FormatTime(TotalremainingTime.Hours), FormatTime(TotalremainingTime.Minutes), FormatTime(TotalremainingTime.Seconds));
               presenter.sensordata.TimedurationValue = String.Format("{0}:{1}:{2}", FormatTime(TotaldurationTime.Hours), FormatTime(TotaldurationTime.Minutes), FormatTime(TotaldurationTime.Seconds));
            }


        }

        string FormatTime(int time)
        {
            if (time < 10)
                return "0" + time.ToString();
            return time.ToString();
        }
        //   MessageDialog calEndMessageDlg = null;
        ContentDialog calEndMessageDlg = null;
        async void CallEndingWarning()
        {
            string contents = "Your call is going to end in a minute. Would you like to extend the call? Extending the call will result in additional charges.";
            string Title = "Call Time out Warning";

            calEndMessageDlg = new ContentDialog
            {
                Title = Title,
                Content = contents,
                PrimaryButtonText = "YES",
                SecondaryButtonText = "NO",
            };

            var res = await calEndMessageDlg.ShowAsync();

            if (res == ContentDialogResult.Primary)
            {
                calExTended = true;

                calEndMessageDlg = null;
            }
            else if (res == ContentDialogResult.Secondary)
            {

            }
        }

        bool outserviceFlag = false;
        void CheckSensorData()
        {
            bool statusoutofservice = false;
            string Title = "Out of Service";
            string cont = "Cabin out of service.";
            if (isCabinSw)
            {
                if (presenter.InstrumentCleaning)
                {
                    cont = "Cabin ready- after completion of cleaning cycle.";
                    statusoutofservice = true;

                }

                try
                {
                    var result = Task.Run(async () => { return await presenter.ReadIPAddressAsync(); }).Result;
                }
                catch (Exception)
                { }

               
                if (presenter.Motiondetected)
                {
                    statusoutofservice = true;
                }

                if (presenter.Dirtdetected)
                {
                    statusoutofservice = true;
                }

                if (presenter.InstrumentCleaning)
                {
                    cont = "Cabin ready- after completion of cleaning cycle.";
                    statusoutofservice = true;

                }

                if (statusoutofservice)
                {
                    ContentDialog dlg = new ContentDialog
                    {
                        Title = Title,
                        Content = cont,
                        PrimaryButtonText = "Cancel",
                    };

                    var val = dlg.ShowAsync();
                }
                outserviceFlag = statusoutofservice;
            }
        }

        /// <summary>
        /// handle core communication commands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void CoreCommunicationMessage(object sender, EventArgs e)
        {
            string msg = ((ChartDataeventMsg)e).Msg;
            //RX
            if (msg.Contains("rx") && msg.Contains("@"))
            {
                ProcessRXmessage(msg);
                return;
            }
            
            switch (msg)
            {
                case "s81e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnEKGOperation(); //EKG ON OFF
                    }));
                    break;
                case "s80e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnEKGOperation(); //EKG ON OFF
                    }));
                    break;
                case "s41e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        stethoscopechestStartStop = true;
                        //   manualclick = false;
                        BtnStethoscopeChestOperation();
                    }));
                    break;
                case "s40e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        stethoscopechestStartStop = false;
                        //manualclick = false;
                        BtnStethoscopeChestOperation();
                    }));
                    break;
                case "s31e": //PULSEOXYMETER
                case "s30e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnPOOperation();
                    }));
                    break;
                case "s51e":
                case "s50e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnSHoperation();
                    }));
                    break;
                case "s61e":
                case "s60e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnSeatedWeightOperation();
                    }));
                    break;
                case "s71e":
                case "s70e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnOtoscopeOperation();
                    }));
                    break;
                case "s91e":
                case "s90e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnSpirometerOperation();
                    }));
                    break;
                case "s141e":
                case "s140e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnBPOperation();
                    }));
                    break;
                case "s21e":
                case "s20e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        ThermoMeterOperation();
                    }));
                    break;
                case "s11e":
                case "s10e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtntDarmascopeOperation();
                    }));
                    break;
                case "s101e":
                case "s100e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnGlucoseMonitorOperation();
                    }));
                    break;
                case "s11117e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        BtnLungsstethoscopeOperation();
                    }));
                    break;
                case "s2201e":
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                      {
                          StethoscopeUI.ResetStethoscope();
                      }));

                    break;
                case "s2202e":
                    isSthethoscopeChest = false;
                    stethoscopechestStartStop = false;
                    presenter.StartRX(false);
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        UpdateBtnColor(btnStethoscopeChest, false);
                    }));
                    break;
                case "s2203e":
                    isSthethoscopeChest = false;
                    stethoscopechestStartStop = false;
                    presenter.StartTX(0, false);
                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        UpdateBtnColor(btnStethoscopeChest, false);
                    }));
                    break;
                case "s1Ee":
                    BtnEmergencyOperation();
                    break;
                case "saCHe":
                    BtnCHOperation("a", btnCHAuto);
                    break;
                case "sbCHe":
                    BtnCHOperation("b", btnCHbottom);
                    break;
                case "scCHe":
                    BtnCHOperation("c", btnCHCenter);
                    break;
                case "stCHe":
                    BtnCHOperation("t", btnCHTop);
                    break;
                case "sdCHe":
                    BtnCHOperation("d", btnCHDown);
                    break;
                case "suCHe":
                    BtnCHOperation("u", btnCHUP);
                    break;
               
                default:
                   // DisplayMessage?.Invoke(this, new TXRXeventsArgs(msg));
                    HandleCleaningSystem(msg);
                    HandleChairBackMovement(msg);
                    break;
            }

            if (msg.Contains("s11") && isCabinSw) //id 11 stethoscopelungs
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                {

                    StethoscopeUI.StethoscopeCommand(msg);
                }));
            }

              if(msg.Contains("SERIALDEVICE-REMOVED"))
                    HandleSerialDeviceStatus(e);
             

        }
         
        void HandleCleaningSystem(string msg)
        {
            switch(msg)
            {
                case "S0E":
                    if (cleaningstate[0] == 1)
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "0d", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                         
                        BtnControllerOperation(0);//dry operation after wash
                        cleaningstate[0] = 0;
                    }
                    break;
                case "SOEC":
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "0c", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        cleaningstate[0] = 3;
                        BtnControllerOperation(0); //wash operation done
                        cleaningstate[0] = 0;
                    }
                    break;
                case "S1E":
                    if (cleaningstate[1] == 1)
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "1d", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        BtnControllerOperation(1);//dry operation after wash
                        cleaningstate[1] = 0;
                    }
                    else  
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "1c", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        cleaningstate[1] = 3;
                        BtnControllerOperation(1); //wash operation done
                        cleaningstate[1] = 0;
                    }
                    break;
                case "S2E":
                    if (cleaningstate[2] == 1)
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "2d", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        BtnControllerOperation(2);//dry operation after wash
                        cleaningstate[2] = 0;
                    }
                    else 
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "2c", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        cleaningstate[2] = 3;
                        BtnControllerOperation(2); //wash operation done
                        cleaningstate[2] = 0;
                    }
                    break;
                case "S3E":
                    if (cleaningstate[3] == 1)
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "3d", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        BtnControllerOperation(3);//dry operation after wash
                        cleaningstate[3] = 0;
                    }
                    else 
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "3c", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        cleaningstate[3] = 3;
                        BtnControllerOperation(3); //wash operation done
                        cleaningstate[3] = 0;
                    }
                    break;
                case "S4E":
                    if (cleaningstate[4] == 1)
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "4d", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        BtnControllerOperation(4);//dry operation after wash
                        cleaningstate[4] = 0;
                    }
                    else 
                    {
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, "4c", "CL");
                        presenter.SendMessage(cmd, false, false, true);
                        cleaningstate[4] = 3;
                        BtnControllerOperation(4); //wash operation done
                        cleaningstate[4] = 0;
                    }
                    break;
                case "S5E":
                    
                    break;
                case "s0wCLe": 
                    cleaningstate[0] = 0;
                    BtnControllerOperation(0);
                    cleaningstate[0] = 1;
                    break;
                case "s0dCLe": 
                    cleaningstate[0] = 1;
                    BtnControllerOperation(0);
                    cleaningstate[0] = 0;
                    break;
                case "s0aCLe":
                    cleaningstate[0] = 2;
                    BtnControllerOperation(0);
                    cleaningstate[0] = 0;
                    break;
                case "s1wCLe":
                    cleaningstate[1] = 0;
                    BtnControllerOperation(1);
                    cleaningstate[1] = 1;
                    break;
                case "s1dCLe":
                    cleaningstate[1] = 1;
                    BtnControllerOperation(1);
                    cleaningstate[1] = 0;
                    break;
                case "s1aCLe":
                    cleaningstate[1] = 2;
                    BtnControllerOperation(1);
                    cleaningstate[1] = 0;
                    break;
                case "s2wCLe":
                    cleaningstate[2] = 0;
                    BtnControllerOperation(2);
                    cleaningstate[2] = 1;
                    break;
                case "s2dCLe": 
                    cleaningstate[2] = 1;
                    BtnControllerOperation(2);
                    cleaningstate[2] = 0;
                    break;
                case "s2aCLe": ;
                    cleaningstate[2] = 2;
                    BtnControllerOperation(2);
                    cleaningstate[2] = 0;
                    break;
                case "s3wCLe":
                    cleaningstate[3] = 0;
                    BtnControllerOperation(3);
                    cleaningstate[3] = 1;
                    break;
                case "s3dCLe":
                    cleaningstate[3] = 1;
                    BtnControllerOperation(3);
                    cleaningstate[3] = 0;
                    break;
                case "s3aCLe":

                    cleaningstate[3] = 2;
                    BtnControllerOperation(3);
                    cleaningstate[3] = 0;
                    break;
                case "s4wCLe":
                    cleaningstate[4] = 0;
                    BtnControllerOperation(4);
                    cleaningstate[4] = 1;
                    break;
                case "s4dCLe":
                    cleaningstate[4] = 1;
                    BtnControllerOperation(4);
                    cleaningstate[4] = 0;
                    break;
                case "s4aCLe":
                    cleaningstate[4] = 2;
                    BtnControllerOperation(4);
                    cleaningstate[2] = 0;
                    break;
                case "s0cCLe":
                    cleaningstate[0] = 3;
                    BtnControllerOperation(0);
                    cleaningstate[0] = 0;
                    break;
                case "s1cCLe":
                    cleaningstate[1] = 3;
                    BtnControllerOperation(1);
                    cleaningstate[1] = 0;
                    break;
                case "s2cCLe":
                    cleaningstate[2] = 3;
                    BtnControllerOperation(2);
                    cleaningstate[2] = 0;
                    break;
                case "s3cCLe":
                    cleaningstate[3] = 3;
                    BtnControllerOperation(3);
                    cleaningstate[3] = 0;
                    break;
                case "s4cCLe":
                    cleaningstate[4] = 3;
                    BtnControllerOperation(4);
                    cleaningstate[4] = 0;
                    break;
            }
        }

      async  void HandleSerialDeviceStatus(EventArgs e )
        {
            string msg = ((ChartDataeventMsg)e).Msg;
            string msg2 = ((ChartDataeventMsg)e).Msg2;
            switch (msg)
            {
                case "SERIALDEVICE-REMOVED":
                    if(isCabinSw)
                    {
                        string npmessage = "SERIALDEVICE-REMOVED#" + msg2;
                        presenter.SendMessage(npmessage);
                        await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                        {
                            DisplaypoupMessage(msg2, "Serial device removed");
                        }));
                    }
                    break;
            };

            if (msg.Contains("SERIALDEVICE-REMOVED#"))
            {
                string prefix = "SERIALDEVICE-REMOVED#";
                msg2 = msg.Substring(prefix.Length);

                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                {
                    DisplaypoupMessage(msg2, "Serial device removed");
                }));
            }
               
             
        }

       async void DisplaypoupMessage(string Headings, string msg)
        {
       
            MessageDialog dlg = new MessageDialog(  Headings, msg);
            await dlg.ShowAsync();
        }

        void ProcessRXmessage(string msg)
        {
            //RX
            if (msg.Contains("rx") && msg.Contains("@"))
            {
                string[] data = msg.Split('@');
                switch (data[0])
                {
                    case "rx8":
                    case "rx9":
                    case "rx3":
                        DisplayGraph(data[1]);
                        break;
                }
            }

        }

        async void DisplayGraph(string value)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                presenter.StartChartDisplay(value);
            }));
        }

        //standing height
        private void BtnSH_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;

            BtnSHoperation();

        }

        /// <summary>
        /// handle btntanding height operation
        /// </summary>
        bool btnSHToggle = false;
        void BtnSHoperation()
        {
            btnSHToggle = !btnSHToggle;
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "5", "1");
                if (!btnSHToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "5", "0");

                presenter.SendMessage(msg);
            }
            else
            {

                string syscmd = "";
                if (btnSHToggle)
                    syscmd = string.Format(Serialdevicestatusformat, ((int)(TESTRIGOPERATIONS.DEP_SH_HW)),1);
                else
                    syscmd = string.Format(Serialdevicestatusformat, ((int)TESTRIGOPERATIONS.RET_SH_HW), 0);
                presenter.SendControlCMD(syscmd);


            }
            UpdateBtnColor(btnSH, btnSHToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.HEIGHT, btnSHToggle);
        }

        /// <summary>
        /// Seating weight
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSeatedWeight_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnSeatedWeightOperation();
        }

        /// <summary>
        /// Handle btnSeatedWeight Operation
        /// </summary>
        bool btnSeatedWeightToggle = false;
        void BtnSeatedWeightOperation()
        {
            btnSeatedWeightToggle = !btnSeatedWeightToggle;
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "6", "1");
                if (!btnSeatedWeightToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "6", "0");
                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btnSeatedWeightToggle)
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.DEP_SEAT_HW, 1);
                else
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.RET_SEAT_HW, 0);
                presenter.SendControlCMD(syscmd);
            }

            UpdateBtnColor(btnSeatedWeight, btnSeatedWeightToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.WEIGHT, btnSeatedWeightToggle);
        }

        //BP DATA
        bool btnBPToggle = false;
        private void BtnBP_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnBPOperation();
        }

        //BPDATA
        void BtnBPOperation()
        {
            btnBPToggle = !btnBPToggle;
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "14", "1");
                if (!btnBPToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "14", "0");
                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btnBPToggle)
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_BP, 1); 
                else
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.RET_BP, 0);

                presenter.SendControlCMD(syscmd);

            }
            UpdateBtnColor(btnBP, btnBPToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.BP, btnBPToggle);
        }

        /// <summary>
        /// Thermometer data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        bool btnthermoThermoToggle = false;
        private void Btnthermo_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            ThermoMeterOperation();
        }
        /// <summary>
        /// Handle Thermometer operation
        /// </summary>
        void ThermoMeterOperation()
        {
            btnthermoThermoToggle = !btnthermoThermoToggle;
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "2", "1");
                if (!btnthermoThermoToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "2", "0");
                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btnthermoThermoToggle)
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_THERMOMETER, 1);
                else
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.RET_THERMOMETER, 1);
                presenter.SendControlCMD(syscmd);
                // serialDeviceMgr.SystemCommand(CoreCommunicationTXRX.TAT_5000Thermometer, syscmd);
            }
            UpdateBtnColor(btnthermo, btnthermoThermoToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.TERMO, btnthermoThermoToggle);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool btntDarmascopeToggle = false;
        private void BtntDarmascope_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtntDarmascopeOperation();
        }

        /// <summary>
        /// Handle Thermometer Operation
        /// </summary>
        void BtntDarmascopeOperation()
        {
            if (btnOtoscopeToggle) //otoscope in progress can't proceed
                return;

            btntDarmascopeToggle = !btntDarmascopeToggle;
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "1", "1");
                if (!btntDarmascopeToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "1", "0");
                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btntDarmascopeToggle)
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_DERMASCOPE, 1);
                else
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.RET_DERMASCOPE, 0);

                presenter.SendControlCMD(syscmd);
            }
            UpdateBtnColor(btntDarmascope, btntDarmascopeToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.DERMASCOPE, btntDarmascopeToggle);
        }

        /// <summary>
        /// Otoscope operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool btnOtoscopeToggle = false;
        private void BtnOtoscope_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnOtoscopeOperation();
        }

        /// <summary>
        /// btn Otoscope Operation
        /// </summary>
        void BtnOtoscopeOperation()
        {
            if (btntDarmascopeToggle) //if dermascope in use can't proceed
                return;

            btnOtoscopeToggle = !btnOtoscopeToggle;
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "7", "1");
                if (!btnOtoscopeToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "7", "0");

                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btnOtoscopeToggle)
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_OTOSCOPE, 1);
                else
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.RET_OTOSCOPE, 0);

                presenter.SendControlCMD(syscmd);
            }
            UpdateBtnColor(btnOtoscope, btnOtoscopeToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.OTOSCOPE, btnOtoscopeToggle);
        }

        bool btnSpirometerToggle = false;
        private void BtnSpirometer_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;

            BtnSpirometerOperation();
        }

        async void BtnSpirometerOperation()
        {
            btnSpirometerToggle = !btnSpirometerToggle;
            presenter.ModelObject.ClearChart();
            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, (int)InstrumentsID.SPIROMETER, "1");
                if (!btnSpirometerToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, (int)InstrumentsID.SPIROMETER, "0");
                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btnSpirometerToggle)
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_SPIROMETER, 1);
                else
                    syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.RET_SPIROMETER, 0);

                presenter.SendControlCMD(syscmd);

                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                {

                    presenter.ModelObject.StartSpirometer(btnSpirometerToggle);
                }));
            }

            UpdateBtnColor(btnSpirometer, btnSpirometerToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.SPIROMETER, btnSpirometerToggle);
        }

        bool btnGlucoseMonitorToggle = false;
        private void BtnGlucoseMonitor_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnGlucoseMonitorOperation();
        }

        void BtnGlucoseMonitorOperation()
        {
            btnGlucoseMonitorToggle = !btnGlucoseMonitorToggle;

            if (!isCabinSw)
            {
                string msg = string.Format(CoreCommunicationTXRX.coreTX, "10", "1");
                if (!btnGlucoseMonitorToggle)
                    msg = string.Format(CoreCommunicationTXRX.coreTX, "10", "0");
                presenter.SendMessage(msg);
            }
            else
            {
                string syscmd = "";
                if (btnGlucoseMonitorToggle)
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.DEP_GLUCOMETER, 1);
                else
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.RET_GLUCOMETER, 0);

                presenter.SendControlCMD(syscmd);

            }

            UpdateBtnColor(btnGlucoseMonitor, btnGlucoseMonitorToggle);
            presenter.ModelObject.ReadPatientInstrumentData(PatientInformation.PatientInfoTab.GLOCOSE, btnGlucoseMonitorToggle);
        }


        /// <summary>
        /// stethoscope chest clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool stethoscopechestStartStop = false;

        private void BtnStethoscopeChest_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnStethoscopeChestOperation();
        }

        void BtnStethoscopeChestOperation()
        {
            try
            {
                SolidColorBrush br = new SolidColorBrush
                {
                    Color = Colors.LightSkyBlue
                };

                if (!isCabinSw)
                {
                    stethoscopechestStartStop = !stethoscopechestStartStop;
                    if (stethoscopechestStartStop)
                    {
                        string msg = string.Format(CoreCommunicationTXRX.coreTX, "4", "1");
                        presenter.SendMessage(msg);
                        presenter.StartRX(true);
                        isSthethoscopeChest = true;
                    }
                    else
                    {
                        presenter.StartRX(false);

                        string msg = string.Format(CoreCommunicationTXRX.coreTX, "4", "0");
                        presenter.SendMessage(msg, false, true);
                        isSthethoscopeChest = false;
                    }

                }
                else
                {
                    //Select the stethoscope
                    string syscmd = "";
                    if (stethoscopechestStartStop)
                        syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.SELECTMICROFONE, 1);
                    else
                        syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.SELECTMICROFONE, 0);

                    presenter.SendControlCMD(syscmd);

                    isSthethoscopeChest = stethoscopechestStartStop;
                    presenter.StartTX(1, stethoscopechestStartStop);
                }

                UpdateBtnColor(btnStethoscopeChest, stethoscopechestStartStop);
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// stethoscope lungs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLungstethoscope_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            //If stethoscope chest is streaming lungs can't start
            if (isSthethoscopeChest && !isCabinSw)
            {
                return;
            }
            BtnLungsstethoscopeOperation();
        }

        void BtnLungsstethoscopeOperation()
        {
            try
            {
                isSthethoscopeChest = false;
                SolidColorBrush br = new SolidColorBrush
                {
                    Color = Colors.LightSkyBlue
                };

                btnLungstethoscope.Background = br;
                double wdth = MAINMENU.ActualWidth;

                double ht = MAINMENU.ActualHeight + rootGrid.RowDefinitions[1].ActualHeight;
                if (isCabinSw)
                {
                    wdth = rightPaneGrid.ColumnDefinitions[0].ActualWidth;
                    ht = rightPaneGrid.RowDefinitions[0].ActualHeight;
                    Stethoscopelungs.StHeight = (ht / 4) - 40;
                }
                else
                    Stethoscopelungs.StHeight = (ht / 4) - 10;

                StethoscopeUI.Height = ht;  //btnAlarm.ActualWidth * 2+10;
                StethoscopeUI.Width = wdth;

                StethoscopeUI.PresenterObject = presenter;
                StethoscopeUI.RootWindowHandle = this;
                Stethoscopelungpopup.IsOpen = true;
                if (!isCabinSw)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreTXex, "11", "1", "17");
                    presenter.SendMessage(msg);
                }
                else
                {

                }
            } catch (Exception)
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnEKG_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;

            BtnEKGOperation();
        }

        /// <summary>
        /// EKG
        /// </summary>
        async void BtnEKGOperation()
        {
            try
            {
                ekgflag = !ekgflag;
                presenter.ModelObject.ClearChart();
                //    
                SolidColorBrush br = new SolidColorBrush();
                if (ekgflag)
                {
                    br.Color = Colors.LightSkyBlue;
                }
                else
                    br = null;
                btnEKG.Background = br;
                if (!isCabinSw)
                {
                    string msg = string.Format(CoreCommunicationTXRX.coreTX, "8", "1");
                    presenter.SendMessage(msg);
                }
                else
                {
                    string syscmd = "";
                    if (ekgflag)
                        syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_EKG, 1);
                    else
                        syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.RET_EKG, 0);

                    presenter.SendControlCMD(syscmd);


                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        presenter.StartEKG(ekgflag);
                    }));
                }
            } catch (Exception)
            {

            }
        }

        /// <summary>
        /// Update Button Color
        /// </summary>
        /// <param name="btn"></param>
        /// <param name="toggle"></param>
        void UpdateBtnColor(Button btn, bool toggle)
        {
            SolidColorBrush br = new SolidColorBrush();
            if (toggle)
            {
                br.Color = Colors.LightSkyBlue;
            }
            else
                br = null;
            btn.Background = br;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPO_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnPOOperation();
        }

        async void BtnPOOperation()
        {
            try
            {
                presenter.ModelObject.ClearChart();
                oxymeter = !oxymeter;
                SolidColorBrush br = new SolidColorBrush();
                if (oxymeter)
                {
                    br.Color = Colors.LightSkyBlue;
                }
                else
                    br = null;
                btnPO.Background = br;

                if (!isCabinSw)
                {

                    string msg = string.Format(CoreCommunicationTXRX.coreTX, "3", "1");
                    presenter.SendMessage(msg);

                }
                else
                {
                   

                    string syscmd = "";
                    if (oxymeter)
                        syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.DEP_PO, 1);
                    else
                        syscmd = string.Format(Serialdevicestatusformat,(int) TESTRIGOPERATIONS.RET_PO, 0);

                    presenter.SendControlCMD(syscmd);

                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
                    {
                        presenter.StartPO(oxymeter);
                    }));


                }
            }
            catch (Exception)
            {

            }
        }

        private async void BtnSendDataNPT_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog dlg = new MessageDialog("Smart card data has been transmitted.", "File Transfer");
            await dlg.ShowAsync();
        }

        /// <summary>
        /// SMART CARD button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSmartCard_Checked(object sender, RoutedEventArgs e)
        {

            if (!popupSmartCard.IsOpen && presenter.IsConnected)
            {
                presenter.ReadDeviceData(DEVICEDATA.SMARTCARD);
                popupSmartCard.IsOpen = true;
            }
            else
            {
                popupSmartCard.IsOpen = false;
            }
        }

        public void Dispose()
        {
            if (incomingCallTimer != null)
            {
                incomingCallTimer.Stop();
                incomingCallTimer = null;
            }


            if (TimeMonitorTimer != null)
            {
                TimeMonitorTimer.Stop();
                TimeMonitorTimer = null;
            }
        }

        bool _intractivemode = true;
        bool IsIntractiveMode
        {
            get
            {
                if (!isCabinSw && !presenter.IsConnected)
                return false;
                 return _intractivemode;
                
               
            }
        }

        bool isSthethoscopeChest = false;
        DispatcherTimer incomingCallTimer; //incomming call timer.
        //    Timer TimeMonitorTimer;
        DispatcherTimer TimeMonitorTimer; //call duration timer
        DateTimeOffset StartTime;
        bool calExTended = false;
        bool oxymeter = false;
        bool ekgflag = false;
        bool BtnOpenCloseflag = false;
        private void BtnOpenClose_Click(object sender, RoutedEventArgs e)
        {
            UpdatebtnStatusColor(btnOpenClose, true);
            string syscmd = "";
            BtnOpenCloseflag = !BtnOpenCloseflag;
            if (BtnOpenCloseflag)
                syscmd = "$R1";
            else
                syscmd = "$R2";

            if (BtnOpenCloseflag)
                syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.OPENDOOR, 1);
            else
                syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.CLOSEDOOR, 0);

            presenter.SendControlCMD(syscmd); 
            UpdatebtnStatusColor(btnOpenClose, false);
        }


        private void BtnEmergency_Click(object sender, RoutedEventArgs e)
        {
            BtnEmergencyOperation();
        }

        private void BtnEmergencyOperation()
        {

            //UpdatebtnStatusColor(btnEmergency, true);
            if (!isCabinSw)
            {
                String cmd = string.Format(CoreCommunicationTXRX.coreTX, "1", "E");
                presenter.SendMessage(cmd);
            }
            else
            {
                string syscmd = "$R3";
               // if (BtnOpenCloseflag)
                    syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.OPENDOOR, 1);
               // else
                   // syscmd = string.Format(Serialdevicestatusformat, TESTRIGOPERATIONS.CLOSEDOOR, 0);

                presenter.SendControlCMD(syscmd); ;

            }

            // UpdatebtnStatusColor(btnEmergency, false);
        }

       
        private void BtnCHAuto_Click(object sender, RoutedEventArgs e)
        {
            if (!IsIntractiveMode)
                return;
            BtnCHOperation( "a", btnCHAuto);
           

        }
        
        private void BtnCHUP_Click(object sender, RoutedEventArgs e)
        {
            if (chairmovementinprogress)
                return;
            BtnCHOperation(  "u", btnCHUP);
           
        }

        private void BtnCHDown_Click(object sender, RoutedEventArgs e)
        {
            if (chairmovementinprogress)
                return;
            BtnCHOperation("d", btnCHDown); 
        }

        
        private void BtnCHTop_Click(object sender, RoutedEventArgs e)
        {
            if (chairmovementinprogress)
                return;
            BtnCHOperation("t", btnCHTop); 
        }

        private void BtnCHCenter_Click(object sender, RoutedEventArgs e)
        {
            if (chairmovementinprogress )
                return;
            BtnCHOperation( "c", btnCHCenter); 
        }

        private void BtnCHbottom_Click(object sender, RoutedEventArgs e)
        {
            if (chairmovementinprogress)
                return;
            BtnCHOperation("b", btnCHbottom); 
        }

        void BtnCHOperation(string cmdstr, Button btn = null)
        {
            if (chairmovementinprogress)
                return;

            if (isCabinSw)
            {
                 UpdatebtnStatusColor(btn, true);
                chairmovementinprogress = false;
            }
            else
            {
                chairmovementinprogress = false;
                UpdatebtnStatusColor(btn, false);
            }

            chairmovementpos = cmdstr;
            if (!isCabinSw)
            {
                string cmd = string.Format(CoreCommunicationTXRX.coreTX, cmdstr, "CH");
                presenter.SendMessage(cmd);
            }
            else
            {
                int indx = 0;
                switch (cmdstr)
                {
                    case "a":
                        
                        indx = 0;
                        break;
                    case "u":
                        indx = 1;
                        break;
                    case "d":
                        indx = 2;
                        break;
                    case "t":
                        indx = 3;
                        break;
                    case "c":
                        indx = 4;
                        break;
                    case "b":
                        indx = 5;
                        break;
                }

                string  syscmd = string.Format(Serialdevicestatusformat, (int)TESTRIGOPERATIONS.SELECT_CHAIRPOSITION, indx.ToString());

                presenter.SendControlCMD(syscmd);

                string cmd = string.Format(CoreCommunicationTXRX.coreTX, "m"+ cmdstr, "CH");
                presenter.SendMessage(cmd);
            }


        }

       
        string chairmovementpos = string.Empty;
        bool chairmovementinprogress = false;
        void HandleChairBackMovement(string str)
        {
            bool status = false;
            bool update = false;
            if(str.Equals("GB"))
            {
                if (isCabinSw)
                {
                    string cmd = string.Format(chairmovementpos+"GB\r\n");
                    presenter.SendMessage(cmd);
                }
                chairmovementinprogress = false;
                status = false;
                update = true;
            }
            else //if(str.Equals("smCHe"))
            {
                switch(str)
                {
                    case "smaCHe":
                        status = true;
                        update = true;
                        chairmovementpos = "a";
                        break;
                    case "smbCHe":
                        status = true;
                        update = true;
                        chairmovementpos = "b";
                        break;
                    case "smcCHe":
                        status = true;
                        update = true;
                        chairmovementpos = "c";
                        break;
                    case "smdCHe":
                        status = true;
                        update = true;
                        chairmovementpos = "d";
                        break;
                    case "smuCHe":
                        status = true;
                        update = true;
                        chairmovementpos = "u";
                        break;
                    case "smtCHe":
                        status = true;
                        update = true;
                        chairmovementpos = "t";
                        break;
                    case "aGB":
                        status = false;
                        update = true;
                        chairmovementpos = "a";
                        break;
                    case "bGB":
                        status = false;
                        update = true;
                        chairmovementpos = "b";
                        break;
                    case "cGB":
                        status = false;
                        update = true;
                        chairmovementpos = "c";
                        break;
                    case "dGB":
                        status = false;
                        update = true;
                        chairmovementpos = "d";
                        break;
                    case "uGB":
                        status = false;
                        update = true;
                        chairmovementpos = "u";
                        break;
                    case "tGB":
                        status = false;
                        update = true;
                        chairmovementpos = "t";
                        break;
                }
               

            }

            if (update)
            {
                switch (chairmovementpos)
                {
                    case "a":
                        UpdatebtnStatusColor(btnCHAuto, status);
                        break;
                    case "b":
                        UpdatebtnStatusColor(btnCHbottom, status);
                        break;
                    case "c":
                        UpdatebtnStatusColor(btnCHCenter, status);
                        break;
                    case "d":
                        UpdatebtnStatusColor(btnCHDown, status);
                        break;
                    case "t":
                        UpdatebtnStatusColor(btnCHTop, status);
                        break;
                    case "u":
                        UpdatebtnStatusColor(btnCHUP, status);
                        break;
                }
               
            }
        }
        
        
        async void UpdatebtnStatusColor(Button btn, bool toggle)
        {
            if (btn == null)
                return;
            SolidColorBrush br = new SolidColorBrush
            {
                Color = Colors.LightSkyBlue
            };

            if (!toggle)
            {
                br.Color = Colors.LightGray;
            }
            
                
                  
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, (() =>
            {
                btn.Background = br;
            }));
        }


        private void BtnDermascopeCln_Click(object sender, RoutedEventArgs e)
        {
            if ( presenter.IsConnected  )
                return;

            if (cleaningstate.ContainsKey(1) && cleaningstate[1] > 0)
                cleaningstate[1] = 2;
            BtnControllerOperation(1,true);
           
        }

        private void BtnThermometerCln_Click(object sender, RoutedEventArgs e)
        {
            if (presenter.IsConnected)
                return;
            if (cleaningstate.ContainsKey(2) && cleaningstate[2] > 0)
                cleaningstate[2] = 2;
            BtnControllerOperation(2,true);
            
        }

        private void BtnPulseOxymeterCln_Click(object sender, RoutedEventArgs e)
        {
            if (presenter.IsConnected )
                return;
            if (cleaningstate.ContainsKey(3) && cleaningstate[3] > 0)
                cleaningstate[3] = 2;
            BtnControllerOperation(3,true);
           

        }
        private void BtnstethoscopeCln_Click(object sender, RoutedEventArgs e)
        {
            if (presenter.IsConnected )
                return;
            if (cleaningstate.ContainsKey(4) && cleaningstate[4] > 0)
                cleaningstate[4] = 2; //abort
             
            BtnControllerOperation(4,true);

           
        }


       // int cleaningState = 0;
       // int insrumentID_Cleaning = 0;
        Dictionary<int, int> cleaningstate = new Dictionary<int, int>();
        private void BtnController_Click(object sender, RoutedEventArgs e)
        {
            if (presenter.IsConnected) //NO CLEANING IF CONNECTED
                return;
            if (cleaningstate.ContainsKey(0) && cleaningstate[0] > 0)
                cleaningstate[0] = 2;
            BtnControllerOperation(0,true);
        }

       
        void BtnControllerOperation(int insrumentID_Cleaning,bool notify=false)
        {
            if (!cleaningstate.ContainsKey(insrumentID_Cleaning))
                 cleaningstate.Add(insrumentID_Cleaning, 0);
            

            if (!isCabinSw)
            {
                switch (cleaningstate[insrumentID_Cleaning])
                {
                    case 0:
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString() + "w", "CL");
                        if (notify)
                        {
                           presenter.SendMessage(cmd, false, false, true);
                        }

                        cleaningstate[insrumentID_Cleaning] = 1;
                        if (cleaningstate.ContainsKey(insrumentID_Cleaning))
                            cleaningstate[insrumentID_Cleaning] = 2;

                        presenter.NotifyError(this, new TXRXeventsArgs("Wash", insrumentID_Cleaning.ToString()));
                       
                        break;
                    case 1:
                        presenter.NotifyError(this, new TXRXeventsArgs("Dry", insrumentID_Cleaning.ToString()));
                        cleaningstate[insrumentID_Cleaning] = 2;
                        break;
                    case 2:
                        cmd = string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString()+"a", "CL");
                        if(notify)
                        presenter.SendMessage(cmd, false, false, true);
                        // cleaningstate[insrumentID_Cleaning] = 0;
                        if (cleaningstate.ContainsKey(insrumentID_Cleaning))
                            cleaningstate[insrumentID_Cleaning] = 0;

                        presenter.NotifyError(this, new TXRXeventsArgs("Abort", insrumentID_Cleaning.ToString()));
                       
                        break;
                    case 3:
                        if (cleaningstate.ContainsKey(insrumentID_Cleaning))
                            cleaningstate[insrumentID_Cleaning] = 0;
                        presenter.NotifyError(this, new TXRXeventsArgs("WashDone", insrumentID_Cleaning.ToString()));

                        break;
                }
            }
            else
            { 
                switch (cleaningstate[insrumentID_Cleaning])
                {
                    case 0:
                      //  serialDeviceMgr.SystemCommand(CoreCommunicationTXRX.Smart_Card_Door_opener, string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString(),"w"));
                        presenter.NotifyError(this, new TXRXeventsArgs("Wash", insrumentID_Cleaning.ToString()));
                        cleaningstate[insrumentID_Cleaning] = 1;
                       // SimulatedTimerFunction(insrumentID_Cleaning);
                        presenter.InstrumentCleaning = true;
                       
                        String cmd = string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString() + "w", "CL");
                        if (notify)
                            presenter.SendMessage(cmd, false, false, true);
                        cmd = string.Format(TestrigCMDformat, ((int)TESTRIGOPERATIONS.CLEAN_WASH_DRY).ToString(),"1");

                        presenter.SendControlCMD(cmd);
                        break;
                    case 1:
                       // serialDeviceMgr.SystemCommand(CoreCommunicationTXRX.Smart_Card_Door_opener, string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString(), "d"));
                        presenter.NotifyError(this, new TXRXeventsArgs("Dry", insrumentID_Cleaning.ToString()));
                        break;

                    case 2:
                        presenter.InstrumentCleaning = false;
                       // serialDeviceMgr.SystemCommand(CoreCommunicationTXRX.Smart_Card_Door_opener, string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString(), "a"));
                        presenter.NotifyError(this, new TXRXeventsArgs("Abort", insrumentID_Cleaning.ToString()));
                   //     SimulatedTimerFunction(insrumentID_Cleaning, true);

                        if (cleaningstate.ContainsKey(insrumentID_Cleaning))
                            cleaningstate[insrumentID_Cleaning] = 0;

                         cmd = string.Format(CoreCommunicationTXRX.coreTX, insrumentID_Cleaning.ToString() + "a", "CL");

                        if (notify)
                        {
                            presenter.SendMessage(cmd, false, false, true);
                            cmd = string.Format(TestrigCMDformat, ((int)TESTRIGOPERATIONS.CLEAN_WASH_DRY).ToString(), "0");
                             presenter.SendControlCMD(cmd);

                        }


                        break;

                    case 3:
                        presenter.InstrumentCleaning = false;
                        if (cleaningstate.ContainsKey(insrumentID_Cleaning))
                            cleaningstate[insrumentID_Cleaning] = 0;

                        presenter.NotifyError(this, new TXRXeventsArgs("WashDone", insrumentID_Cleaning.ToString()));
                        break;
                }

            }
        }

        
        
      
        

        private void BtnTestRigFrame_Click(object sender, RoutedEventArgs e)
        {
            //TestrigframeUICtrl.upd
            //  presenter.ModelObject.ConnectToControlSW();
            presenter.ModelObject.SendControlCMD("CabinSWApp: CabinSWApp  Connected.");

            if (!TestRigFramepopup.IsOpen)
            {
                TestRigFramepopup.IsOpen = true;
                SolidColorBrush br = new SolidColorBrush
                {
                    Color = Colors.LightSkyBlue
                };
                BtnTestRigFrame.Background = br;
            }
            else
            {
                TestRigFramepopup.IsOpen = false;
                SolidColorBrush br = new SolidColorBrush
                {
                    Color = Colors.Gray
                };
                BtnTestRigFrame.Background = br;
            }
                
        }
    }
}
