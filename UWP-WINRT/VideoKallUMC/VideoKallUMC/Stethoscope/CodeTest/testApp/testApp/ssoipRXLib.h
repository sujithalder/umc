#pragma once

#define SSOIPRXEXPORT extern "C"   __declspec(dllexport)


SSOIPRXEXPORT void   SetLogFolder(const char* logfilder);
SSOIPRXEXPORT void   SetupConfiguration(const char*ip, short port, const char* cutoff, const char* username, const char*password);


SSOIPRXEXPORT void   LogMessage(const char* msg, bool append);

SSOIPRXEXPORT void   Connect();
SSOIPRXEXPORT void   Disconnect();

SSOIPRXEXPORT void   Record();
SSOIPRXEXPORT void ReadConfigurationFile();