﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMC
{
  public  class SensorData: INotifyPropertyChanged
    {
       

        //Sensor data 
        readonly string _SENSOR1 = "CEILING SENSOR RX";//"Celing Sensor RX";
        readonly string _SENSOR2 = "SEAT SENSOR RX  ";
        readonly string _SENSOR3 = "BP CUFF TX INFLATE";
        readonly string _SENSOR4 = "CONTROL SYSTEM";
        readonly string _SENSOR5 = "GLOVE SENSOR RX";
        readonly string _SENSOR6 = "BLOW SENSOR RX";
        readonly string _SENSOR7 = "SEAT BACK Position";
        readonly string _SENSOR8 = "CABIN CONTROL";
        readonly string _SENSOR9 = "CABIN MONITOR";
        readonly string _SENSOR10 = "TIME-BALANCE";//"TX DEPLOY/STORE";
        readonly string _SENSOR11 = "CABIN MONITOR";
        public event PropertyChangedEventHandler PropertyChanged;
         

        /// <summary>
        /// CABIN Monitor data
        /// </summary>
        /// 
        /// <summary>
        /// CABIN MONITOR DATA
        /// </summary>
        string _cint_humidity = string.Empty;
        string _cabin_int_temp = string.Empty;
        string _cabin_pathogen_count = string.Empty;
        string _cabin_ext_temp = string.Empty;
        string _cabin_ext_humidity = string.Empty;
        public string CINT_HUMIDITY
        {
            get { return _cint_humidity; }
            set
            {
                _cint_humidity = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CINT_HUMIDITY"));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CABIN_INT_TEMP
        {
            get { return _cabin_int_temp; }
            set
            {
                _cabin_int_temp = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_INT_TEMP"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_PATHOGEN_COUNT
        {
            get { return _cabin_pathogen_count; }
            set
            {
                _cabin_pathogen_count = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_PATHOGEN_COUNT"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_EXT_TEMP
        {
            get { return _cabin_ext_temp; }
            set
            {
                _cabin_ext_temp = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_EXT_TEMP"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CABIN_EXT_HUMIDITY
        {
            get { return _cabin_ext_humidity; }
            set
            {
                _cabin_ext_humidity = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CABIN_EXT_HUMIDITY"));
            }
        }
        public string SENSOR1
        {
            get { return _SENSOR1; }
        }
        public string SENSOR2
        {
            get { return _SENSOR2; }
        }

        public string SENSOR3
        {
            get { return _SENSOR3; }
        }
        public string SENSOR4
        {
            get { return _SENSOR4; }
        }
        public string SENSOR5
        {
            get { return _SENSOR5; }
        }
        public string SENSOR6
        {
            get { return _SENSOR6; }
        }
        public string SENSOR7
        {
            get { return _SENSOR7; }
        }
        public string SENSOR8
        {
            get { return _SENSOR8; }
        }
        public string SENSOR9
        {
            get { return _SENSOR9; }
        }
        public string SENSOR10
        {
            get { return _SENSOR10; }
        }
        public string SENSOR11
        {
            get { return _SENSOR11; }
        }

        public string Data1Sensor1
        {
            get
            {
                return " Light:";
            }
            
        }
        
        public string Data2Sensor1
        {
            get
            {
                return " Occupancy:";
            }
        }
        // 
        public string Data1Sensor2
        {
            get
            {
                return " Systolic:";
            }
        }

        public string Data2Sensor2
        {
            get
            {
                return " Diastolic :";
            }
        }
        public string Data1Sensor3
        {
            get
            {
                return "Clean Wash Dry";
            }
        }

        public string Data2Sensor3
        {
            get
            {
                return "Dermascope";
            }
        }

        public string Data3Sensor3
        {
            get
            {
                return "Thermometer";
            }
        }
        public string Data4Sensor3
        {
            get
            {
                return "Pulse Oxymeter";
            }
        }

       

        public string Data5Sensor3
        {
            get
            {
                return "Stethoscope";
            }
        }

        public string Data1Sensor4
        {
            get
            {
                return " Finger:";
            }
        }

        public string Data2Sensor4
        {
            get
            {
                return " Index Finger:";
            }
        }

        public string Data3Sensor4
        {
            get
            {
                return " Middle finger:";
            }
        }

        public string Data4Sensor4
        {
            get
            {
                return " Ring finger:";
            }
        }



        public string Data1Sensor5
        {
            get
            {
                return " Mass Air Flow:";
            }
        }

        public string Data2Sensor5
        {
            get
            {
                return " Mass Air Flow:";
            }
        }

        public string Data3Sensor5
        {
            get
            {
                return " Mass Air Flow:";
            }
        }

        public string Data4Sensor5
        {
            get
            {
                return " Mass Air Flow:";
            }
        }

        public string Data1Sensor6
        {
            get
            {
                return "Auto";
            }
        }
        public string Data2Sensor6
        {
            get
            {
                return "UP";
            }
        }
        public string Data3Sensor6
        {
            get
            {
                return "DOWN";
            }
        }
        public string Data4Sensor6
        {
            get
            {
                return "TOP";
            }
        }

        public string Data5Sensor6
        {
            get
            {
                return "CENTER";
            }
        }

        public string Data6Sensor6
        {
            get
            {
                return "BOTTOM";
            }
        }

        public string Data1Sensor7
        {
            get
            {
                return "CABIN INT HUMIDITY:";
            }
        }
        public string Data2Sensor7
        {
            get
            {
                return "CABIN INT TEMP:";
            }
        }

        public string Data3Sensor7
        {
            get
            {
                return "CABIN PATHOGEN COUNT:";
            }
        }

        public string Data4Sensor7
        {
            get
            {
                return "CABIN EXT TEMP:";
            }
        }
        public string Data5Sensor7
        {
            get
            {
                return "CABIN EXT HUMIDITY:";
            }
        }



        string _Value1Sensor1 = "On";
        public string Value1Sensor1
        {
            get
            {
                string val =  _Value1Sensor1;
                return val;
            }
            set
            {
                _Value1Sensor1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value1Sensor1"));
            }
        }

        string _Value2Sensor1 = "Yes"; 
         
        public string Value2Sensor1
        {
            get
            {
                string val = " " + _Value2Sensor1;
                return val;
            }
            set
            {
                _Value2Sensor1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value2Sensor1"));
            }
        }

        string _Value1Sensor2 = "120 mmHg";
        string _Value2Sensor2 = "90 mmHg";
        public string Value1Sensor2
        {
            get
            {
                string val = " " + _Value1Sensor2;
                return val;
            }
            set
            {
                _Value1Sensor2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value1Sensor2"));
            }
        }

        public string Value2Sensor2
        {
            get
            {
                string val = " " + _Value2Sensor2;
                return val;
            }
            set
            {
                _Value2Sensor2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value2Sensor2"));
            }
        }

        string _Value1Sensor3 = "Deploy";
        public string Value1Sensor3
        {
            get
            {
                string val = " " + _Value1Sensor3;
                return val;
            }
            set
            {
                _Value1Sensor3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("_Value1Sensor3"));
            }
        }

        string _Value2Sensor3 = "Restore";
        public string Value2Sensor3
        {
            get
            {
                string val = " " + _Value2Sensor3;
                return val;
            }
            set
            {
                _Value2Sensor3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("_Value2Sensor3"));
            }
        }

        string _Value1Sensor4 = "SD";
        public string Value1Sensor4
        {
            get
            {
                string val = " " + _Value1Sensor4;
                return val;
            }
            set
            {
                _Value1Sensor4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value1Sensor4"));
            }
        }

        string _Value2Sensor4 = "4.20";
        public string Value2Sensor4
        {
            get
            {
                string val = " " + _Value2Sensor4;
                return val;
            }
            set
            {
                _Value2Sensor4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value2Sensor4"));
            }
        }

        string _Value3Sensor4 = "3.70";
        public string Value3Sensor4
        {
            get
            {
                string val = " " + _Value3Sensor4;
                return val;
            }
            set
            {
                _Value3Sensor4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value3Sensor4"));
            }
        }

        string _Value4Sensor4 = "6.70";
        public string Value4Sensor4
        {
            get
            {
                string val = " " + _Value4Sensor4;
                return val;
            }
            set
            {
                _Value4Sensor4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value4Sensor4"));
            }
        }


        string _Value1Sensor5 = "50gm/sec";
        public string Value1Sensor5
        {
            get
            {
                string val = " " + _Value1Sensor5;
                return val;
            }
            set
            {
                _Value1Sensor5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value1Sensor5"));
            }
        }

        string _Value2Sensor5 = "80gm/sec";
        public string Value2Sensor5
        {
            get
            {
                string val = " " + _Value2Sensor5;
                return val;
            }
            set
            {
                _Value2Sensor5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value2Sensor5"));
            }
        }
        string _Value3Sensor5 = "110gm/sec";
        public string Value3Sensor5
        {
            get
            {
                string val = " " + _Value3Sensor5;
                return val;
            }
            set
            {
                _Value3Sensor5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value3Sensor5"));
            }
        }
        string _Value4Sensor5 = "150gm/sec";
        public string Value4Sensor5
        {
            get
            {
                string val = " " + _Value4Sensor5;
                return val;
            }
            set
            {
                _Value4Sensor5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value4Sensor5"));
            }
        }

        string _Value1Sensor6 = "10";
        public string Value1Sensor6
        {
            get
            {
                string val = " " + _Value1Sensor6;
                return val;
            }
            set
            {
                _Value1Sensor6= value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value1Sensor6"));
            }
        }
        string _Value2Sensor6 = "5";

        public string Value2Sensor6
        {
            get
            {
                string val = " " + _Value2Sensor6;
                return val;
            }
            set
            {
                _Value2Sensor6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value2Sensor6"));
            }
        }
        string _Value3Sensor6 = "2";
        
        public string Value4Sensor6
        {
            get
            {
                string val = " " + _Value2Sensor6;
                return val;
            }
            set
            {
                _Value2Sensor6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value2Sensor6"));
            }
        }
        public string Value3Sensor6
        {
            get
            {
                string val = " " + _Value3Sensor6;
                return val;
            }
            set
            {
                _Value3Sensor6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Value3Sensor6"));
            }
        }
        string _TimeLeft = "Remaining Time:";
        public string TimeLeft
        {
            get { return _TimeLeft; }
            set
            {
                _TimeLeft = string.Format(_TimeLeft, value);
                OnPropertyChanged(new PropertyChangedEventArgs("TimeLeft"));
            }
        }

        string _TimeLeftValue ="";
        public string TimeLeftValue
        {
            get {
                
                    return _TimeLeftValue; 
            }
            set
            {
                _TimeLeftValue = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TimeLeftValue"));
            }
        }

        string _Timeduration = "Call Duration:";
        public string TimeDuration
        {
            get { return _Timeduration; }
            set
            {
                _Timeduration = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TimeDuration"));
            }
        }

        string _TimedurationValue = "";
        public string TimedurationValue
        {
            get { return _TimedurationValue; }
            set
            {
                _TimedurationValue = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TimedurationValue"));
            }
        }

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        
    }
}
