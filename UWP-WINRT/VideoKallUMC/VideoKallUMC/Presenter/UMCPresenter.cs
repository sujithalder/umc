﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;
using UMC.Presenter;
using UMC.VideoConference;
using serialDevices;

namespace UMC
{
    public enum DEVICEDATA
    {
        CABINMONITOR,
        SMARTCARD,
        ATTENDANTNP,

    }

    public class TXRXeventsArgs : EventArgs
    {
        public TXRXeventsArgs() { }
        public TXRXeventsArgs(string data) { msg = data; }
        public TXRXeventsArgs(string data1,string data2) { msg = data1;msg2 = data2; }

        public string msg;
        public string msg2;
    }

    /// <summary>
    /// 
    /// </summary>
    public class UMCPresenter : INotifyPropertyChanged
    {


        /// <summary>
        /// SMART CARD DATA
        /// </summary>
        string _valueOnCard = string.Empty;
        string _visitcharge = string.Empty;
        string _cleaningOnCard = string.Empty;
        string _totalCleaningOnCard = string.Empty;
        string _visitCost = string.Empty;



        ////NPI Attendant Data
        string _name_npi = string.Empty;
        string _dea = string.Empty;
        string _provider_data = string.Empty;
        string _facility_data = string.Empty;
        string _facility_zip = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<EventArgs> Cameracommands;
        public   event EventHandler<EventArgs> ChartDisplay;
        public event EventHandler<EventArgs> CoreCommMessageReceived;
        public event EventHandler<EventArgs> TestCommRigMessageReceived;
        public SerialDeviceCommMgr _serialdevMgr = null;
        //public event EventHandler<EventArgs> StethoscopeArrayEvent;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        public SerialDeviceCommMgr SerialDevMgr
        {
            set { _serialdevMgr = value; }
            get { return _serialdevMgr; }
        }
        string _smartCardID = string.Empty;
        public string SmartCardID
        {
            get { return _smartCardID; }
            set
            {
                _smartCardID = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SmartCardID"));
            }
        }

        string _smtzipCode;
        public string SmtzipCode
            {
            get { return _smtzipCode; }
            set {
                _smtzipCode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SmtzipCode"));
            }
            }

        public string _cabinID = null;
        public string CabinID
        {
            get { return _cabinID; }
            set {
                _cabinID = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CabinID"));
            }
        }

        public string _cabinStatus;
        public string CabinStatus
        {
            get { return _cabinStatus; }
            set {
                _cabinStatus = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CabinStatus"));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string VisitCharge
        {
            get { return _visitcharge; }
            set
            {
                _visitcharge = value;
                OnPropertyChanged(new PropertyChangedEventArgs("VisitCharge"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public string ValueOnCard
        {
            get { return _valueOnCard; }
            set
            {
                _valueOnCard = value;

                OnPropertyChanged(new PropertyChangedEventArgs("ValueOnCard"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CleaningOnCard
        {
            get { return _cleaningOnCard; }
            set
            {
                _cleaningOnCard = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CleaningOnCard"));
            }
        }

        public string TotalCleaningOnCard
        {
            get { return _totalCleaningOnCard; }
            set
            {
                _totalCleaningOnCard = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TotalCleaningOnCard"));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public string VisitCost
        {
            get { return _visitCost; }
            set
            {
                _visitCost = value;
                OnPropertyChanged(new PropertyChangedEventArgs("VisitCost"));
            }
        }

        
        /// <summary>
        /// Read the simulated data from file.
        /// </summary>
        /// <param name="device"></param>
        public void ReadDeviceData(DEVICEDATA device)
        {
            switch (device)
            {
                case DEVICEDATA.SMARTCARD:
                    _modelObject.ReadSmartCard();
                    break;
                case DEVICEDATA.CABINMONITOR:
                    _modelObject.CabinMonitor();
                    break;
                case DEVICEDATA.ATTENDANTNP:
                    _modelObject.NPAttendant();
                    break;
            }
        }

        /// <summary>
        /// NPI DATA
        /// </summary>
        public String NAME_NPI
        {
            get { return _name_npi; }
            set
            {
                _name_npi = value;
                OnPropertyChanged(new PropertyChangedEventArgs("NAME_NPI"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DEA
        {
            get { return _dea; }
            set
            {
                _dea = value;
                OnPropertyChanged(new PropertyChangedEventArgs("DEA"));
            }

        }

        public string PROVIDER_NPI
        {
            get { return _provider_data; }
            set
            {
                _provider_data = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PROVIDER_NPI"));
            }

        }

        public string FACILITY_NAME
        {
            get { return _facility_data; }
            set
            {
                _facility_data = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FACILITY_NAME"));
            }
        }

        public string FACILITY_ZIP
        {
            get { return _facility_zip; }
            set
            {
                _facility_zip = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FACILITY_ZIP"));
            }
        }

       
        UMCModel _modelObject = null;
        public UMCModel ModelObject
        {
            get { return _modelObject; }
            set
            {
                _modelObject = value;
                _modelObject.Presenter = this;
                _pinfo.Presenter = this;
            }
        }

        PatientInformation _pinfo = new PatientInformation();

        public PatientInformation PatientInfo
        {
            get { return _pinfo; }
        }

        StethoscopeRx stethoscope = null;
        public void StartRX(bool connect=false)
        {
            if (!IsConnected)
                return;

            if (stethoscope == null)
            {
                stethoscope = new StethoscopeRx();
                stethoscope.TXevents += new EventHandler<EventArgs>(TXRXEventHandler);
            }

            stethoscope.StartRX(connect);
        }

        public string RXLogFile 
        {
            get
            {
                return stethoscope.RXLogFile;
            }
        }
         
        public void StartTX(int stethoscopeIndx, bool stopSreaming=false,bool lungs=false)
        {
            if (!IsConnected && stopSreaming)
            {
                return;
            }
               
            if (stethoscope == null)
            {
                stethoscope = new StethoscopeRx();
                stethoscope.TXevents += new EventHandler<EventArgs>(TXRXEventHandler);
                stethoscope.InitializeTX();
            }
            stethoscope.StartTX(stethoscopeIndx, stopSreaming,lungs);

        }

        public void TXRXEventHandler(object sender,EventArgs args)
        {
             TXRXevents?.Invoke(this, args); 
        }

        public void NotifyError(object sender, EventArgs args)
        { 
             TXRXevents?.Invoke(this, args); 
        }
        public event EventHandler<EventArgs> TXRXevents;

        public SensorData sensordata = new SensorData();

       public void CloseStethoscope()
        {
            
        }
        #region CAMERACOMMANDS
        public void StartCall()
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("START"));
        }

        public void InitializeCamera()
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("RESTART"));
        }

        public void MuteAudio(bool muteorunmute)
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("MUTE"));
        }
        public void TakePic()
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("TAKEPIC"));
        }

        public void EndCall()
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("STOPCAMERA"));
        }

        public void AcceptCall()
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("ACCEPTCALL"));
        }

        public void RefusedCall()
        {
            Cameracommands?.Invoke(this, new VideostreamingMsg("REFUSEDCALL"));
        }
        
        #endregion

        public void StartChartDisplay(string msg)
        {
            ChartDisplay?.Invoke(this, new ChartDataeventMsg(msg));
        }

        public void ClearChart()
        {
            _modelObject.ClearChart();
        }
        public void StartEKG(bool ekg)
        {
            _modelObject.StartEKG(ekg);
        }

        public void StartPO(bool ekg)
        {
            _modelObject.StartPO(ekg);
        }
        
        public void SaveNote(string note)
        {
            _modelObject.SaveNote(note);
        }

        public void MessageReceived(string msg)
        {
            CoreCommMessageReceived?.Invoke(this, new ChartDataeventMsg(msg));
        }

        public void MessageReceived(string msg,string msg2)
        {
            CoreCommMessageReceived?.Invoke(this, new ChartDataeventMsg(msg,msg2));
        }

        public void TestRigMessageReceived(string msg)
        {
            TestCommRigMessageReceived?.Invoke(this, new ChartDataeventMsg(msg));
        }
        public   void InitializeCommunicationModule()
        {
             _modelObject.InitializeCommunicationModule();
           // comm.Wait();
            return  ;
        }

        public void SendMessage(string msg,bool enabledelayafterMsg=false,bool enableDelaybeforemsg=false,bool connectionMsg=false)
        {
            _modelObject.SendMessage(msg, enabledelayafterMsg,enableDelaybeforemsg, connectionMsg);
        }

      public  void SendControlCMD(string cmd)
        {
            _modelObject.SendControlCMD(cmd);
        }
        bool _isCabinSW=false;
        public bool IsCabinSW
        {
            get {return _isCabinSW; }
            set { _isCabinSW = value; }
        }
        string _ipaddress = string.Empty;
        string _controlsystemIPAddress = string.Empty;
        string _myIPAddress = string.Empty;
        public string IPAddress 
        {
             get{   return _ipaddress;}
            
        }

        //CONTROLSYSTEM
        public string ControlSystemIPAddress
        {
            get { return _controlsystemIPAddress; }
        }

        public string MyIPAddress
        {
            get { return _myIPAddress; }
            set { _myIPAddress = value; }
        }


        public async Task<bool> ReadIPAddressAsync()
        {
            var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("config.txt");
            var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);
            foreach (var line in alltext)
            {
                string[] data = line.Split(':');
               switch(data[0])
                {
                    case "IP":
                        _ipaddress = data[1];
                        break;
                    case "CONTROLSYSTEM":
                        _controlsystemIPAddress = data[1];
                        break;
                    case "MYIPADDRESS":
                        MyIPAddress = data[1];
                        break;
                    case "MAXTIMEDOUT":
                        try
                        {
                           
                            MaxTimedOut = Convert.ToInt32(data[1]);
                        }
                        catch (Exception)
                        {
                            MaxTimedOut = 10;
                        }
                      
                        break;
                    case "MOTIONDETECTED":
                        if (data[1].Equals("1"))
                        {
                            Motiondetected = true;
                        }
                        else
                            Motiondetected = false;
                        break;
                    case "DIRTDETECTED":
                        if (data[1].Equals("1"))
                        {
                            Dirtdetected = true;
                        }
                        else
                            Dirtdetected = false;
                        break;
                    case "INSTRUMENTCLEANING":
                        if (data[1].Equals("1"))
                        {
                            InstrumentCleaning = true;
                        }
                        else
                            InstrumentCleaning = false;

                        break;
                    case "AUTOSTETHOSCOPETIMEINVERVAL":
                        AutoTimeInterval = Convert.ToInt32(data[1]);
                        break;
                }              

            }

            return true;
        }

        public async Task<bool> ReadInstrumentsCOMports()
        {
            var localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile = await localFolder.GetFileAsync("InstrumentsCOMports.txt");
            var alltext = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);
            foreach (var line in alltext)
            {
                string[] data = line.Split(':');
                switch (data[0])
                {
                    case "BloodPressure":
                        CoreCommunicationTXRX.BloodPressure = data[1];
                        break;
                    case "TAT_5000Thermometer":
                        CoreCommunicationTXRX.TAT_5000Thermometer= data[1];
                        break;
                    case "Pulse_Oxymeter":
                        CoreCommunicationTXRX.Pulse_Oxymeter = data[1];
                        break;
                    case "Height_Weight":
                        CoreCommunicationTXRX.Height_Weight = data[1];
                        break;
                    case "Stethoscope_Chair":
                        CoreCommunicationTXRX.Stethoscope_Chair = data[1];
                        break;
                    case "Instrumentcleaning":
                        CoreCommunicationTXRX.Instrumentcleaning = data[1];
                        break;
                    case "VSAT_Modem":
                        CoreCommunicationTXRX.VSAT_Modem = data[1];
                        break;
                    case "Smart_Card_Door_opener":
                        CoreCommunicationTXRX.Smart_Card_Door_opener = data[1];
                        break;
                    case "Glucose_Monitor":
                        CoreCommunicationTXRX.Glucose_Monitor = data[1];
                        break;
                    case "DermoscopeCamera":
                        CoreCommunicationTXRX.DermoscopeCamera = data[1];
                        break;
                    case "EKG":
                        CoreCommunicationTXRX.EKG = data[1];
                        break;
                    case "OtoscopeCamera":
                        CoreCommunicationTXRX.OtoscopeCamera = data[1];
                        break;
                    case "Spirometer":
                        CoreCommunicationTXRX.Spirometer = data[1];
                        break;
                    case "Ins_BloodPressure_Indx":
                        CoreCommunicationTXRX.Ins_BloodPressure_Indx = data[1];
                        break;
                    case "Ins_Thermometer_Indx":
                        CoreCommunicationTXRX.Ins_Thermometer_Indx = data[1];
                        break;
                    case "Ins_Pulse_Oxymeter_Indx":
                        CoreCommunicationTXRX.Ins_Pulse_Oxymeter_Indx = data[1];
                        break;
                    case "Ins_Height_Weight_Indx":
                        CoreCommunicationTXRX.Ins_Height_Weight_Indx = data[1];
                        break;
                    case "Ins_Stethoscope_Chair_Indx":
                        CoreCommunicationTXRX.Ins_Stethoscope_Chair_Indx = data[1];
                        break;
                    case "Ins_Instrumentcleaning_Indx":
                        CoreCommunicationTXRX.Ins_Instrumentcleaning_Indx = data[1];
                        break;
                    case "Ins_VSAT_Modem_Indx":
                        CoreCommunicationTXRX.Ins_VSAT_Modem_Indx = data[1];
                        break;
                    case "Ins_Smart_Card_Door_opener_Indx":
                        CoreCommunicationTXRX.Ins_Smart_Card_Door_opener_Indx = data[1];
                        break;
                    case "Ins_Glucose_Monitor_Indx":
                        CoreCommunicationTXRX.Ins_Glucose_Monitor_Indx = data[1];
                        break;
                    case "Ins_DermoscopeCamera_Indx":
                        CoreCommunicationTXRX.Ins_DermoscopeCamera_Indx = data[1];
                        break;
                    case "Ins_EKG_Indx":
                        CoreCommunicationTXRX.Ins_EKG_Indx = data[1];
                        break;
                    case "Ins_OtoscopeCamera_Indx":
                        CoreCommunicationTXRX.Ins_OtoscopeCamera_Indx = data[1];
                        break;
                    case "Ins_Spirometer_Indx":
                        CoreCommunicationTXRX.Ins_Spirometer_Indx = data[1];
                        break;
                    case "simulatedResponse":
                        if(data[1].Equals("1"))
                        CoreCommunicationTXRX.simulatedResponse = true;
                        else
                            CoreCommunicationTXRX.simulatedResponse = false;
                        break;
                }

            }

            return true;
        }

        int _autotimedout = 5;
     public   int AutoTimeInterval
        {          
            get { return _autotimedout; }
            set { if (value < 2)
                    _autotimedout = 2;
                else
                    _autotimedout = value;
                    }
        }

        bool _instrumentCleaning = false;
      public bool InstrumentCleaning
        {
            get { return _instrumentCleaning; }
           set {
               
                _instrumentCleaning = value;


            }
        }

        bool _dirtdetected= false;
        public bool Dirtdetected
        {
            get { return _dirtdetected; }
            set {

                _dirtdetected = value;
                   
            }
        }

        int _maxTimedout = 10; 
        public int MaxTimedOut
        {
            get { return _maxTimedout; }
            set
            {
                _maxTimedout = value;

            }
        }
        

        bool _motiondetected =false;
        public bool Motiondetected
        {
            get { return _motiondetected; }
            set {
               
                    _motiondetected = value;
                 
            }
        }

        bool _isConnected = false;
       public bool IsConnected
        {
            get { return _isConnected; }
            set { _isConnected = value; }
        }

        
    }


    public class ChartDataeventMsg : EventArgs
    {
        public ChartDataeventMsg(string msgin)
        {
            Msg = msgin;
        }
        public ChartDataeventMsg(string msgin,string msg2)
        {
            Msg = msgin;
            Msg2 = msg2;
        }
        public string Msg { get; set; }
        public string Msg2 { get; set; }
    }
}
