﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.IO;

namespace UMC
{
   public class PatientInformation: INotifyPropertyChanged
    {
        public enum PatientInfoTab
        {
            INFO,
            NOTE,
            VITAL,
            HEIGHT,
            WEIGHT,
            BP,
            
            PO,
            TERMO,
            DERMASCOPE,
            OTOSCOPE,
            EKG,
            SPIROMETER,
            GLOCOSE,

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
                PropertyChanged?.Invoke(this, e);
        }

        string _fullname=string.Empty;
        string _dateofBirth = string.Empty;
        string _medicalRecordNo = string.Empty;
        string _noteTxt = string.Empty;
        string _genderselectionIndex = string.Empty;
        string _maritialselectionIndex = string.Empty;
        string _employmentstatus = string.Empty;
        string _age = "Age: ";
        string _referalSource = string.Empty;
        string _isActive = "False";
       
        public string FullName
        {
            get { return _fullname; }
            set
            {
                _fullname = value;
                OnPropertyChanged(new PropertyChangedEventArgs("FullName"));
            }
        }

        public string DateOfBirth
        {
            get { return _dateofBirth; }
            set
            {
                _dateofBirth = value;
                OnPropertyChanged(new PropertyChangedEventArgs("DateOfBirth"));
            }
        }

        public string MedicalRecordNo
        {
            get { return _medicalRecordNo; }
            set
            {
                _medicalRecordNo = value;
                OnPropertyChanged(new PropertyChangedEventArgs("MedicalRecordNo"));
            }
        }

        

        public string Genderstatus
        {
            get { return _genderselectionIndex; }
            set
            {
                _genderselectionIndex = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Genderstatus"));
            }
        }

        public string Maritialstatus
        {
            get { return _maritialselectionIndex; }
            set
            {
                _maritialselectionIndex = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Maritialstatus"));
            }
        }

        public string Employmentstatus
        {
            get { return _employmentstatus; }
            set
            {
                _employmentstatus = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Employmentstatus"));
            }
        }

        public string Age
        {
            get { return _age; }
            set
            {
                _age = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Age"));
            }
        }

        public string ReferralSource
        {
            get { return _referalSource; }
            set
            {
                _referalSource = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ReferralSource"));
            }
        }

        
        
        public string NoteTxt
        {
            get { return _noteTxt; }
            set
            {
                _noteTxt = value;
                OnPropertyChanged(new PropertyChangedEventArgs("NoteTxt"));
            }
        }


        public string IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IsActive"));
            }
        }

        string _pulseRate = string.Empty;
        string _bloodpressure = string.Empty;
        string _weight = string.Empty;
        string _temprature = string.Empty;
        string _resiratory = string.Empty;
        string _pulseRateComments = string.Empty;
        string _bloodpressureComments = string.Empty;
        string _weightComments = string.Empty;
        string _tempratureComments = string.Empty;
        string _resiratoryComments = string.Empty;
        public string PulseRate
        {
            get { return _pulseRate; }
            set
            {
                _pulseRate = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PulseRate"));
            }
        }

        public string PulseRateComments
        {
            get { return _pulseRateComments; }
            set
            {
                _pulseRateComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PulseRateComments"));
            }
        }

        public string BloodPressure
        {
            get { return _bloodpressure; }
            set
            {
                _bloodpressure = value;
                OnPropertyChanged(new PropertyChangedEventArgs("BloodPressure"));
            }
        }

        public string BloodPressureComments
        {
            get { return _bloodpressureComments; }
            set
            {
                _bloodpressureComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("BloodPressureComments"));
            }
        }

        public string Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Weight"));
            }
        }

        public string WeightComments
        {
            get { return _weightComments; }
            set
            {
                _weightComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WeightComments"));
            }
        }

        public string Temprature
        {
            get { return _temprature; }
            set
            {
                _temprature = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Temprature"));
            }
        }


        public string TempratureComments
        {
            get { return _tempratureComments; }
            set
            {
                _tempratureComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TempratureComments"));
            }
        }

        public string Resiratory
        {
            get { return _resiratory; }
            set
            {
                _resiratory = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Resiratory"));
            }
        }

        public string ResiratoryComments
        {
            get { return _resiratoryComments; }
            set
            {
                _resiratoryComments = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ResiratoryComments"));
            }
        }

        public string _Result1 = string.Empty;
        public string _Result2 = string.Empty;
        public string _Result3 = string.Empty;
        public string _Result4 = string.Empty;
        public string _Result5 = string.Empty;
        public string _Result6 = string.Empty;
        public string _Result7 = string.Empty;
        public string _Result8 = string.Empty;
        public string _Result9 = string.Empty;
        public string _Result10 = string.Empty;
        public string _Result11 = string.Empty;
        public string _Result12 = string.Empty;

        public string Result1
        {
            get { return _Result1; }
            set { _Result1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result1"));
            }
        }

        public string Result2
        {
            get { return _Result2; }
            set
            {
                _Result2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result2"));
            }
        }
        public string Result3
        {
            get { return _Result3; }
            set
            {
                _Result3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result3"));
            }
        }
        public string Result4
        {
            get { return _Result4; }
            set
            {
                _Result4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result4"));
            }
        }
        public string Result5
        {
            get { return _Result5; }
            set
            {
                _Result5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result5"));
            }
        }
        public string Result6
        {
            get { return _Result6; }
            set
            {
                _Result6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result6"));
            }
        }
        public string Result7
        {
            get { return _Result7; }
            set
            {
                _Result7 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result7"));
            }
        }
        public string Result8
        {
            get { return _Result8; }
            set
            {
                _Result1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result8"));
            }
        }
        public string Result9
        {
            get { return _Result9; }
            set
            {
                _Result9 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result9"));
            }
        }
        public string Result10
        {
            get { return _Result10; }
            set
            {
                _Result10 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result10"));
            }
        }
        public string Result11
        {
            get { return _Result11; }
            set
            {
                _Result11 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result11"));
            }
        }
        public string Result12
        {
            get { return _Result12; }
            set
            {
                _Result12 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Result12"));
            }
        }


        public void ReadPatientInformation(PatientInfoTab tab)
        {
            switch(tab)
            {
                case PatientInfoTab.INFO:
                case   PatientInfoTab.NOTE:
                    _presenter.ModelObject.ReadPatientRecord();
                    break;
                case PatientInfoTab.VITAL:
                    _presenter.ModelObject.ReadVitalData();
                    break;
                case PatientInfoTab.HEIGHT:
                    _presenter.ModelObject.ReadPatientRecord();
                    break;
            }
        }

        UMCPresenter _presenter = null;
      public UMCPresenter Presenter
        {
            set { _presenter = value; }
        }

    }
}
