#pragma once
typedef unsigned char byte;

 
#define DAQEXPORT extern "C"   __declspec(dllexport)

 
typedef void(__stdcall *callbackFunc)(const char*);

DAQEXPORT  void Initialize(int devicenumber);
DAQEXPORT  int ReadPort(int portID);
DAQEXPORT void WriteData(int portID,byte data);
DAQEXPORT const char* Description();
DAQEXPORT int MaxChannel();
DAQEXPORT int DeviceNumber();
DAQEXPORT  void RegisterRXCallback(callbackFunc func);
