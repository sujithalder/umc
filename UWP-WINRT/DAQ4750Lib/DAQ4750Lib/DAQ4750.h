#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include "bdaqctrl.h"
#include "compatibility.h"
#include<string>
using namespace std;
using namespace Automation::BDaq;

typedef unsigned char byte;
typedef void(__stdcall *callbackFunc)(const char*);

class DAQ4750
{
public:
	void Initialize(int deviceNumber);
	 
	byte ReadPort(int PortID);
	void WriteData(int PortID, byte data);
	string Description();
	int MaxChannel()
	{
		DioFeatures *Di = instantDiCtrl->getFeatures();
		return Di->getChannelCountMax();
	}

	int DeviceNumber()
	{
		DeviceInformation devinfo;

		instantDiCtrl->getSelectedDevice(devinfo);
		return devinfo.DeviceNumber;
	}
	void RegisterRXCallback(callbackFunc func)
	{
		NotifyClient = func;
	}
	DAQ4750();
	~DAQ4750();
	string ErrorCodestr(ErrorCode er)
	{
		string ecode = std::to_string((int)er);
		switch (er)
		{
		case 0xA0000000:
			ecode = "WarningIntrNotAvailable";
			break;
		case 0xA0000001:
			ecode = "WarningParamOutOfRange";
			break;

		case 0xA0000002:
			ecode = "WarningPropValueOutOfRange";
			break;
		case 0xA0000003:
			ecode = "WarningPropValueNotSpted";
			break;
		case 0xA0000004:
			ecode = "WarningPropValueConflict";
			break;
		case 0xA0000005:
			ecode = "WarningVrgOfGroupNotSame";
			break;
		case 0xA0000006:
			ecode = "WarningPropPartialFailed";
			break;
		case 0xA0000007:
			ecode = "WarningFuncStopped";
			break;
		case 0xA0000008:
			ecode = "WarningFuncTimeout";
			break;
		case 0xA0000009:
			ecode = "WarningCacheOverflow";
			break;
		case 0xA000000A:
			ecode = "WarningBurnout";
			break;
		case 0xA000000B:
			ecode = "WarningRecordEnd";
			break;
		case 0xA000000C:
			ecode = "WarningProfileNotValid";
			break; 
		case 0xE0000000:
			ecode = "ErrorHandleNotValid";
			break;
		case 0xE0000001:
			ecode = "ErrorParamOutOfRange";
			break;
		case 0xE0000002:
			ecode = "WarningFuncTimeout";
			break;
		case 0xE0000003:
			ecode = "ErrorParamFmtUnexpted";
			break;
		case 0xE0000004:
			ecode = "ErrorMemoryNotEnough";
			break;
		case 0xE0000005:
			ecode = "ErrorBufferIsNull";
			break;
		case 0xE0000006:
			ecode = "ErrorBufferTooSmall";
			break;
		case 0xE0000007:
			ecode = "ErrorDataLenExceedLimit";
			break;
		case 0xE0000008:
			ecode = "ErrorFuncNotSpted";
			break; 
		case 0xE0000009:
			ecode = "ErrorEventNotSpted";
			break;
		case 0xE000000A:
			ecode = "ErrorPropNotSpted";
			break;
		case 0xE000000B:
			ecode = "ErrorPropReadOnly";
			break;
		case 0xE000000C:
			ecode = "ErrorPropValueConflict";
			break;
		case 0xE000000D:
			ecode = "ErrorPropValueOutOfRange";
			break;
		case 0xE000000E:
			ecode = "ErrorPropValueNotSpted";
			break;
		case 0xE000000F:
			ecode = "ErrorPrivilegeNotHeld";
			break; 

		case 0xE0000010:
			ecode = "ErrorPrivilegeNotAvailable";
			break;
		case 0xE0000011:
			ecode = "ErrorDriverNotFound";
			break;
		case 0xE0000012:
			ecode = "ErrorDriverVerMismatch";
			break; 

		case 0xE0000013:
			ecode = "ErrorDriverCountExceedLimit";
			break;
		case 0xE0000014:
			ecode = "ErrorDeviceNotOpened";
			break;
		case 0xE0000015:
			ecode = "ErrorDeviceNotExist";
			break; 
			 
		case 0xE0000016:
			ecode = "ErrorDeviceUnrecognized";
			break;
		case 0xE0000017:
			ecode = "ErrorConfigDataLost";
			break;
		case 0xE0000018:
			ecode = "ErrorFuncNotInited";
			break;
			 

			 
		}  
		 
		/*
			/// <summary>
			/// The required property is not supported. 
			/// </summary>
			

			/// <summary>
			/// The specified property value is out of range.
			/// </summary>
			
			/// <summary>
			/// The required privilege is not available because someone else had own it. 
			/// </summary>
			

			/// <summary>
			/// The loaded driver count exceeded the limitation. 
			/// </summary>
			

			/// <summary>
			/// The required device is unrecognized by driver. 
			/// </summary>
			

			/// <summary>
			/// The function is busy. 
			/// </summary>

			ErrorFuncBusy = 0xE0000019,

			/// <summary>
			/// The interrupt resource is not available. 
			/// </summary>
			ErrorIntrNotAvailable = 0xE000001A,

			/// <summary>
			/// The DMA channel is not available. 
			/// </summary>
			ErrorDmaNotAvailable = 0xE000001B,

			/// <summary>
			/// Time out when reading/writing the device. 
			/// </summary>
			ErrorDeviceIoTimeOut = 0xE000001C,

			/// <summary>
			/// The given signature does not match with the device current one.
			/// </summary>
			ErrorSignatureNotMatch = 0xE000001D,

			/// <summary>
			/// The function cannot be executed while the buffered AI is running.
			/// </summary>
			ErrorFuncConflictWithBfdAi = 0xE000001E,

			/// <summary>
			/// The value range is not available in single-ended mode.
			/// </summary>
			ErrorVrgNotAvailableInSeMode = 0xE000001F,

			/// <summary>
			/// The value range is not available in 50omh input impedance mode..
			/// </summary>
			ErrorVrgNotAvailableIn50ohmMode = 0xE0000020,

			/// <summary>
			/// The coupling type is not available in 50omh input impedance mode..
			/// </summary>
			ErrorCouplingNotAvailableIn50ohmMode = 0xE0000021,

			/// <summary>
			/// The coupling type is not available in IEPE mode.
			/// </summary>
			ErrorCouplingNotAvailableInIEPEMode = 0xE0000022,

			/// <summary>
			/// The Communication is failed when reading/writing the device.
			/// </summary>
			ErrorDeviceCommunicationFailed = 0xE0000023,

			/// <summary>
			/// The device's 'fix number' conflicted with other device's
			/// </summary>
			ErrorFixNumberConflict = 0xE0000024,

			/// <summary>
			/// The Trigger source conflicted with other trigger configuration
			/// </summary>
			ErrorTrigSrcConflict = 0xE0000025,

			/// <summary>
			/// All properties of a property set are failed to be written into device.
			/// </summary>
			ErrorPropAllFailed = 0xE0000026,

			/// <summary>
			/// Undefined error 
			/// </summary>
			ErrorUndefined = 0xE000FFFF,
				*/
return ecode;
	}
private:
	InstantDiCtrl * instantDiCtrl;
	InstantDoCtrl * instantDoCtrl;
	callbackFunc NotifyClient;
};

