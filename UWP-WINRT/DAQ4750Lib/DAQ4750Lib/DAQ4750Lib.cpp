// DAQ4750Lib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include"DAQ4750Lib.h"
#include<iostream>
#include"DAQ4750.h"
#include"LogfileUtils.h"
using namespace std;
LogfileUtils logutil;
DAQ4750 *daq = new DAQ4750();
DAQEXPORT void Initialize(int devicenumber)
{

	daq->Initialize(devicenumber);

}

DAQEXPORT int ReadPort(int portID)
{
	return daq->ReadPort(portID);
}

DAQEXPORT void WriteData(int portID, byte data)
{
	daq->WriteData(portID, data);
}

DAQEXPORT  const char* Description()
{
	string desc = daq->Description();
	return desc.c_str();
}

DAQEXPORT int MaxChannel()
{
	return daq->MaxChannel();
}

DAQEXPORT int DeviceNumber()
{
	return daq->DeviceNumber();
}

DAQEXPORT  void RegisterRXCallback(callbackFunc func)
{
	daq->RegisterRXCallback(func);
}