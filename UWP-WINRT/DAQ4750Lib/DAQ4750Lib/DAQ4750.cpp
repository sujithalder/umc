#include "stdafx.h"
#include "DAQ4750.h"

#define  deviceDescription  L"DemoDevice,BID#0"
void DAQ4750::Initialize(int deviceNumber)
{
	if (NotifyClient != NULL)
		NotifyClient("Initialize0");
	ErrorCode ret = Success;
	
	instantDiCtrl = InstantDiCtrl::Create();

	 //DeviceInformation devInfo(deviceDescription);
	DeviceInformation devInfo(deviceNumber);

	ret = instantDiCtrl->setSelectedDevice(devInfo);

	if (ret != Success)
	{
		  
		if (NotifyClient != NULL)
			NotifyClient(ErrorCodestr(ret).c_str());
		return;
	}

	  instantDoCtrl = InstantDoCtrl::Create();

	  ret = instantDoCtrl->setSelectedDevice(devInfo);
	  if (ret != Success)
	  {
		  if (NotifyClient != NULL)
			  NotifyClient(ErrorCodestr(ret).c_str());
		  return;
	  }

	  if (NotifyClient != NULL)
		  NotifyClient("Initialize");
}

 

byte DAQ4750::ReadPort(int PortID)
{
	if (instantDiCtrl == NULL) return -1;
	byte portData=0;
	ErrorCode        ret = Success;
	ret = instantDiCtrl->Read(PortID, portData);
	if (ret != Success)
	{
		if (NotifyClient != NULL)
			NotifyClient(ErrorCodestr(ret).c_str());
		return;
	}
	return portData;
}

void DAQ4750::WriteData(int PortID, byte data)
{
	ErrorCode err = Success;

	err = instantDoCtrl->Write(PortID, data);
	if (err != Success)
	{
		if (NotifyClient != NULL)
			NotifyClient(ErrorCodestr(err).c_str());
		return;
	}
}

string DAQ4750::Description()
{
	if (instantDiCtrl == NULL) return "";

	DeviceInformation devinfo;

	instantDiCtrl->getSelectedDevice(devinfo);
	wstring lstr = devinfo.Description ;
	string desc (lstr.begin(),lstr.end() );
	return desc; 
}

DAQ4750::DAQ4750()
{
	if (NotifyClient != NULL)
		NotifyClient("ctr");
	instantDiCtrl = NULL;
}


DAQ4750::~DAQ4750()
{
	instantDiCtrl->Dispose();
}
